program tsat
    implicit none

   ! PALENTI DODECANE 
    real*8 :: pinfty1 = 4e8,pinfty2 = 0.0
    real*8 :: cp1 = 2534.00, cp2 = 2005.00
    real*8 :: cv1 = 1077.7,cv2 = 1956.45
    real*8 :: gm1 = 2.35,gm2 = 1.025
    real*8 :: q1 = -755.269e3,q2 = -237.547e3
    real*8 :: qp1 = 0.0,qp2 = -24.4e3
    real*8 :: psat = 1.5e5


    !! SAUREL DODECANE
  !  real*8 :: pinfty1 = 4e8,pinfty2=0.0 
  !  real*8 :: cp1 = 2534, cp2 = 2005
  !  real*8 :: cv1 = 1077,cv2 = 1956.00
  !  real*8 :: gm1 = 2.35,gm2 = 1.025
  !  real*8 :: q1 = -755e3,q2 = -237e3
  !  real*8 :: qp1 = 0.0,qp2 = -24e3
  !  real*8 :: psat = 1.5e5
 



    ! Coefficients
    real*8 :: adum,bdum,cdum,ddum
    real*8:: tdum_old,tdum1

    ! index
    integer :: i


    !calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    bdum = (q1-q2)/(cp2-cv2)
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)

    print*, adum ,bdum ,cdum, ddum



    tdum_old = 300.00    
    do i = 1,100        
        tdum1 = bdum/(log(psat+pinfty2)-adum-cdum*log(tdum_old)-ddum*log(psat+pinfty1))
        if(abs((tdum1-tdum_old)/tdum_old) .lt. 1e-5) goto 10
        tdum_old = tdum1
    enddo
    
    10   print *, "TSAT = ", tdum1, "PSAT = ", psat/1e5, " ATM"  
    print *, -log(psat+pinfty2),  -adum-(bdum/tdum1)-cdum*log(tdum1)-ddum*log(psat+pinfty1)



    



end program tsat
