program satSolve
    implicit none

    !------------------  INITIALIZING ------------------------------------------------!
    ! Dodecane Properties


    ! Temperature 
    real*8 :: pstart , pend                            ! ATM
    real*8 :: tstart, tend 
    real*8 :: tdiff
    real*8 :: numT
    integer :: numTint
    real*8,dimension(:),ALLOCATABLE :: temper_arr     !To store temperature

    ! Pressure 
    real*8,DIMENSION(:),ALLOCATABLE :: p_arr

    ! Other variables (loop)
    integer :: i    




    !! SCALING 
    real*8 :: cvscale, tempscale, pscale, rscale, gibbs_scale  


    !! ASSUME PRESSURE, GET TEMPERATURE
    numT = 100

    pstart = 0.1e5             ! 0.1 atm in Pa  
    pend   = 30e5              ! 10 atm in Pa 
    tdiff = (pend-pstart)/(numT-1)

    numTint = int(numT)
    ALLOCATE(p_arr(numTint))
    p_arr(1) = pstart

    do i = 2,numTint
         p_arr(i) = p_arr(i-1) + tdiff
    end do
    ALLOCATE(temper_arr(numTint))




    !! ASSUME TEMPERATURE, GET PRESSURE
    ! allocate temperature arr
    ! numT = 100

    ! tstart = 298            
    ! tend   = 473               
    ! tdiff = (tend-tstart)/(numT-1)

    ! numTint = int(numT)
    ! ALLOCATE(temper_arr(numTint))
    ! temper_arr(1) = tstart

    ! do i = 2,numTint
    !     temper_arr(i) = temper_arr(i-1) + tdiff
    ! end do
    !   !allocate Temperature arr
    ! ALLOCATE(p_arr(numTint))
  
  

    !! Scales
    pscale = 1e5
    rscale = 1000
    tempscale = 300 
    cvscale = pscale/(rscale*tempscale) 
    gibbs_scale = cvscale*tempscale 


    open(unit = 21, file = 'sat.dat', action = 'write')

    ! GETTING Psat
    write(*,*) "PRESSURE, TEMPERATURE, GIBBS LIQUID, GIBBS VAPOR"  
    do i = 1,numTint
        call steffensonBW(p_arr(i),temper_arr(i)) !! Assume P, get T



        ! call steffenson(p_arr(i),temper_arr(i))    !!Assume T, get P
        write(21,*)  temper_arr(i), p_arr(i)/pscale
        print*, temper_arr(i), p_arr(i)/pscale


    end do 
    



    !! Deallocating
    DEALLOCATE(temper_arr)
    DEALLOCATE(p_arr)

  


end program satSolve





!--- FOR GOING BACKWARD, ACCEPT P THEN GET T-----!

subroutine steffensonBW(pressdum,result)
    implicit none
   
    real*8,INTENT(IN) :: pressdum
    real*8,INTENT(OUT) :: result
    real*8  :: tguess
    real*8 :: error
    real*8 :: tol = 1e-5
    real*8 :: fprime
    real*8 :: y,y2, tnext, fguess, fnext, error_diff
    integer :: i 

    tguess = 300.00
    error = 1.00


    do while (abs(error)>tol) 
    
        call f(pressdum, tguess,y)

        call f(pressdum,tguess+y,y2)

        fprime = (y2-y)/y

        tguess = tguess - (y/fprime)

        call f(pressdum,tguess,fguess)

        call f(pressdum,tnext,fnext)

        call f(pressdum,tguess,error_diff)

        error = abs(error_diff)/(tguess)


    end do


    ! do i = 1,100
    
    !     call f(pressdum, tguess,y)

    !     call f(pressdum,tguess+y,y2)

    !     fprime = (y2-y)/y

    !     tnext = tguess - (y/fprime)            

    !     error_diff = tnext-tguess

    !     error = abs(error_diff)/(tguess)

    !     if (abs(error)<tol) exit 

    !     tguess = tnext
    
    ! end do

    result = tguess

end subroutine steffensonBW






! Function accepts temper, p and other coeff a, b, c , d

subroutine f(pdum,tdum, res)
    implicit none

    !   DODECANE PALENTI
    real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    real*8 :: cp1 = 2532.595, cp2 = 2005.36125
    real*8 :: cv1 = 1077.7,cv2 = 1956.45
    real*8 :: gm1 = 2.35,gm2 = 1.025
    real*8 :: q1 = -775.269e3,q2 = -237.547e3
    real*8 :: qp1 = 0.0,qp2 = -24.4e3 


   
    !   DODECANE MATEYER, SAUREL
    ! real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    ! real*8 :: cp1 = 2534, cp2 = 2005
    ! real*8 :: cv1 = 1077,cv2 = 1956
    ! real*8 :: gm1 = 2.35,gm2 = 1.025
    ! real*8 :: q1 = -755e3,q2 = -237e3
    ! real*8 :: qp1 = 0.0,qp2 = -24e3 






    ! Saurel Variables coefficients
    real*8 :: adum,bdum,cdum,ddum

    real*8,intent(in) :: tdum, pdum
    real*8,intent(out) :: res

    !calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    bdum = (q1-q2)/(cp2-cv2)
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)



    res = log(pdum+pinfty2)-adum-(bdum/tdum)-cdum*log(tdum)-ddum*log(pdum+pinfty1)
    
end subroutine f









! Subroutine for Steffenson's Method


subroutine steffenson(result,temperdum)
     implicit none

     real*8,INTENT(IN) :: temperdum
     real*8,INTENT(OUT) :: result
     real*8  :: pguess 
     real*8 :: errorTOL, errorDIFF
     real*8 :: tol = 1e-5
     real*8 :: fprime
     real*8 :: y,y2

     pguess = 300.00
     errorTOL = 100.00


     do while (abs(errorTOL)>tol) 
         call f(pguess, temperdum,y)

         call f(pguess+y,temperdum,y2)

         fprime = (y2/y)-1.00

         pguess = pguess - (y/fprime)

         call f(pguess,temperdum,errorDIFF)

         errorTOL = abs(errorDIFF)/pguess


     end do

     result = pguess

end subroutine steffenson



