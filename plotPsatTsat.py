import matplotlib.pyplot as plt 
import numpy as np 
from numpy import genfromtxt

##Load data from iter 
iterSat = np.loadtxt('sat.dat')
iterTemp = iterSat[:,0]
iterPres = iterSat[:,1]

##Load data from Engauge French paper 
frenchSat = genfromtxt('frenchPsat.csv', delimiter=',',skip_header=1)
frenchTemp = frenchSat[:,0]
frenchPres = frenchSat[:,1]


# print(iterTemp)
# print(frenchTemp)


plt.figure(1)
figure = plt.gcf()
figure.set_size_inches(8, 6)
plt.plot(iterTemp,iterPres,'-b',label="Current")
plt.plot(frenchTemp,frenchPres,'xr',label="Experimental")
plt.tick_params(axis='both', which='major', direction = "in", labelsize=18)	
plt.xlabel('Temperature (K)',fontsize=18)
plt.ylabel('Pressure (atm)',fontsize=18)
plt.xlim([350,650])
plt.ylim([0,20])
plt.legend(fontsize=18)
# plt.savefig('/home/maxle/leminhman0312@gmail.com/Master_Thesis/figures/iterVSlemetayer.png',dpi=100)
plt.savefig('/home/maxle/leminhman0312@gmail.com/Master_Thesis/figures/iterVSlemetayer.eps',format='eps',dpi=1000)


# plt.show()

# plt.figure(1)
# figure = plt.gcf()
# figure.set_size_inches(8, 6)
# plt.plot(iterTemp,iterPres,'-b')
# plt.tick_params(axis='both', which='major', labelsize=18)	
# plt.xlabel('Temperature (K)',fontsize=18)
# plt.ylabel('Pressure (atm)',fontsize=18)
# plt.xlim([350,650])
# plt.ylim([0,20])
# plt.savefig('/home/maxle/leminhman0312@gmail.com/Master_Thesis/figures/TsatPsat.png',dpi=100)