# This is makefile for RocSDT Fortran


#**********************************************************************
# PreProcessor Directive Definitions
#
# _____________________________________________________________________
# INTERFACECORRECTION - Directive for selecting Shukla Reinit Scheme
#    0 = Original Shukla 2010 scheme, without particle
#          mass or volume or shape conservation
#    1 = Shukla 2010 scheme with particle mass conserv only
#    2 = Shukla 2010 scheme with particle mass+volume conserv 
#    3 = Shukla 2010 scheme with particle mass+volume+shape conserv 
#    if no reinitialization, set to any number not listed above
#    
# _____________________________________________________________________
# GEOMETRY - Directive for selecting problem coordinate system
#    1 = Cartesian Coordinate System (x,y,z)
#    2 = Axisymmetric Cylindrical Coordinate System (r,z)
# 
# _____________________________________________________________________
# ROCPACK - Directive for reading in phi field from rocpack file
#    0 = off
#    1 = on (note must set input file in readrocpack.f90)
# 
# _____________________________________________________________________
# PERBC - Directive for selecting periodic boundary condition 
#         note this can only be turned on when GEOMETRY=1 and probdim>1
#         note periodic in cartesian y-direction only 
#    0 = off
#    1 = on
#
# _____________________________________________________________________
# REACT - Directive for including reaction physics in simulation
#         note, when on, nspec in inputfile must be greater than zero
#    0 = off
#    1 = on
# 
# _____________________________________________________________________
# probdim - Directive for selecting problem dimensions (1,2, or 3)
#    1 = one-dimensional problem (used when GEOMETRY=1 or GEOMETRY=2)
#    2 = two-dimensional problem (used with GEOMETRY=1 or GEOMETRY=2)
#    3 = three-dimensional problem (only used when GEOMETRY=1)
# 
# _____________________________________________________________________
# frozenphi - Directive for selecting fixed particles during 
#             shape correction 
#    0 = off (particles are allowed move)
#    1 = on  (particles are fixed to t=0 phi field)
# 
# 
# End of PreProcessor Directive Definitions
#**********************************************************************



# This section is for preprocessing directives
DIREC:= -DINTERFACECORRECTION=4\
        -DGEOMETRY=1 \
        -DROCPACK=0 \
        -DPERBC=0 \
        -DREACT=0 \
        -Dprobdim=1 \
        -Dfrozenphi=0



# uncomment the compiler you wish to use 
# Definition for compiler
F := gfortran #gnu fortran compiler
#F := /opt/intel/bin/ifort #intel fortran compiler


# uncomment the flags you wish to use, match with compiler
# Flags for compiler
# ifortran flags
#FLAGS := -c -cpp $(DIREC) -O3 -r8 -no-wrap-margin -nofixed 
#gfortran flags
# FLAGS := -c -cpp $(DIREC) -O3 -fdefault-real-8 #-ffpe-trap=invalid,zero
#gfortran debug flags
FLAGS := -c -g -cpp $(DIREC) -fdefault-real-8 


# Path to the source directory for searching *.f90 files
SRC = src/
VPATH = src:src/RWfiles:src/Shukla2010files



# List of read/write function and related files
RWFILES =   writetecplot.o writedrag_singleparticle.o \
            writerestart.o readrestart.o writefile.o \
            schlieren.o readrocpack.o

# List of Shukla2010 interface correction function files
SHUKLA10 = calcphirhs.o calcrhorhs4.o calcrhorhs5.o\
           shukla2010five.o zz_rhomat.o shapecorrect.o \
           zz_phi.o secant_mass_zz.o secant_volmass_zz.o \


# List of files for temperature correction
TMPCOR = calctemprhs4.o tempcorrection.o 


# List of files for flux computation/time integration/boundary condition
FLXTIME = calcfluidprop.o slopelimiter.o BC.o hllc.o tvd.o \
          rk3.o solvepressure.o updatereaction.o updatemixrho.o

# List of files for initialization
INIT = readinput.o readmaterials.o allocate.o deallocate.o \
       makegrid.o makefiles.o initialflowfield.o MG_EOS.o \
       MG_shockstates.o Stiff_shockstates.o


#List of files for appendix B

APPB = appendixB.o


#List of files for integrating 6.3 saurel (mass/thermal) 

THERMO = solveSource6663.o findMaxSource.o solveTsat.o findMaxSourceSy2.o

#List of files to calculate temperature

TEMPER = calctemper.o testtemper.o

#List of files to calculate pressure

PRESSURE = calcPressure.o calcRhoInternalE.o

#List of files to calculate phase 

PHASE = phaseTrans.o pelantiSolve.o palentiTest.o pelantiSolveJZ.o

# Files for compilation
FILES := main.o Modules.o updatelastphi.o \
         $(RWFILES) $(FLXTIME) $(TMPCOR) $(SHUKLA10) $(INIT) $(TEMPER) $(PRESSURE) $(APPB) $(THERMO) $(PHASE) progmain.o



#________________________________________
#          Main Code/Modules
#________________________________________
rocsdt: $(FILES) 
	$(F) $(FILES) -o rocsdt 
#-llapack -lblas -lmpi 

%.o: %.f90
	$(F) $(FLAGS) -I $(SRC) $< 

# command to remove object and module files "make clean"
clean: 
	rm -f *.o *.mod


# command to remove object+module+rocsdt executable files "make clear"
clear: 
	rm -f rocsd* *.o *.mod 


# command to remove datafiles and figures "make reset"
reset:
	cd test/ && rm -f *.dat tecplotoutput/*.dat tecplotoutput/*.txt  tecplotoutput/frames_movies/*.png tecplotoutput/frames_movies/*.mp4  \
  && rm -f fort.* 
#   note if you are rerunning the same case using rocsdt then
#   you must type "make reset" before "./rocsdt"
