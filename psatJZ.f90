  subroutine psat(pdum,tdum)
    !   DODECANE PALENTI
     real*8 :: pinfty1 = 4e8,pinfty2=0.0 
     real*8 :: cp1 = 2532.595, cp2 = 2005.36125
     real*8 :: cv1 = 1077.7,cv2 = 1956.45
     real*8 :: gm1 = 2.35,gm2 = 1.025
     real*8 :: q1 = -775.269e3,q2 = -237.547e3
     real*8 :: qp1 = 0.0,qp2 = -24.4e3 



     !   DODECANE SAUREL 
    !  real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    !  real*8 :: cp1 = 2531, cp2 = 2005
    !  real*8 :: cv1 = 1077,cv2 = 1956.00
    !  real*8 :: gm1 = 2.35,gm2 = 1.025
    !  real*8 :: q1 = -775e3,q2 = -237e3
    !  real*8 :: qp1 = 0.0,qp2 = -24.4e3 


  ! Saurel Variables coefficients
    real*8 :: adum,bdum,cdum,ddum

!!$    real*8,intent(in) :: tdum, pdum
!!$    real*8,intent(out) :: res
    real*8 :: tdum_n, res,tguess
    ! real*8, INTENT(INOUT):: tguess
    real*8 :: f, f1, g
    integer i
    
    real*8, intent(in) :: pdum 
    real*8, intent(out) :: tdum 

    !calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    bdum = (q1-q2)/(cp2-cv2)
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)

      

    tguess = 300

    do i = 1, 100
!!$       tdum_n = bdum/(log(pdum+pinfty2)-adum-cdum*log(tdum)-ddum*log(pdum+pinfty1))
       f = log(pdum+pinfty2)-adum-(bdum/tguess)-cdum*log(tguess)-ddum*log(pdum+pinfty1)
       f1 = log(pdum+pinfty2)-adum-(bdum/(tguess+f))-cdum*log(tguess+f)-ddum*log(pdum+pinfty1)
       g=f1/f-1.
       tdum_n = tguess - f/g
       
       if (abs((tdum_n-tguess)/tguess) .lt. 1e-3) exit 

       tguess = tdum_n 


      !  print*, i , tdum, pdum 

    enddo

    tdum = tguess 

    print*, tdum, pdum 

  end subroutine psat



  program main

    implicit none
    real*8,dimension(:),ALLOCATABLE :: p_arr, t_arr 
    real*8 :: pstart, pend, pdiff 
    real*8 :: numP
    integer :: numPint, i 

    real*8 :: ptest, ttest

    ! allocate temperature arr
    numP = 100

    pstart = 0.1e5
    pend = 30e5

    pdiff = (pend-pstart)/(numP-1)

    numPint = int(numP)
    ALLOCATE(p_arr(numPint))
    p_arr(1) = pstart

    do i = 2,numPint
         p_arr(i) = p_arr(i-1) + pdiff
    end do

    !allocate Pressure arr
    ALLOCATE(t_arr(numPint))
   
    open(unit = 21, file = 'sat.dat', action = 'write')

    DO i = 1,size(p_arr)
      call psat(p_arr(i),t_arr(i))
      write(21,*) t_arr(i), p_arr(i)/1e5
    END DO 


   
    ! ptest = 8e5
    ! ttest = 300
    ! call psat(ptest,ttest)





    DEALLOCATE(t_arr)
    DEALLOCATE(p_arr)
    CLOSE(21)

  end program main 