# -*- coding: utf8 -*-

from __future__ import unicode_literals
import numpy as np 
import os 
import sys


from matplotlib.ticker import ScalarFormatter


import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import fnmatch
import matplotlib.gridspec as gridspec


myPath = '/home/maxle/sonov2/test/tecplotoutput/'

iarr = []

datCounter = 0
for root, dirs, files in os.walk(myPath):
	for file in files:    
		if file.endswith('.dat'):
			iarr.append(file)
			datCounter += 1

iarr = np.sort(iarr)

print(iarr)

figure = plt.gcf()
figure.set_size_inches(20,10)

##Scaling for plots

lengthscale = 1.00
lscale = 1.00
pscale = 1e5
rscale = 1000.00

vscale = np.sqrt(pscale/rscale)

tscale = 300.00

figwidth = 10
figheight = 10


# MAIN PLOT CODE
#FOR interface at x = 0.75 , pick range 0.7 to 0.8 
#CORRESPOND to i = 584 to 657

interlow = 584
interhigh = 657

gridspec.GridSpec(4,3)

for ii, i in enumerate(iarr):


    fileii = open(myPath+i, 'r')

    line = fileii.readline()
    tline = line.split()
    time = float(tline[1])

    # time = time/10.



    state = np.loadtxt(myPath+i,skiprows=2)
    x = state[:,0]
    rho = state[:,1]
    u = state[:,2]
    p = state[:,3]
    t1 = state[:,4]
    t2 = state[:,5]
    phi1 = state[:,6]
    phi2 = state[:,7]
    # a = state[:,8]*vscale
    rhomat1 = state[:,8]


    # plt.suptitle("MODIFIED RESULTS at %i iteration , interface at X = 0.75 m , time = %.5f second" %(ii*1000,time), fontsize=18)


    fig = plt.figure(1)
    ax = fig.add_subplot(221)
    plt.semilogy(x*lscale,p*pscale/1e5,'-xb')
    # plt.plot(x*lscale,p*pscale/1e5,'-xb')
    ax.yaxis.set_major_formatter(ScalarFormatter())
    ax.tick_params(axis='both', which='major', labelsize=14)	
    plt.xlim([0,1])
    plt.ylabel('Pressure (0.1 x MPa)',fontsize=18)



    ax = fig.add_subplot(222)
    # plt.subplot(2,2,1)
    # plt.title('VELOCITY vs distance',fontsize=20)
    plt.plot(x*lscale,vscale*u,'-xb')
    ax.tick_params(axis='both', which='major', labelsize=14)
    # ax.tick_params(axis='y', which='major', pad=20)	
    plt.xlim([0,1])
    plt.ylim([0,350])
    plt.ylabel('Velocity (m/s)',fontsize=18,labelpad=20)




    ax = fig.add_subplot(223)
    plt.semilogy(x*lscale,rscale*rho,'-xb')
    ax.tick_params(axis='both', which='major', labelsize=14)	
    plt.xlim([0,1])
    plt.ylim([1,1000])
    plt.xlabel('x(m)',fontsize=16)
    plt.ylabel(r'Density (kg/m$^3$)',fontsize=18)
    ax.yaxis.set_major_formatter(ScalarFormatter())


    
    # plt.subplot(2,2,4)
    ax = fig.add_subplot(224)
    # plt.title('Vapor mass fraction vs distance',fontsize=20)
    # plt.plot(x*lscale,phi1,'-xb')
    # ax.tick_params(axis='both', which='major', labelsize=14)	
    # plt.xlim([0,1])
    # plt.xlabel('x(m)',fontsize=16)
    # plt.ylabel('Vapor volume fraction',fontsize=18,labelpad=20)

    # plt.plot(x*lscale,rhomat1/(rho)) 

    # plt.subplot(2,2,4)
    # plt.plot(phi1, vscale*u,'-xb')
    



    # plt.subplot(2,2,4)
    # plt.plot(p,t2,'-xb')

    # plt.subplot(3,3,5)
    # plt.title('TEMPERATURE')
    # plt.plot(x*lscale,tscale*t1,'-or',label="T vapor")
    # plt.plot(x*lscale,tscale*t2,'-xb',label="T liquid")
    # plt.legend()	

    # plt.subplot(3,3,6)
    # plt.title('INTERFACE TEMPERATURE %i'%ii )
    # plt.plot(x[interlow:interhigh],t1[interlow:interhigh],'-or',label='T vapor')
    # plt.plot(x[interlow:interhigh],t2[interlow:interhigh],'-xb',label='T liquid')
    # plt.legend()

    # plt.subplot(3,3,7)
    # plt.title('SPEED SOUND (m/s)')
    # plt.plot(x*lscale,a,'-xb')

    # plt.subplot(2,2,5)
    # plt.subplot2grid((4,3), (0,0), colspan=2, rowspan=3)
    # plt.title("Speedsound vs Phi")
    # plt.plot(phi2,a,'-xb',label="vs Phi liquid")
    # plt.plot(phi1,a,'-or',label="vs Phi vapor")
    # plt.legend()


    plt.plot((1./(rho*rscale)),p*pscale)
    plt.tight_layout(pad=2.00,w_pad = 8.00) 
    plt.subplots_adjust(top=0.88)
    fname = '%06d.png'%ii
    plt.savefig(fname)
    plt.clf()







#If want to make movie, frame of 20 is good enough 

# os.system("ffmpeg -r 20 -i %06d.png -c:v libx264 -pix_fmt yuv420p results.mp4")
# print("FINISHED CREATING MOVIE")


