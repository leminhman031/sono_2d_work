program tsat 
    implicit none



    ! Water properties
    ! real*8 :: pinfty1 = 1e9,pinfty2 = 0.0
    ! real*8 :: cp1 = 4267,cp2 = 1487
    ! real*8 :: cv1 = 1816, cv2 = 1040
    ! real*8 :: gm1 = 2.35,gm2 = 1.43
    ! real*8 :: q1 = -1167e3,q2 = 2030e3
    ! real*8 :: qp1 = 0.0, qp2 = -23.4e3
    ! real*8 :: psat = 1e5

    real*8 :: pinfty1 = 4e8,pinfty2 = 0.0
    real*8 :: cp1 = 2534.00, cp2 = 2005.00
    real*8 :: cv1 = 1077.7,cv2 = 1956.45
    real*8 :: gm1 = 2.35,gm2 = 1.025
    real*8 :: q1 = -755.269e3,q2 = -237.547e3
    real*8 :: qp1 = 0.0,qp2 = -24.4e3



    ! Coefficients
    real*8 :: adum,bdum,cdum,ddum
    real*8:: tdum_old,tdum1

    ! Loop variables
    integer :: i, iter


    ! Array variables
    real*8,DIMENSION(:),ALLOCATABLE :: p_arr,t_arr
    real*8 :: pstart = 1e5, pend = 26e5
    integer :: nsize = 100
    real*8 :: diff



    ! Seting up arrays
    diff = (pend-pstart)/(nsize-1)

    ALLOCATE(p_arr(nsize))
    ALLOCATE(t_arr(nsize))

    p_arr(1) = pstart

    do i = 2,nsize
         p_arr(i) = p_arr(i-1) + diff
    end do


    !calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    bdum = (q1-q2)/(cp2-cv2)
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)


   

    ! main iter solver

    ! print *, -log(psat+pinfty2),  -adum-(bdum/500.)-cdum*log(500.)-ddum*log(psat+pinfty1)
    tdum_old = 300.00    


    do i = 1,nsize    
        do iter = 1,100
        t_arr(i) = bdum/(log(p_arr(i)+pinfty2)-adum-cdum*log(tdum_old)-ddum*log(p_arr(i)+pinfty1))
        if(abs((tdum1-tdum_old)/tdum_old) .lt. 1e-5) goto 10
        tdum_old = t_arr(i)
        enddo
        
        10   print *, "TSAT = ", t_arr(i), "PSAT = ", p_arr(i)/1e5, " ATM"  
        ! print *, -log(p_arr(i)),  -adum-(bdum/t_arr(i))-cdum*log(t_arr(i))-ddum*log(p_arr(i)+pinfty1)
    end do

    ! p_arr(:) = p_arr(:)/1e5

    do i = 1,nsize
        print*, p_arr(i), t_arr(i)
    end do


   

  








end program tsat

