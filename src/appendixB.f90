subroutine appendixB(idum,jdum,kdum)
    ! CALCULATE APPENDIX B WITHIN IF
    use globalvar
    use gridvariables
    use mieeos5eqn_ntemp
    use RungKutt3
    use appendixBvar

    integer, intent(in) :: idum,jdum,kdum

    real :: e_scale 


    ! CALCULATE Ck
    coeff_C(1) = (1.00-(matprop(2,1)*T(idum,jdum,kdum,1)))/&
        (matprop(2,1)*(p(idum,jdum,kdum)+matprop(3,1)))

    coeff_C(2) = (1.00-(matprop(2,2)*T(idum,jdum,kdum,2)))/&
        (matprop(2,2)*(p(idum,jdum,kdum)+matprop(3,2)))




    !!  GIBBS 1 

    !!! CALCULATE
    gibbsdum1(1) = (matprop(2,1)*matprop(1,1)-matprop(9,1))*T(idum,jdum,kdum,1)

    gibbsdum2(1) = matprop(1,1)*T(idum,jdum,kdum,1)
  
    !! DIMENSIONAL 
    !T(idum,jdum,kdum,1) = T(idum,jdum,kdum,1)*Tempscale    !T 

    !p(idum,jdum,kdum) = p(idum,jdum,kdum)*pscale     ! P 

    !matprop(3,1) = matprop(3,1)*pscale               ! Pinfty   

    !matprop(1,1) = matprop(1,1)*cvscale              ! CV 

    !matprop(8,1) = matprop(8,1)*(cvscale/Tempscale)  
    !matprop(9,1) = matprop(9,1)*cvscale               ! Qp 
 
    !! LOG TERM 
    gibbsdum3(1) = log((T(idum,jdum,kdum,1)**(matprop(2,1)))/&
        ((p(idum,jdum,kdum)+matprop(3,1))**(matprop(2,1)-1.00)))

    !! CONVERT BACK 
    !T(idum,jdum,kdum,1) = T(idum,jdum,kdum,1)/Tempscale    !T 

    !p(idum,jdum,kdum) = p(idum,jdum,kdum)/pscale     ! P 

    !matprop(3,1) = matprop(3,1)/pscale               ! Pinfty   

    !matprop(1,1) = matprop(1,1)/cvscale              ! CV 

    !matprop(8,1) = matprop(8,1)/(cvscale/Tempscale)  
    !matprop(9,1) = matprop(9,1)/cvscale              ! Qp 
    
    !! FINAL GIBBS 
    gibbs(1)= gibbsdum1(1)-(gibbsdum2(1)*gibbsdum3(1))+matprop(8,1)         












    !! GIBBS 2

    !!! CALCULATE      
    gibbsdum1(2) = (matprop(2,2)*matprop(1,2)-matprop(9,2))*T(idum,jdum,kdum,2)

    gibbsdum2(2) = matprop(1,2)*T(idum,jdum,kdum,2)
    
    !! DIMENSIONAL   
    !T(idum,jdum,kdum,2) = T(idum,jdum,kdum,2)*Tempscale    !T 

    !p(idum,jdum,kdum) = p(idum,jdum,kdum)*pscale     ! P 

    !matprop(3,2) = matprop(3,2)*pscale               ! Pinfty   

    !matprop(1,2) = matprop(1,2)*cvscale              ! CV 

    !matprop(8,2) = matprop(8,2)*(cvscale/Tempscale)  
    !matprop(9,2) = matprop(9,2)*cvscale               ! Qp 
 
    !! LOG TERM 
  
    gibbsdum3(2) = log((T(idum,jdum,kdum,2)**(matprop(2,2)))/&
        ((p(idum,jdum,kdum)+matprop(3,2))**(matprop(2,2)-1.00)))
  
      
    !!! CONVERT BACK
    !T(idum,jdum,kdum,2) = T(idum,jdum,kdum,2)/Tempscale    !T 

    !p(idum,jdum,kdum) = p(idum,jdum,kdum)/pscale     ! P 

    !matprop(3,2) = matprop(3,2)/pscale               ! Pinfty   

    !matprop(1,2) = matprop(1,2)/cvscale              ! CV 

    !matprop(8,2) = matprop(8,2)/(cvscale/Tempscale)  

    !matprop(9,2) = matprop(9,2)/cvscale               ! Qp 

    
    !! FINAL GIBBS 
    gibbs(2)= gibbsdum1(2)-(gibbsdum2(2)*gibbsdum3(2))+matprop(8,2)         
      




    !! NON DIM THE GIBBS 

    e_scale = pscale/rscale
    gibbs(1) = gibbs(1)/e_scale 
    gibbs(2) = gibbs(2)/e_scale 
  
    !!!! ---------------------- END DIM EVERYTHING ----------------------------------------- !!!!


    ! CALCULATE Dk
    coeff_D(1) = (matprop(8,1)-gibbs(1))/T(idum,jdum,kdum,1)
    coeff_D(2) = (matprop(8,2)-gibbs(2))/T(idum,jdum,kdum,2)


    ! Calculate phasic rho
    phasic_rho(1) = rhomat(idum,jdum,kdum,1)/(phi(idum,jdum,kdum,1))
    phasic_rho(2) = rhomat(idum,jdum,kdum,2)/(phi(idum,jdum,kdum,2))



    ! Calculate phasic c
    phasic_c(1) = sqrt((matprop(2,1)*(p(idum,jdum,kdum)+matprop(3,1)))/phasic_rho(1))
    phasic_c(2) = sqrt((matprop(2,2)*(p(idum,jdum,kdum)+matprop(3,2)))/phasic_rho(2))   

  
    ! Calculate phasic enthalpy, h = CpT

    phasic_h(1) = matprop(10,1)*T(idum,jdum,kdum,1)
    phasic_h(2) = matprop(10,2)*T(idum,jdum,kdum,2)


    !***************** DUMMY VARIABLES  ***************************!

    !Calculate Adum1, 2nd bracket term in A coefficient = 1st coeff for A' = (gammak-1)/(rhok*ck^2)

    coeff_Adum1(2) = (matprop(2,2)-1)/(matprop(2,2)*(p(idum,jdum,kdum)+matprop(3,2)))
    coeff_Adum1(1) = (matprop(2,1)-1)/(matprop(2,1)*(p(idum,jdum,kdum)+matprop(3,1)))


    !Calculate Adum2, last two terms in A coefficient

    coeff_Adum2(1) = 1.00/(matprop(1,1)*matprop(2,1)*(rhomat(idum,jdum,kdum,1)))
    coeff_Adum2(2) = 1.00/(matprop(1,2)*matprop(2,2)*(rhomat(idum,jdum,kdum,2)))



    !  Coeff Bdum1, 2nd bracket term in B coefficient
    coeff_Bdum1(2) = ((fluidprop(1)*(p(idum,jdum,kdum)+fluidprop(2)))/phasic_rho(2))-&
        ((fluidprop(1)-1)*phasic_h(2))


    coeff_Bdum1(1) = ((fluidprop(1)*(p(idum,jdum,kdum)+fluidprop(2)))/phasic_rho(1))-&
        ((fluidprop(1)-1)*phasic_h(1))


    !   Coeff Bdum2, last bracket term in B coefficient

    coeff_Bdum2(2) = 1.00/(matprop(1,2)*matprop(2,2)*(matprop(2,2)-1)&
                    *phasic_rho(2))


    coeff_Bdum2(1) = 1.00/(matprop(1,1)*matprop(2,1)*(matprop(2,1)-1)&
                    *phasic_rho(1))


    ! third bracket terms in A prime
  
    coeff_Aprime_dum(2) = (1.00+((coeff_D(2))/(matprop(1,2)*matprop(2,2))))*&
                            (1.00/(rhomat(idum,jdum,kdum,2)))

    coeff_Aprime_dum(1) = (1.00+((coeff_D(1))/(matprop(1,1)*matprop(2,1))))*&
                            (1.00/(rhomat(idum,jdum,kdum,1)))


    ! last bracket term in B prime
    coeff_Bprime_dum(2) = (1.00+((coeff_D(2))/(matprop(1,2)*matprop(2,2))))*&
                            (1.00/((matprop(2,2)-1.00)*phasic_rho(2)))

    coeff_Bprime_dum(1) = (1.00+((coeff_D(1))/(matprop(1,1)*matprop(2,1))))*&
                            (1.00/((matprop(2,1)-1.00)*phasic_rho(1)))         


    !***************** CALCULATE COEFF A ***************************!
    coeff_A = -(coeff_C(1)-coeff_C(2))*fluidprop(1)*(p(idum,jdum,kdum)+fluidprop(2))*&
            (coeff_Adum1(1)-coeff_Adum1(2))+coeff_Adum2(1)+coeff_Adum2(2)


    !***************** CALCULATE COEFF B ***************************!

    coeff_B = -((coeff_C(1)-coeff_C(2))*rho(idum,jdum,kdum))*(coeff_Bdum1(1)-coeff_Bdum1(2))&
                 -rho(idum,jdum,kdum)*(fluidprop(1)-1)*(phasic_h(1)-phasic_h(2))&
                    *(coeff_Bdum2(1)-coeff_Bdum2(2))



    !***************** CALCULATE COEFF A PRIME ***************************!


    Aprime_first = ((coeff_D(1)*coeff_C(1))-(coeff_D(2)*coeff_C(2))) &
                    *fluidprop(1)*(p(idum,jdum,kdum)+fluidprop(2))


    Aprime_first = Aprime_first*(coeff_Adum1(1)-coeff_Adum1(2))
    
    Aprime_second = -coeff_Aprime_dum(1)-coeff_Aprime_dum(2)

    coeff_Aprime = Aprime_first + Aprime_second 

    !***************** CALCULATE COEFF B PRIME ***************************!


    Bprime_first = (coeff_D(1)*coeff_C(1)-coeff_D(2)*coeff_C(2))*rho(idum,jdum,kdum)*&
      (coeff_Bdum1(1)-coeff_Bdum1(2))

    Bprime_second =  rho(idum,jdum,kdum)*(fluidprop(1)-1)*(phasic_h(1)-phasic_h(2))*&
            (coeff_Bprime_dum(1)+coeff_Bprime_dum(2))


    coeff_Bprime = Bprime_first + Bprime_second
   

    PRINT*, '-------------------------------------------------------------------------------------------'
    print*, "APPENDIX B at t = 0" 
    print*, ""
    PRINT*,"APPENDIX B VARIABLES AT i = ", idum 
    PRINT*, "MAJOR" 
    PRINT*, "A  ", coeff_A 
    PRINT*, "B  ", coeff_B  
    PRINT*, "APRIME ", coeff_Aprime , " = ", Aprime_first, " + ", Aprime_second 
    PRINT*, "BPRIME", coeff_Bprime, " = ", Bprime_first, " + ", Bprime_second   
    PRINT*, "" 
    PRINT*, "MINOR"
    PRINT*, "C1 C2 ", coeff_C(1), coeff_C(2) 
    PRINT*, "D1 D2 ", coeff_D(1), coeff_D(2)
    PRINT*, "NON DIM GIBBS LIQUID GIBBS VAPOR", gibbs(2), gibbs(1)
    PRINT*, "DIM GIBBS LIQUID GIBBS VAPOR", gibbs(2)*(cvscale*Tempscale), gibbs(1)*(cvscale*Tempscale)
    PRINT*, "T1 T2", T(idum,jdum,kdum,1), T(idum,jdum,kdum,2) 

    

    





end subroutine appendixB
