!***********************************************************************
      subroutine Stiff_shockstates(lpp,pjmp,uleft,rleft,Tleft,ms)
!***********************************************************************
! This subroutine sets the shocked states for a STIFFENED EOS


! pull required variables from respective modules
      use globalvar

      integer, intent(in) :: lpp   !Input as imat
      real, intent(inout) :: pjmp
      real, intent(inout) :: uleft,rleft,Tleft,ms

! create local variables (tmp is variable to store value temporarily)
      real :: tmp
      real :: pinf,gam,rright
      real :: T2T1,Tright

! set right state flow properties (ahead of shock)
      rright = matprop(4,lpp)
      Tright = 1.0

! claculate post shock properties
      gam = matprop(2,lpp)
      pinf = matprop(3,lpp)

      ms = sqrt( 1.0 + ( 0.5*(gam+1.0)/gam )* &
              &(pjmp-1.0)*initpres/(initpres+pinf) )

      tmp = (gam+1.0)*ms*ms/( (gam-1.0)*ms*ms + 2.0)

      rleft = rright*tmp

      uleft = ms*sqrt(gam*(initpres+pinf)/rright)*(1.0 - 1.0/tmp)

      T2T1 = (1 + (ms*ms-1.0)*2.0*gam/(gam+1.0))*&
              &(2.0 + (gam-1.0)*ms*ms)/((gam+1)*ms*ms)

      Tleft = Tright*T2T1

      end subroutine Stiff_shockstates
!***********************************************************************
