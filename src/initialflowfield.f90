!***********************************************************************
      subroutine initialflowfield()
!***********************************************************************
! This subroutine sets the initial flowfield of simulation at t=0
! Note 5-equation model is implicitly assumed on
! Note rhomat = rho*phi is implicitly assumed on

! pull required variables from respective modules
      use globalvar 
      use gridvariables
      use shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: rhoY

! create local variables (tmp is variable to store value temporarily)
      real :: rleft, uleft, vleft, pleft, Tleft,cleft,mleft
      real :: rright,uright, vright, pright, Tright,cright,mright
      real :: mach,pinf,gam,T2T1,deltabar,rem
      real :: pjump, wleft, wright
      integer :: zz,ishift,izmax,initflag
      real :: contactpoint

      real  :: rho_liquid_dum = 0.0, rho_vapor_dum = 0.0

      real :: e_liquid_dum, e_vapor_dum 
      integer :: iterTemp 

      integer :: idum, imatdum 

      write(6,*),'___________________________&
                 _______________________________________________'
      write(6,*),' '
      write(6,*),'SETTING INITIAL FLOWFIELD AT T=0'



! imat is material number that corresponds to the medium
      imat = nshock

! set right state flow properties (ahead of shock)
      pright = initpres
      uright = 0.00 
      ! uright = 2.00/vscale !! For Saurel Fig 15 
      vright = 0.00
      wright = 0.00
      Tright = 1.0
      rright = matprop(4,imat)
      !rright = matprop(4,1)

! calculate left state flow properties (behind shock)
! PJUMP = pressure jump across shock
      uleft = 0.00
      ! uleft = -2.00/vscale  !! For Saurel Fig 15
      vleft = 0.00
      wleft = 0.00
      pleft = pshock/pscale
      pjump = pleft/pright


!_______________________________________________________________________
!**************  BEGIN CALCULATION FOR POST SHOCK PROPERTIES ***********
      if (eos(imat) == 1) then 
! sets post shock prop with stiffened eos
        call Stiff_shockstates(imat,pjump,uleft,rleft,tleft,mach)
        gam = matprop(2,imat)
        pinf = matprop(3,imat)
        cleft=sqrt( gam*(pleft+pinf)/rleft)
        cright=matprop(7,imat)
        mleft=uleft/cleft
        mright=uright/cright
        Us = mach*cright
        taus = lscale/(Us*vscale)
      !   STOP 
      elseif (eos(imat) == 2) then 
! sets post shock properties using Mie-G
        call MG_shockstates(imat,pshock,uleft,rleft,tleft,Us,cleft)
        uleft = uleft/vscale
        rleft = rleft/rscale
        tleft = tleft/tempscale
        taus = lscale/Us
        Us = Us/vscale
        cleft = cleft/vscale
        cright = matprop(7,imat)
        mleft=uleft/cleft
        mright=uright/cright
      endif
!**************  END CALCULATION FOR POST SHOCK PROPERTIES *************
!_______________________________________________________________________



!_______________________________________________________________________
!************** CALCULATE NORMALIZER, TAU TO OUTLET BOUNDARY ***********
! the nondim tau required for shock to hit outlet boundary
! if you are doing 1D problem with shock 
! in x,y, or z directions use
!      tmp = (amax1(xmax,ymax,zmax)-xshock)/Us
! otherwise for 2D or 3D problem, we usually set shock in x-directions
! so for this set up use 
!      tmp = (xmax-xshock)/Us
      tmp = (amax1(xmax,ymax,zmax)-xshock)/Us
      tautooutlet = tmp*tscale/taus
      ! tautooutlet = tmp*tscale
      print*,'tautooutlet is ',tautooutlet
! Calculate normalizer used to calculate Cd 
! in writedrag_singleparticle.f90
! radius is the radius of the particle
! norml is based on projected area being a circle for GEOMETRY==2
      if (num_particles.gt.0) then
        do n=1,num_particles
#if GEOMETRY==1
#if probdim==1
! norml is based on projected area = d_p for 1D cartesian
          norml(n) = 0.5*rleft*uleft*uleft*2.0*radius(n)
#endif
#if probdim==2
! norml is based on projected area being a circle
          norml(n) = 0.5*rleft*uleft*uleft*delz*2.0*radius(n)
#endif

#else

! norml is based on projected area being a circle
          norml(n) = 0.5*rleft*uleft*uleft*pie*radius(n)*radius(n)
#endif
        enddo
      endif
!********** END CALCULATE NORMALIZER, TAU TO OUTLET BOUNDARY ***********
!_______________________________________________________________________



!_______________________________________________________________________
!************** DISPLAY CALCULATED VARIABLES IN TERMINAL ***************
      write(6,*)' '
      write(6,*)'  TAUS (s)       :',taus
      write(6,*)'  TAUS to outlet :',tautooutlet
      write(6,*)' '
      if (num_particles.gt.0) then
        do n=1,num_particles
          write(6,*)'  particle                    :',n
          write(6,*)'     DRAG NORMALIZER (nondim) :',norml(n)
        enddo
        write(6,*)' '
      endif
      write(6,*)'  SHOCK Speed (m/s) :', Us*vscale
      write(6,*)'  SHOCK Mach #      :', Us/cright
      write(6,*)' '
      write(6,*)'  PRE-SHOCK VALUES'
      write(6,*)'     Rho (kg/m3)       :', rright*rscale
      write(6,*)'     U (m/s)           :', uright*vscale
      write(6,*)'     V (m/s)           :', vright*vscale
      write(6,*)'     W (m/s)           :', wright*vscale
      write(6,*)'     P (Pa)            :', pright*pscale
      write(6,*)'     T (K)             :', Tright*Tempscale
      write(6,*)'     Mach #            :', mright
      write(6,*)'     Sound speed (m/s) :', cright*vscale
      write(6,*)' '
      write(6,*)'  POST-SHOCK VALUES:'
      write(6,*)'     Rho (kg/m3)       :', rleft*rscale
      write(6,*)'     U (m/s)           :', uleft*vscale
      write(6,*)'     V (m/s)           :', vleft*vscale
      write(6,*)'     W (m/s)           :', wleft*vscale
      write(6,*)'     P (Pa)            :', pleft*pscale
      write(6,*)'     T (K)             :', Tleft*Tempscale
      write(6,*)'     Mach #            :', mleft
      write(6,*)'     Sound speed (m/s) :', cleft*vscale
      write(6,*)' '
!************ END DISPLAY CALCULATED VARIABLES IN TERMINAL *************
!_______________________________________________________________________



!_______________________________________________________________________
!************ PRINT OUT VALUES AND PROBLEM SETUP ***********************
      write(21,*)'% nx, ny, nz'
      write(21,*) ni
      write(21,*) nj
      write(21,*) nk
      write(21,*)'% dx, dy, dz'
      write(21,*) delx
      write(21,*) dely
      write(21,*) delz
      write(21,*)'% Points across diameter: xdir'
      write(21,*) int(1./delx)
      write(21,*)'% geom, xmin, xmax, ymin, ymax, zmin, zmax'
      write(21,*) geom
      write(21,*) xmin
      write(21,*) xmax
      write(21,*) ymin
      write(21,*) ymax
      write(21,*) zmin
      write(21,*) zmax
      write(21,*)'% Scalings: length, time, rho, vel, pres, temp'
      write(21,*) lscale
      write(21,*) tscale
      write(21,*) rscale
      write(21,*) vscale
      write(21,*) pscale
      write(21,*) Tempscale
      write(21,*)'%  PRE-SHOCK VALUES: rright,uright,', &
                 'vright,pright,Tright,cright,Mright'
      write(21,*) rright*rscale
      write(21,*) uright*vscale
      write(21,*) vright*vscale
      write(21,*) pright*pscale
      write(21,*) Tright*Tempscale
      write(21,*) cright*vscale
      write(21,*) mright
      write(21,*)'%  POST-SHOCK VALUES: rleft,uleft,vleft,',&
                                 'pleft,Tleft,cleft,Mleft'
      write(21,*) rleft*rscale
      write(21,*) uleft*vscale
      write(21,*) vleft*vscale
      write(21,*) pleft*pscale
      write(21,*) Tleft*Tempscale
      write(21,*) cleft*vscale
      write(21,*) mleft
      write(21,*)'%  Shock speed, Mshock, taus, CD_normalizer'
      write(21,*) Us*vscale
      write(21,*) Us/cright
      write(21,*) taus
      if (num_particles.gt.0) then
        do n=1,num_particles
          write(21,*) norml(n)
        enddo
      endif
!************** END PRINT OUT VALUES AND PROBLEM SETUP *****************
!_______________________________________________________________________



!_______________________________________________________________________
!******************* SET INITIAL PHI FIELD *****************************
#if ROCPACK==1
! call function that reads in phi field from rocpack
      call readrocpack
#else
      do k = 1,NK
      do j = 1,NJ
      do i = 1,NI

      ! do k=(-NG+1),NK+NG
      ! do j=(-NG+1),NJ+NG
      ! do i=(-NG+1),NI+NG
#if GEOMETRY==1
       
         contactpoint = 0.75    !! for Saurel Fig 10
      !    contactpoint = 0.5       !! for Saurel Fig 15 expansion tube
         
      !    print*, xc(i,j,k), contactpoint
      !    stop
         
      !    tmp = xc(i,j,k)-contactpoint

         

         !! ORIGINAL PHI 
      !    phi(i,j,k,1) = 1.0-(0.5+0.5*tanh(tmp/lsphi))
      !    phi(i,j,k,2) = 1.0-phi(i,j,k,1)
     
         !!!PHI SAUREL FIXED (2 and 1 SWITCH)
      !    phi(i,j,k,2) = 1.0-(0.5+0.5*tanh(tmp/lsphi))
      !    phi(i,j,k,1) = 1.0-phi(i,j,k,2)



      !! WORKING FOR SAUREL FIG 10
      !    IF (xc(i,j,k) .LE. contactpoint) then 
      !       phi(i,j,k,2) = 1.00
      
      !    ELSE 
      !       phi(i,j,k,1) =  1.00-phi(i,j,k,2)
      !    END IF
         

      !    phi(i,j,k,1) = 1.00
      !    phi(i,j,k,2) = 1e-8

         IF (xc(i,j,k) .LE. 0.75) then 
            phi(i,j,k,2) = 1.00
            
      
         ELSE 
            phi(i,j,k,2) =  1e-8
         END IF

         phi(i,j,k,1) = 1.00 - phi(i,j,k,2)
         




     

      !    tiny = 1e-2    !! For Saurel Fig 15 
     
     
      !    IF (xc(i,j,k) .LE. contactpoint) then 
      !       phi(i,j,k,2) = 1.00
      
      !    ELSE 
      !       phi(i,j,k,1) =  1e-2
      !    END IF
     
     
          
      




       

   
      


      ! PREVENT PHI FROM GOING NEGATIVE    
      do imat = 1,nmat
            if (phi(i,j,k,imat)==0.0) then
                  phi(i,j,k,imat) = tiny
            else 
                  phi(i,j,k,imat) = phi(i,j,k,imat) - tiny
            end if

      end do


#else
! Following for input_AirAL_stiffm6.txt and input_AirAL_stiffm122.txt
! sphere

         do n=1,num_particles
            tmp = sqrt( ((xc(i,j,k)-xc0(n))**2)+&
              & ((yc(i,j,k)-yc0(n))**2)+&
              & ((zc(i,j,k)-zc0(n))**2))-radius(n)
            phi(i,j,k,1) = (0.5+0.5*tanh(tmp/lsphi))
            phi(i,j,k,2) = 1.000 - phi(i,j,k,1)
         enddo

! for inputfile petn01.txt
         Phi(i,j,k,1)=1.00



#endif
      enddo 
      enddo
      enddo
#endif

#if ROCPACK==0
       write(6,*)' Adjusting Phi-Field to grid'
       do n=1,20
        rho = 0.0
        rhomat = 0.0
        call shukla2010five
        write(6,*)'   iter = ',n,', max_phi_res = ',resphi
        if (resphi.le.tol) goto 22
        enddo
  22    continue
       write(6,*)' Finished Adjusting Phi-Field to grid'
       write(6,*)' '
#endif
!******************* END SET INITIAL PHI FIELD *************************
!_______________________________________________________________________



!_______________________________________________________________________
!********************* SET INITIAL FLOW FIELD **************************
!
! 1st set the variables in the shocked/unshocked region
! Note 5-equation model is implicitly assumed on 
! Note rhoalpha (rhomat = rho*phi) is implicitly assumed on
!     In DrZhang's code, if 5-eqn is on then mixture density is  
!     recalculated as the sum of rhomat*phi
      T=1.0
      do k = 1,NK
      do j = 1,NJ
      do i = 1,NI
      
      ! do k=(-NG+1),NK+NG
      ! do j=(-NG+1),NJ+NG
      ! do i=(-NG+1),NI+NG
! set T=1 since it is a dervied variable




        !! UNIFORM CASE SETTINGS 
        !! Setting Pressure 

        !p(i,j,k) = 1e6/pscale  


        !! Set up temperature 
        !T(i,j,k,1) = 307/Tempscale        !Tvapor = Tsat
        !T(i,j,k,2) = 310/Tempscale        !Tliquid > Tsat  


        !! Calculate RHO

        !e_liquid_dum = T(i,j,k,2)*matprop(1,2) 
        !rho_liquid_dum = (p(i,j,k)+matprop(2,2)*matprop(3,2))/&
                    !((matprop(2,2)-1.0)*(e_liquid_dum-matprop(8,2))) 


        !e_vapor_dum = T(i,j,k,1)*matprop(1,1) 
        !rho_vapor_dum = (p(i,j,k)+matprop(2,1)*matprop(3,1))/&
                    !((matprop(2,1)-1.0)*(e_vapor_dum-matprop(8,1))) 



        !!! Calculate rhomat 
        !rhomat(i,j,k,1) = rho_vapor_dum*phi(i,j,k,1)
        !rhomat(i,j,k,2) = rho_liquid_dum*phi(i,j,k,2)

        !!!Backing out Rho at t = 0 

        !rho(i,j,k) = rhomat(i,j,k,1) + rhomat(i,j,k,2) 

        do lp = 1,nmat ! note rhoalpha is implicitly on
          rhomat(i,j,k,lp)= matprop(4,lp)*phi(i,j,k,lp)
        enddo
        
        
        if (xc(i,j,k) .le. xshock) then
      !     rho(i,j,k)      = rleft
          !MAX, PHASE TRANS
          rho(i,j,k)      = 500/rscale  !! original saurel fig 10

      !     rho(i,j,k)      = 607/rscale

      !     rho(i,j,k)      = 1150/rscale  !! FOR SAUREL FIG 15
          !rho(i,j,k)      = rhomat(i,j,k,1)+rhomat(i,j,k,2)  !! UNIFORM 
          u(i,j,k)        = 0 !original 
      !     u(i,j,k)        = -2.00/vscale !! FOR SAUREL FIG 15
          v(i,j,k)        = vleft
          w(i,j,k)        = wleft
          p(i,j,k)        = pleft
          !p(i,j,k)        = 1e6/pscale     !! UNIFORM 
          !rhomat(i,j,k,nshock) = rleft
          !MAX, PHASE TRANS
          rhomat(i,j,k,nshock) = 500/rscale  !! original saurel 

      !     rhomat(i,j,k,nshock) = 607/rscale


      !     rhomat(i,j,k,nshock) = 1150/rscale  !! FOR SAUREL FIG 15


        else
      !     u(i,j,k) = uright
          u(i,j,k) = 0  !!original 
          v(i,j,k) = vright
          w(i,j,k) = wright
          p(i,j,k) = pright
          !p(i,j,k) = 1e6/pscale         !! UNIFORM  

          !rho(i,j,k)      = rhomat(i,j,k,1)+rhomat(i,j,k,2)   !! UNIFORM 
          
      !     rho(i,j,k) = 0.0
          rho(i,j,k) = 2.00/rscale !! Max, Phase, oriignal saurel

      !     rho(i,j,k) = 1e-4/rscale 

      !     rho(i,j,k) = 1150/rscale  !! FOR SAUREL FIG 15



          do lp = 1,nmat 
            rho(i,j,k)= rho(i,j,k) + rhomat(i,j,k,lp)
          enddo


#if  REACT==1 
        !t0  = 1.0+0.1*exp(-0.5*(xc(i,j,k)**2)/39.47/39.47)
        !cv=(1.+5.3*2.94e4)/4.3
        !rho(i,j,k) = (1.0+5.3*2.94e4)/(t0*cv*4.3) 


! for petn01.txt
         del = 40.0e-6/lscale
        rho(i,j,k) = (1.0-0.2*exp(-0.5*(xc(i,j,k)**2)/del/del))*&
        & matprop(4,1)

        !rho(i,j,k) = 1.0-0.08*exp(-0.5*(xc(i,j,k)**2)/2.0/2.0)


        rhomat(i,j,k,1) = rho(i,j,k)*phi(i,j,k,1)
        rhomat(i,j,k,2) = rho(i,j,k)*phi(i,j,k,2)

! uncomment following eqn if you are doing pulse case?
!          P(i,j,k) = 1.0+(1.0e4)*exp(-0.5*(xc(i,j,k)**2)/4.0/4.0)
#else
          rho(i,j,k) = 0.0
          do lp = 1,nmat 
            rho(i,j,k)= rho(i,j,k) + rhomat(i,j,k,lp)
          enddo
#endif
        endif
      enddo
      enddo
      enddo



!
      call BC(1)
!****************** END SET INITIAL FLOW FIELD *************************
!_______________________________________________________________________

!_______________________________________________________________________
!*********************** SET SPECIES FIELD *****************************
#if REACT==1
      do loop=1,nspec
      do k = 1,NK
      do j = 1,NJ
      do i = 1,NI
        rhoY(i,j,k,loop)=phi(i,j,k,1)*rho(i,j,k)
      enddo 
      enddo 
      enddo
      enddo
#endif
!*********************** END SET SPECIES FIELD *************************
!_______________________________________________________________________



      call updatelastphi
      timeT = 0.00
      giter = 0


#if ROCPACK==1
         initflag=0
#else
      initflag=1
      izmax=5
#endif


      if(initflag==1) then
        print*,"doing initial shukla scheme: "
        do zz=1,izmax
#if   INTERFACECORRECTION==0
      print*, 'CURRENTLY CALLING SHUKLA2010FIVE'

          call shukla2010five
#elif INTERFACECORRECTION==1
          call secant_mass_zz()
#elif INTERFACECORRECTION==2
          call secant_volmass_zz()
#endif
        print*,'zz, resphi : ',zz,resphi
        if (resphi.le.tol) goto 11
        enddo
  11    continue
        print*,' '
        call updatelastphi
      endif


! set phi start field for particle
      if(num_particles.gt.0)then
        print*,'assigning phistart : '
        do n=1,num_particles
          phistart(:,:,:,n) = phi(:,:,:,nmed+n)
        enddo
      endif

#if ROCPACK==1
      iter_reinit = 0
#endif


! get the center coord of original field
      if(num_particles.gt.0) then
      do lp=1,num_particles
        xtmp = 0.0000
        ytmp = 0.0000
        ztmp = 0.0000
        pmass(lp) = 0.0000
        do k=1,NK
        do j=1,NJ
        do i=1,(NI)
           pmass(lp) = pmass(lp) + rhomat(i,j,k,lp+nmed)*vol(i,j,k)
           xtmp = xtmp + rhomat(i,j,k,lp+nmed)*vol(i,j,k)*xc(i,j,k)
           ytmp = ytmp + rhomat(i,j,k,lp+nmed)*vol(i,j,k)*yc(i,j,k)
           ztmp = ztmp + rhomat(i,j,k,lp+nmed)*vol(i,j,k)*zc(i,j,k)
        enddo
        enddo
        enddo !k=1,NK
        partcentr_old(1,lp) = xtmp/pmass(lp)
        partcentr_old(2,lp) = ytmp/pmass(lp)
        partcentr_old(3,lp) = ztmp/pmass(lp)
      enddo
      endif


#if   INTERFACECORRECTION==0
         write(6,*)' *** No Correction; Five eqn'
         write(21,*)'% No Correction; Five eqn'
         call shukla2010five
        call updatelastphi

#elif INTERFACECORRECTION==1
      write(6,*)' *** Mass Correction'
      call secant_mass_zz()

#elif INTERFACECORRECTION==2
      write(6,*)' *** Mass and Volume Correction'
      write(21,*)'% Mass and Volume Correction'
      call secant_volmass_zz()


#elif INTERFACECORRECTION==3
      write(6,*)' *** Mass, Volume, and Shape Correction'
      write(21,*)'% Mass, Volume, and Shape Correction'
      call shapecorrect
      call secant_volmass_zz()

#endif

      write(6,*),' '
      if (iterT>0) call tempcorrection()


! apply boundary condition  
      call BC(1) 

      print*, 'FINISHED SETTING INITIAL FLOWFIELD AT T=0'
      print*, ' '
      print*, ' '

      close(21)

      end subroutine initialflowfield
!***********************************************************************
