!***********************************************************************
      subroutine MG_EOS(lp,p,v,temp)
!***********************************************************************
! This subroutine sets the states for a MG EOS
      use globalvar, only: matprop,rscale,vscale,cvscale,&
                           &pscale,Tempscale,initpres


!     Dimensional values

      rho00 = matprop(4,lp)*rscale
      c00 = matprop(7,lp)*vscale
      cv = matprop(1,lp)*cvscale
      s = matprop(6,lp)
      g = matprop(2,lp)-1.0
      p00 = initpres*pscale
      T00 = Tempscale

      e00 = cv*T00
      v00 = 1.0/rho00
!
!     compute e
!
      vv=v
      phi = 1.0d0-vv/v00
      pH = c00*c00*phi/((1.0d0-s*phi)**2)/v00 + p00
      f = pH*(1.0d0-0.5d0*g*(v00/vv-1.0d0)) &
          - g*p00*(v00-vv)/(2.0*vv)
      if (vv>v00) then
         f = c00*c00*(1.0/vv-1.0/v00) + p00
      endif
      e = e00 + (vv/g)*(p-f)

!     compute es
      es = 0.
      ddv = (v-v00)/199.
      do i = 1,199
         vv=v00+(i-1)*ddv
         phi = 1. - vv/v00
         phv = p00 + (c00**2)*phi/((1.-s*phi)**2)/v00
         fv = phv*(1.-0.5*G*(v00-vv)/vv)-0.5*G*p00*(v00-vv)/vv
         if (vv>v00) then
            fv = c00*c00*(1.0/vv-1.0/v00) + p00
         endif
         es = es + (fv-G*e00/vv)*(vv**G)*ddv
      enddo

      es = e00*((v00/v)**G) - es/(v**G)
      Ts = T00*((v00/v)**G)

      temp = (e-es)/cv+Ts
      

      end subroutine MG_EOS
!***********************************************************************
