subroutine solveSource6663(dtdum)

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var

    real, intent(in) :: dtdum


    ! Calculating Q1 and Ydot1 
      
    do i = 1,NI
    do j = 1,NJ
    do k = 1,NK
  
        q1 = -((coeff_Bprime)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B)) &
        *(1.00/dtdum)*(T(i,j,k,2)-T(i,j,k,1)) &
        + ((coeff_B)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
        *(1.00/dtdum)*(gibbs(2,i)-gibbs(1,i))



        ydot1 = -((coeff_Aprime)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
        *(1.00/dtdum)*(T(i,j,k,2)-T(i,j,k,1)) &
        + ((coeff_A)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
        *(1.00/dtdum)*(gibbs(2,i)-gibbs(1,i))

    end do      
    end do
    end do
  


end subroutine solveSource6663