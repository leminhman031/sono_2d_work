!***********************************************************************
      subroutine sloplim(slplmt,a,b,c)
!***********************************************************************
! pull required variables from respective modules
        use globalvar, only: nmat, sigspeed

! Declare the calling variables and specify their intent
        integer, intent(in) :: slplmt
        real, intent(in) :: a, b
        real, intent(out)  :: c


! ----------------------------------------------------------------------
!                           MINMOD SLOPE LIMITER
! ----------------------------------------------------------------------
! This section is MINMOD slope limiter (slplmt = 1)
      if (slplmt == 1) then
 
         if ( (a > 0) .and. (b > 0) ) then 

                if (a > b) then 
                      c = b
                else 
                   c = a
                endif

         elseif ( ( a < 0) .and. (b < 0) ) then 

                if (a < b) then 
                  c = b
                else 
                   c = a
                endif

         else
              c = 0
         endif 

      endif


! ----------------------------------------------------------------------
!                           Other Slope limiters
! ----------------------------------------------------------------------
! Following sections are for other slope limiters (to be added)


      end subroutine sloplim
!***********************************************************************
