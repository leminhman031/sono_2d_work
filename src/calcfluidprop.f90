!***********************************************************************
      subroutine calcfluidprop(stte,prop,ai,bj,ck,slct)
!***********************************************************************
! this function calculates fluid gamma and fluid p_infinity 
!     during every call 
! it also calculates fluid speed of sound when slct = 2
! it also calculates conserved energy rho*E and rho*E+P when 
!     slct = 1 or 2


! pull required variables from respective modules
      use globalvar
      use gridvariables
      use mieeos5eqn_ntemp
      

      real,  intent(in), dimension(6+nmat+nmat+nspec) :: stte    !   local to this subroutine
      real,  intent(inout), dimension(7) :: prop
      integer,  intent(in):: ai,bj,ck,slct

      

      ! ! MAX, phase transition variables

      real :: rho_internal_e
      real,dimension(1:nmat) :: yk,qk
      real :: checkpoint
      real :: rhomat1, rhomat2 

      real :: dumpinf1, dumpinf2 


      real :: toppinf2, bottompinf2 

      rho_internal_e = 0.0
      yqmix = 0.0
      yk = 0.0
      qk = 0.0
      checkpoint = 0.0
      



     

  

      

    !
! stte array elements: rho, u, v, w, p, T, 
!                      Phi_1 to Phi_(nmat),
!                      rhomat_1 to rhomat_(nmat),
!                      spec_1 to nspec (# of species)

!-----------------------------------------------------------------------
!     CALCULATING FLUID GAMMA AND FLUID P_INFINITY
!-----------------------------------------------------------------------
      dpidr = 0.0000

!     this part is for 5 eqn model with MieEOS, need to 
!     recalculate material P_infinity and dPIdr
!  NOTE - 5-eqn model and rhoalpha is assumed implicitly on
      do imat=1,nmat
        if(stte(6+imat).gt.0.0) then
          if (eos(imat)==2) then

            r0 = matprop(4,imat)
            c0 = matprop(7,imat)
            e0 = matprop(5,imat)
            p0 = initpress
            mies = matprop(6,imat)
            g = matprop(2,imat)
            r = stte(6+(nmat-1)+imat)/(6+imat)
            if (r > r0) then
            ! not expanded
              fi = 1.0-(r0/r)
              a = r0*c0*c0*fi/(1.0-mies*fi)/(1.0-mies*fi)
              b = (g-1.0)*(r-r0)/r0
              dadr = (r0/r/r)*( (a/fi) + 2.0*a*mies/(1.0-mies*fi) )
              dbdr = (g-1.0)/r0
              matprop(3,imat) = (1.0/g)*( (g-1.0)*r*e0 -p0 - &
              & a + p0*b + 0.5*a*b)
              b=1.0-0.5*(g-1.0)*(r-r0)/r0
              dbdr = -1.0*0.5*(g-1.0)/r0
              dpidr(imat) = (1.0/g)*( (g-1.0)*e0 - (a*dbdr + b*dadr) &
              & + (g-1.0)/r0)
            else
              matprop(3,imat) = (1.0/g)*&
                              &((g-1.0)*r*e0 - p0 - c0*c0*(r-r0))
              dpidr(imat) = (1.0/g)*( (g-1.0)*e0 - c0*c0 )
            endif
          endif
        endif
      enddo 

! calculate fluid gamma and pinf using mixture rule
! now that each material's p_infinity and dpidr is calculated
! this is Shuklas Eq 19b 19c
      dumgam = 0.00
      dumpinf = 0.00
      dpidrfluid = 0.00
      yqmix = 0.00



      !PRINT*,"RHOMAT1 = ",  stte(6+1+nmat),"RHOMAT2 = ",  stte(6+2+nmat)
      !PRINT*, "MIXTURE RHO = ", stte(1)


      do imat=1,nmat

        dumgam = dumgam + stte(6+imat)/( matprop(2,imat) - 1.0)

        !! This means: dumpinf = (phi1*gamma1*pinfty1)/(gamma-1)
        !dumpinf = dumpinf + &
            !& stte(6+imat)*matprop(2,imat)*matprop(3,imat)/(matprop(2,imat) - 1.0)
        dpidrfluid = dpidrfluid + stte(6+imat)*matprop(2,imat)*dpidr(imat)/&
            & (matprop(2,imat) - 1.0)




        !MAX, PHASE STUFF

        !yk(imat) = stte(6+nmat+imat)/(stte(6+nmat+1)+stte(6+nmat+2))
        yk(imat) = stte(6+nmat+imat)/(stte(1))
        !get qk
        qk(imat) = matprop(8,imat)

        !sum up for yqmix
        yqmix = yqmix + yk(imat)*qk(imat)

      
    enddo



    dumpinf1 = stte(6+1)*matprop(2,1)*matprop(3,1)/(matprop(2,1)-1.0)
    dumpinf2 = stte(6+2)*matprop(2,2)*matprop(3,2)/(matprop(2,2)-1.0)

    !print*, "Checking dumpinf2" 
    !print* ,"phi2", stte(6+2) 
    !print*, "gamma2", matprop(2,2)
    !print*, "pinfty2", matprop(3,2)
    
    dumpinf = dumpinf1 + dumpinf2 

      


! calculate fluid gamma and pinfinty

      

      prop(1) = (1+dumgam)/dumgam     ! fluid gamma>
      prop(2) = dumpinf/(1+dumgam)  ! fluid pinf
    
      !print*, ""
      !print*, "dummy properties for gamma and pinfty" 
      !print*, "dumpinf1 ", dumpinf1, "dumpinf2 ", dumpinf2 
      !print*, "dumpinf ", dumpinf 
      !print*, "fluid pinfty ", prop(2)
      !print*, "dumgamma", dumgam
      !print*, "fluid gamma", prop(1) 
      !print*, ""
      dpidrfluid=dpidrfluid/(1+dumgam) !fluid dpidrho
      


!-----------------------------------------------------------------------
!     END OF CALCULATING FLUID GAMMA AND FLUID P_INFINITY SECTION
!-----------------------------------------------------------------------



!-----------------------------------------------------------------------
!     SECTION FOR CALCULATING FLUID SPEED OF SOUND 
!-----------------------------------------------------------------------

     
      checkpoint = 0.0
      checkpoint = (prop(1)*(stte(5)+prop(2))/stte(1)-prop(1)*dpidrfluid) 


      if (slct==2)then
! prop(2) is fluid pinf and prop(1) is fluid gamma
! calculate fluid speed of sound
      if(checkpoint < 0 )then
         write(6,*), 'Imaginary Speed of Sound Error'
         write(6,*), '  Gamma Fluid is  ',prop(1)
         write(6,*), '  Dum Gamma is    ', dumgam
         write(6,*), '  Dum Pinfty is    ', dumpinf , " = Dumpinf1 ", dumpinf1 , " + Dumpinf2 ", dumpinf2 
         write(6,*), 'Dumpinf2 = ', toppinf2, " / ", bottompinf2  
         write(6,*), '  P infinity Fluid is   ',prop(2)
         write(6,*), '  dPinf/drho Fluid is   ',dpidrfluid
         write(6,*), '  Gamma_fluid*(P+P_inf_fluid)/Rho = ',&
                                           &(stte(5)+prop(2))/stte(1)

         write(6,*), 'Location   '
         write(6,*), '  i is ',ai
         write(6,*), '  j is ',bj
         write(6,*), '  k is ',ck
         write(6,*), '  xc(i,j,k) is  ',xc(ai,bj,ck)
         write(6,*), '  yc(i,j,k) is  ',yc(ai,bj,ck)
         write(6,*), '  zc(i,j,k) is  ',zc(ai,bj,ck)

         write(6,*), 'Input for sound speed calculation '
         write(6,*), '  Rho is  ',stte(1)
         write(6,*), '  U is  ',stte(2)
         write(6,*), '  V is  ',stte(3)
         write(6,*), '  W is  ',stte(4)
         write(6,*), '  P is  ',stte(5)
         do i=1,nmat
           write(istr,'(I6)') i
           write(6,*), '  MATERIAL '//trim(adjustl(istr))//' '
           write(6,*), '    Phi is  ',stte(6+i)
           write(6,*), '    dPinf/drho material is   ',dpidr(i)
           write(6,*), '    rho_mat is   ',stte(6+(nmat)+i)
         enddo

         write(6,*), '  MATERIAL '//trim(adjustl(istr))//&
                     &' EQUATION OF STATE DETAILS  '
         do i=1,nmat
          write(6,*), '    Material Tag           :   ',matstr(i)
          if(eos(i)==2) then
            write(6,*)'   Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(6,*)'   Gruneisen Gamma        :',matprop(2,i)-1.0
            write(6,*)'   Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(6,*)'   e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(6,*)'   Sound Speed (m/s)      :',matprop(7,i)*vscale
            write(6,*)'   Gruneisen-s            :',matprop(6,i)
          elseif(eos(i)==1) then
            write(6,*)'   Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(6,*)'   Stiffened Gamma        :',matprop(2,i)
            write(6,*)'   P_Infinity (Pa)        :',matprop(3,i)*pscale
            write(6,*)'   Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(6,*)'   e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(6,*)'   Sound Speed (m/s)      :',matprop(7,i)*vscale
          endif
          write(6,*),' '
         enddo

         write(6,*),'-------------------------------------------------'
         write(6,*),' Dimensional Scalings  '
         write(6,*),'  length scale (m)             : ',lscale
         write(6,*),'  pressure scale (Pa)          : ',pscale
         write(6,*),'  density scale (kg/m^3)       : ',rscale
         write(6,*),'  temperature scale (K)        : ',tempscale
         write(6,*),'  time scale (s)               : ',tscale
         write(6,*),'  velocity scale (m/s)         : ',vscale
         write(6,*),'  heat capacity scale (J/kg-K) : ',cvscale
         write(6,*),'-------------------------------------------------'
         stop
      
      endif
      !prop(3) = (prop(1)*(stte(5)+prop(2))/stte(1) &
            !& - prop(1)*dpidrfluid)    ! sqrt(       (gamma(p+pinfty)/rho)) - (gamma*dpinftydrho)   )
      
      prop(3) = prop(1)*(stte(5)+prop(2))/stte(1)
              

      !prop(3) = prop(3) - prop(1)*dpidrfluid 


      !print*, ""
      !print*, "SOUND SPEED CHECK "
      !print*, "gamma", prop(1), "p ", stte(5), "pinfty", prop(2) 
      !print*, "gamma(p+pinfty)", prop(1)*(stte(5)+prop(2))
      !print*, "rho", stte(1) 
      !print*, "prop(3) = ", prop(3)
      prop(3)=sqrt(prop(3))
      soundspeed(ai,bj,ck)=prop(3) 

      !print*, "SPEED SOUND, sqrt of prop(3) = ", prop(3)



      endif
!-----------------------------------------------------------------------
!     END OF CALCULATING FLUID SPEED OF SOUND SECTION
!-----------------------------------------------------------------------





! -----------------------------------------------------------------------
!     MAX PHASE TRANS: SECTION FOR yqmix, rhoE, rhoE + P
! -----------------------------------------------------------------------      

     
      
      
      
      if(slct==1 .or. slct==2)then
            
            !rho*e


            rho_internal_e = (stte(5) + stte(1)*yqmix*(prop(1)-1.0)+prop(1)*prop(2))/(prop(1)-1.0)


            !rhoE
            prop(4) =  rho_internal_e+0.5*stte(1)*(stte(2)**2+stte(3)**2 + stte(4)**2)


            !rhoE + P
            prop(5) = prop(4) + stte(5)      
            
            ! print*, prop(4), prop(5)




      end if

      


      


     

!-----------------------------------------------------------------------
!     MAX PHASE TRANS: END SECTION FOR yqmix
!-----------------------------------------------------------------------            








! !-----------------------------------------------------------------------
! !     SECTION FOR CALCULATING CONSERVED RHO*E AND RHO*E+P
! ! -----------------------------------------------------------------------
!       if(slct==1 .or. slct==2)then
! ! calculate fluid rhoE
      
!       prop(4) = (stte(5) + prop(1)*prop(2))/(prop(1)-1.0) + &
!                   & 0.5*stte(1)*(stte(2)**2 + stte(3)**2 + stte(4)**2)
!     ! calculate fluid rhoE+P
!         prop(5) = prop(4) + stte(5)


!       endif




     
     
! !-----------------------------------------------------------------------
! !     END OF CALCULATING CONSERVED RHO*E AND RHO*E+P
! !-----------------------------------------------------------------------











      end subroutine calcfluidprop
!***********************************************************************
