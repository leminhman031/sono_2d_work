subroutine phaseTrans()

    use globalvar
    use gridvariables
    use Shukla2010
    use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var
    use calculateTsat


    !! VARIABLES FOR FILE 
    CHARACTER(*), PARAMETER :: fileplace = "/home/maxle/sonov2/"
    real,DIMENSION(100) :: psat_arr, tsat_arr



    !! VARIABLES FOR PHASE 
    real :: dt_extra, mod_dt
    integer :: indexSat
    real :: tsatCell, tCell 



    !!**************  READING IN SAT TABLE ***********************  !

    open(12,file=fileplace//"psat.txt")
    read(12,*) psat_arr(:)
    close(12)




    open(13,file=fileplace//"tsat.txt")
    read(13,*) tsat_arr(:)
    CLOSE(13)


    !!**************  SCALE SAT TABLE ***********************  !

    psat_arr(:) = psat_arr(:)/pscale    ! Pa to Non dim
    tsat_arr(:) = tsat_arr(:)/300      ! Kelvin to Non dim


    !!**************  PHASE TRANS  *************************  !


    DO i = 1,NI 
        IF (p(i,1,1)<30.00 .AND. p(i,1,1)>0.1 ) THEN
            
            print*, "PASSED PRESSURE CHECK" 
            print*, "i = ", i 
            print*, "P(i,1,1) = ", p(i,1,1) 
            print*, "Psat_arr(1) = ", psat_arr(1) 


            indexSat = int(      (p(i,1,1) -psat_arr(1))/(0.30202) + 1.00) 
            print*, "FINISHED CALCULATING INDEXSAT = ", indexSat 

            tsatCell = tsat_arr(indexSat)
            tCell = T(i,1,1,2) 

            print*, "Tsatcell = ", tsatCell 
            print*, "Tcell = ", tCell 

            IF (tCell > tsatCell) THEN 

                IF ((phi(i,1,1,2)<(1.0-phiSaureltol)) &
                    .AND. (phi(i,1,1,2)>(phiSaureltol))) THEN

                    print*, "i = ", i 

                    call appendixB(i,1,1)

                    PRINT*, '-------------------------------------------------------------------------------------------'

                    !!gives you Q,Ydot, S_alpha1, S_y1, S_y2
                    call solveSource6663(i,1,1,deltat)

                    !if (s_y1 < 0 ) then 
                        !print*, "Sy1 < 0 at i = ", i 
                        !STOP 
                    !end if 
                    !!gives you S_max_alpha1, S_max_y1
                    call findMaxSource(i,1,1,deltat)


                    !!**********  SOLVE dPHI_GAS/dt *****************************!

                    print*, " "
                    print*, "START SOLVING PHI1"
                    if (abs(s_max_alpha1)>abs(s_alpha1)) then
                        print*, "PASSED ALPHA1 AT i = ", i

                        print*, "S_ALPHA1 = ", s_alpha1 
                        phi(i,1,1,1) = phi(i,1,1,1) + deltat*s_alpha1

                        phi(i,1,1,2) = 1.00-phi(i,1,1,1)


                    else
                        print*, "FAILED ALPHA1 AT i = ", i
                        print*, "S_ALPHA1 = ", s_alpha1 
                        r_PHASE = abs(s_max_alpha1/s_alpha1)
                        dtPHASE = r_PHASE*deltat*0.5
                        dt_ratio = deltat/dtPHASE
                        mod_dt = mod(deltat,dtPHASE)

                        print*, "R PHASE = ", r_PHASE 
                        print*, "dt_ratio = ", dt_ratio 

                        do integralcount = 1,int(dt_ratio)
                            phi(i,1,1,1) = phi(i,1,1,1) + dtPHASE*abs(s_alpha1)
                        end do 

                        phi(i,1,1,2) = 1.00-phi(i,1,1,1)
                    end if  !for abs check

                    print* , "PHI1 = ", phi(i,1,1,1), " AT i = ", i 
                    print* , "PHI2 = ", phi(i,1,1,2), " AT i = ", i
                    print* , "PHI1 + PHI2 = ", phi(i,1,1,1) + phi(i,1,1,2) 
                    print*, "FINISHED SOLVING PHI1"


                    !**********  SOLVE dRHOPHi_GAS/dt *********************!
                    print*, ""
                    print*, "START SOLVING DRHOPHI_GAS"  
                    if (abs(s_max_y1)>abs(s_y1)) then

                        print*, "PASSED Y1 AT i = ", i
                        print*, "SY1 = ", s_y1 
                        print*, "DT = ", deltat   
                        rhomat(i,1,1,1) = rhomat(i,1,1,1) + deltat*s_y1 

                    else
                        print*, "FAILED Y1 AT i = ", i
                        r_PHASE = abs(s_max_y1/s_y1) 
                        dtPHASE = r_PHASE*deltat*0.5
                        dt_ratio = deltat/dtPHASE
                        mod_dt = mod(deltat,dtPHASE)


                        print*, "R PHASE = ", r_PHASE 
                        print*, "dt_ratio = ", dt_ratio 
                        print*, "SY1 = ", s_y1 


                        do integralcount = 1,int(dt_ratio)
                            rhomat(i,1,1,1) = rhomat(i,1,1,1) + dtPHASE*(s_y1)
                        end do                  

                    end if  !!   for abs check


                    r_PHASE = 0.0        !!  zero out r_PHASE just in case 

                    print* , "RHO1PHI1 = ", rhomat(i,1,1,1), " AT i = ", i 
                    print*, "FINISHED SOLVING DRHOPHI_GAS"  


                    !!**********  SOLVE dRHOPHI_LIQUID/dt **************************! 
                    print*, ""
                    print*, "START SOLVING DRHOPHI_LIQUID"

                    call findMaxSourceSy2(i,1,1,deltat)


                    if (abs(s_max_y2)>abs(s_y2)) then
                        print*, "PASSED Y2 AT i = ", i
                        print*, "Sy2 = ", s_y2 
                        print*, "Smaxy2 = ", s_max_y2 
                        rhomat(i,1,1,2) = rhomat(i,1,1,2) + deltat*s_y2     

                    else

                        print*, "FAILED AT i = ", i
                        print*, "Sy2 = ", s_y2 
                        print*, "Smaxy2 = ", s_max_y2 
                        r_PHASE = abs(s_max_y2/s_y2)  
                        dtPHASE = r_PHASE*deltat*0.5
                        dt_ratio = deltat/dtPHASE
                        mod_dt = mod(deltat,dtPHASE)

                        do integralcount = 1,int(dt_ratio)
                            rhomat(i,1,1,2) = rhomat(i,1,1,2) + dtPHASE*s_y2 
                        end do

                    end if  !!   for abs check

                    print* , "RHO2PHI2 = ", rhomat(i,1,1,2), " AT i = ", i 
                    print*, "FINISHED SOLVING DRHOPHI_LIQUID"

                    print*, "" 
                    print*, "END PHASE TRANS FOR NODE i = ", i 

                    PRINT*, '-------------------------------------------------------------------------------------------'

                END IF  !! PHI 
            END IF !! TEMPERATURE 
        END IF  !! PRESSURE
    END DO 






end subroutine phaseTrans 
