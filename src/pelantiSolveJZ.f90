subroutine pelantiSolveJZ

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var
    use calculateTsat
    use palentiVar                                             

    !! VARIABLES FOR FILE 
    CHARACTER(*), PARAMETER :: fileplace = ""
    real,DIMENSION(100) :: psat_arr, tsat_arr


   

    !! VARIABLES FOR PHASE 
    integer :: indexSat
    real :: tsatCell, tCell, psatCell
    real :: interTol = 1e-2 !1e-6 


    real,dimension(2) :: rhok_star

    real :: mixture_internal_e_per_vol 

    real :: tstar

    real :: pstar 

    real :: delta_p_table 

    real :: tdum, pdum, rhodum, edum 


    ! interTol = 1e-8



    !!**************  READING IN SAT TABLE ***********************  !

    ! open(12,file=fileplace//"psat.txt")
    ! read(12,*) psat_arr(:)
    ! close(12)


    ! open(13,file=fileplace//"tsat.txt")
    ! read(13,*) tsat_arr(:)
    ! CLOSE(13)
                                               
    


    !!**************  SCALE SAT TABLE ***********************  !

    ! psat_arr(:) = psat_arr(:)/pscale   ! Pa to Non dim
    ! tsat_arr(:) = tsat_arr(:)/300      ! Kelvin to Non dim


    ! do i = 1,100
    !     print*, psat_arr(i)
    ! end do 
    
    ! stop 

    ! delta_p_table = psat_arr(2)-psat_arr(1)

    ! as_palenti = (matprop(10,1)-matprop(10,2)+matprop(9,2)-matprop(9,1))/(matprop(10,2)-matprop(1,2))
    ! bs_palenti = (matprop(8,1)-matprop(8,2))/(matprop(10,2)-matprop(1,2))
    ! cs_palenti = (matprop(10,2)-matprop(10,1))/(matprop(10,2)-matprop(1,2))
    ! ds_palenti = (matprop(10,1)-matprop(1,1))/(matprop(10,2)-matprop(1,2))

    ! tdum =    303.91764079749214!/300.
    ! pdum =    734848.48484848463!/pscale
    ! print *, as_palenti + bs_palenti/(tdum) + cs_palenti*log(tdum*300.)+ds_palenti*log((pdum+matprop(3,1))*pscale)&
    !      - log((pdum+matprop(3,2))*pscale)
    ! stop


    ! rhodum = 2156  
    ! edum = 6.9e8 

    ! rhodum = 918
    ! edum = 2.966e8

    ! pdum =   3.5e5 !2.5e6
    ! tdum = 550


    ! rhodum = (   (pdum/pscale) +matprop(3,2))/&
    !         ((matprop(2,2)-1)*matprop(1,2)*(tdum/300.))   !! non dim rho


    
    ! edum   = rhodum*matprop(1,2)*(tdum/300.)   !! non dim e

    ! rhodum = (   (pdum) +matprop(3,2)*pscale)/&
    !     ((matprop(2,2)-1)*matprop(1,2)*cvscale*(tdum))

    ! edum   = rhodum*matprop(1,2)*cvscale*(tdum)

    

    ! print*, "Original Psat/Tsat = "
    ! print*, "Tsat = ", tdum, " Kelvin, Psat = ", pdum , " Pa"
    
    ! print*, "rhodum = ", rhodum, " non dim"
    ! print*, "edum = ", edum, " non dim"

    ! print*, "Run through C.6 "
    ! call pelantiC6(pdum/pscale,rhodum,edum)  !! arguments = dimensional (Pa, kg/m^3, J/m^3)

    ! STOP 


    ! DO i = 1, NI

    !     IF ((phi(i,1,1,2)<(1.0-interTol)) &
    !          .AND. (phi(i,1,1,2)>(interTol))) THEN

    !             IF (p(i,1,1)<1000.00 .AND. p(i,1,1)>2.0) THEN

    !                 print*, ""
    !                 print*, "At i = ", i 
    !                 print*, "rhoE = ", rhoE(i,1,1)
    !                 print*, "rho*u = ", rhou(i,1,1)
    !                 print*, "rho = ", rho(i,1,1)
    !                 print*, "p = ", p(i,1,1)
    !                 print*, "Phi liquid = ", phi(i,1,1,2)

    !             END IF 

             
    !     END IF
        
        
    ! END DO 


    ! STOP 




    !! Fix to do 2D

    DO i = 1, NI

       IF ((phi(i,1,1,2)<(1.0-interTol)) &
            .AND. (phi(i,1,1,2)>(interTol))) THEN
          
            IF (p(i,1,1)<30.00) THEN


                
                mixture_internal_e_per_vol = rhoE(i,1,1) - 0.5*rhou(i,1,1)**2/rho(i,1,1)
                ! call sleep(1)
                call iterSolve(i,rho(i,1,1),mixture_internal_e_per_vol,pstar)
                
                print*, ""
                ! RECALCULATE ap, bp, dp USING P* from ITER SOLVER 
                print*, "Recalculate ap,bp,dp using P* from iter solver..."
                call pelantiC6(pstar,rho(i,1,1),mixture_internal_e_per_vol,tstar)

                print* ,"P* = ", pstar, " T* = ", tstar
            

                ! call newtonITER(rho(i,1,1),mixture_internal_e_per_vol,pstar)
              
                ! p(i,1,1) = pstar
            
                ! indexSat = int(      (p(i,1,1) -psat_arr(1))/(delta_p_table) + 1.00) 

                !$ SAT PROPERTIES AT CELL 
                ! psatCell = psat_arr(indexSat)
                ! tsatCell = tsat_arr(indexSat) 
                tCell = T(i,1,1,2)

                ! print*, ""
                ! print*, "AT i = ", i 
                ! print*, "index = ", indexSat
                ! print*, "Psat = Psat_arr(" ,indexSat,") = ", psatCell, " P* = Pcell = ", p(i,1,1)


                print*, "rho1 = ", rhomat(i,1,1,1)/phi(i,1,1,1) , "rho2 = ", rhomat(i,1,1,2)/phi(i,1,1,2)


                !$ Check Tliquid > Tsat, metastable 
                IF (tCell > tstar) THEN

                    
                    
                    !! C9 Pelanti 
                    rhok_star(1) = (pstar+matprop(3,1))/((matprop(2,1)-1)*matprop(1,1)*tstar)
                    rhok_star(2) = (pstar+matprop(3,2))/((matprop(2,2)-1)*matprop(1,2)*tstar)
                    ! rhok_star(1) = 0.99*rho(i,1,1)
                   if (rho(i,1,1) > rhok_star(1)) then
                        p(i,1,1) = pstar
                        phi(i,1,1,2) = (rho(i,1,1)- rhok_star(1))/(rhok_star(2)-rhok_star(1)) 
                        phi(i,1,1,1) = 1.00 - phi(i,1,1,2)

                        rhomat(i,1,1,1) = rhok_star(1)*phi(i,1,1,1)
                        rhomat(i,1,1,2) = rho(i,1,1) - rhomat(i,1,1,1) !rhok_star(2)*phi(i,1,1,2)
                  
                    endif
                 
                    ! rhomat(i,1,1,2) = rhok_star(2)*phi(i,1,1,2)
                    ! rhomat(i,1,1,1) = rho(i,1,1) - rhomat(i,1,1,2)

                    print*, "rho*1 = ", rhok_star(1), "rho*2 = ", rhok_star(2)
                    print*, "phi1* = ", phi(i,1,1,1), "phi2* = ", phi(i,1,1,2)
                    print*, "pstar = ", pstar

                    ! call sleep(1)

                END IF !!Tcell 
            END IF!! PRESSURE
         END IF !! PHI
    END DO  !! DO I 

    
    
    



end subroutine pelantiSolveJZ



subroutine pelantiC6(pdum,rhodum,edum,tcode) 

    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3


    real,intent(in) :: pdum,rhodum,edum 
    real,intent(out) :: tcode

    real :: ap_1, ap_2, ap_3 
    real :: bp_1, bp_2, bp_3



    real :: gm1, gm2 
    real :: cv1, cv2 
    real :: cp1, cp2 
    real :: pinf1, pinf2
    real :: qu1, qu2  

    real :: discri

    ! real :: tcode 

    cv1 = matprop(1,1)
    cv2 = matprop(1,2)


    gm1 = matprop(2,1)
    gm2 = matprop(2,2)

    pinf1 = matprop(3,1)
    pinf2 = matprop(3,2)

    cp1 = matprop(10,1)
    cp2 = matprop(10,2)


    qu1 = matprop(8,1)
    qu2 = matprop(8,2)
    
 

    ap_1 = rhodum*cv1*cv2
    ap_2 = (gm1-1.0)*(pdum+gm2*pinf2)
    ap_3 = (gm2-1.0)*(pdum+gm1*pinf1)

    ap_palenti = ap_1*(ap_2-ap_3)




    ! bp_1 = rhodum*edum*((pdum+pinf2)*cv1*(pdum+gm1*pinf1)-(pdum+pinf1)*cv2*(pdum+gm2*pinf2)) !! Max derives

    bp_1 = edum*((gm2-1.0)*cv2*(pdum+pinf1)-(gm1-1.0)*cv1*(pdum+pinf2))

    bp_2 = rhodum*((gm1-1)*cv1*qu2*(pdum+pinf2)-(gm2-1)*cv2*qu1*(pdum+pinf1))
    bp_3 = cv1*(pdum+pinf2)*(pdum+gm1*pinf1)-cv2*(pdum+pinf1)*(pdum+gm2*pinf2)


    bp_palenti = bp_1 + bp_2 + bp_3

    dp_palenti = (qu1-qu2)*(pdum+pinf2)*(pdum+pinf1)

    ! dp_palenti = (qu1-qu2)*(pdum+pinf2)*(pdum+pinf1)-(rhodum*edum)*(qu2-qu1)*(pdum+pinf1)*(pdum+pinf2) !! Max derives


    discri = (bp_palenti*bp_palenti)-(4.0*ap_palenti*dp_palenti)

    ! if (discri < 0.0) then 
    !     print*, "Error negative discriminant ", discriminant
    !     STOP 
    ! endif  
        


    tcode = (-bp_palenti+SQRT(discri))/(2.0*ap_palenti)

    ! print*, "T =  = ", tcode, " P = ", pdum,  " non dim" 
    ! print*, "T =  = ", tcode*300, " Kelvin, P = ", pdum*pscale, " Pa" 



end subroutine pelantiC6




subroutine formC8(pdum,rho_dum,e_dum,c8return,temper_return)


    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3

    real,intent(out) :: c8return,temper_return
    real,intent(in) :: pdum,rho_dum,e_dum
    
    real :: cp_l, cp_v
    real :: cv_l, cv_v 
    real :: gm_l, gm_v
    real :: q_l, q_v
    real :: qp_l, qp_v 
    real :: pinf_l, pinf_v 


    cp_v = matprop(10,1)
    cp_l = matprop(10,2)

    cv_v = matprop(1,1)
    cv_l = matprop(1,2)

    gm_v = matprop(2,1)
    gm_l = matprop(2,2)

    q_v = matprop(8,1)
    q_l = matprop(8,2)

    qp_v = matprop(9,1)
    qp_l = matprop(9,2)

    pinf_v = matprop(3,1)
    pinf_l = matprop(3,2)


     !! form ap,bp,dp 
    call pelantiC6(pdum,rho_dum, e_dum,temper_return)

    !! Calculate palenti variables equation 11b
    as_palenti = (cp_l-cp_v+qp_v-qp_l)/(cp_v-cv_v)
    bs_palenti = (q_l-q_v)/(cp_v-cv_v)
    cs_palenti = (cp_v-cp_l)/(cp_v-cv_v)
    ds_palenti = (cp_l-cv_l)/(cp_v-cv_v)
    

    !! c8
    c8return = as_palenti + bs_palenti/(temper_return) + cs_palenti*log(temper_return*300.)+&
                ds_palenti*log((pdum+pinf_l)*pscale)&
            - log((pdum+pinf_v)*pscale)


    ! print*,"---------------------------"
    ! print*,"inside formC8 "
    ! print*, "input p = ", pdum
    ! print*, "ap = ", ap_palenti
    ! print*, "bp = ", bp_palenti
    ! print*, "dp = ", dp_palenti
    ! print*, "temper C7 = ", temper_return
    ! print*, "c8return = ", c8return
    ! print*,"---------------------------"



end subroutine formC8




subroutine iterSolve(counter,rhodummy,edummy,iterRes)

    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
  
    real :: toler, guess, errorITER, fprime, f1, f2, t1, t2,t_error
    real,intent(out) :: iterRes 
    real,intent(in) :: rhodummy, edummy
    integer, intent(in) :: counter 




    guess = 1
    errorITER = 20
    toler = 1e-2 

    print*, ""
    print*, "--------------------------------------------------------------------------"
    print*, ""
    print*, "INSIDE ITER SOLVER AT i = ", counter
    print*, "INPUT rho0 = ", rhodummy, " e0 = ", edummy
    print*, "Guess before = ", guess
    do while (abs(errorITER)>toler)      


        !!form F
        print*, "calculate f(guess) " 
        call formC8(guess,rhodummy,edummy,f1,t1)
        print*, "fguess = ", f1, " tguess = ", t1

        print*, ""
        print*, "calculate f(f(guess))"
        call formC8(guess+f1,rhodummy,edummy,f2,t2)
        print*, "f(guess+h) = ", f2, " t(guess+h) = ", t2
        
        !!calculate Fprime
        print*, ""
        print*, "calculate fprime" 
        ! fprime = (f2-f1)/f1
        fprime = (f2/f1)-1.0
        print*, "fprime = ", fprime

        print*, ""
        print*, "calculate new guess"
        guess = guess-(f1/fprime)
        print*, "guess = ", guess

        print*, "" 
        print*, "calculate error.."
        call formC8(guess,rhodummy,edummy,errorITER,t_error)
        print*, "error = ", errorITER, " tolerance = ", toler



        ! errorITER = errorITER/guess

        if (abs(errorITER)>toler) then 
            print*, ""
            print*, "CONTINUE ITERATING...."
        end if
        
    end do 


    iterRes = guess 
    print*, "Guess after = ", iterRes
    print*, "FINISHED ITER SOLVER..."
    print*, ""
    print*, "--------------------------------------------------------------------------"




end subroutine iterSolve




subroutine formfprime(pressdum,rho_fprime,e_fprime,fprimedum)
    
    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3

    !! intent variables
    real,INTENT(IN) :: rho_fprime, e_fprime,pressdum
    real,INTENT(OUT) :: fprimedum

    real :: dumvar


    !! Thermo variables
    real :: gm_1, gm_2 
    real :: cv_1, cv_2 
    real :: cp_1, cp_2 
    real :: pinf_1, pinf_2
    real :: eta_1, eta_2  

    
    !! other variables
    real :: tdum_fprime, disc_fprime


    !! Derivative variables

    real :: d_dp_first, d_dp_first_top, d_dp_first_bottom
    real :: app1, bpp1, bpp2, bpp3, bpp4
    real :: d_ap_dp, d_bp_dp, d_dp_dp   !! 3 mains coeff derivatives


    real :: d_apdp_dp


    real :: sqrt_term, dp_sqrt_term 
    real :: d_dp_second, d_dp_second_top, d_dp_second_bottom


    real :: dtemper_dp 

    !! fprime variables
    real :: d_bs_temper, d_cs_temper, d_ds_temper, d_log_temper

    !! Thermo
    cv_1 = matprop(1,1)
    cv_2 = matprop(1,2)

    gm_1 = matprop(2,1)
    gm_2 = matprop(2,2)

    pinf_1 = matprop(3,1)
    pinf_2 = matprop(3,2)

    cp_1 = matprop(10,1)
    cp_2 = matprop(10,2)

    eta_1 = matprop(8,1)
    eta_2 = matprop(8,2)



    !! Getting C.6: ap, bp, dp and T
    call pelantiC6(pressdum,rho_fprime,e_fprime,dumvar)    
  
    disc_fprime = (bp_palenti**2-4*ap_palenti*dp_palenti)
    tdum_fprime = (-bp_palenti+SQRT(discriminant))/(2*ap_palenti)






    !! Derivatives

    !! d/dp (-bp/2ap)
    app1 = rho_fprime*cv_2*cv_1*(gm_1-gm_2)
    bpp1 = e_fprime*((gm_2-1)*cv_2-(gm_1-1)*cv_1)
    bpp2 = rho_fprime*((gm_2-1)*cv_1*eta_2-(gm_2-1)*cv_2*eta_1)
    bpp3 = 2*(cv_1-cv_2)*pressdum
    bpp4 = cv_1*(gm_1*pinf_1+pinf_2) - cv_2*(gm_2*pinf_2+pinf_1)


    d_ap_dp = app1
    d_bp_dp = bpp1+bpp2+bpp3+bpp4
    d_dp_dp = 2*(eta_1-eta_2)*pressdum + (eta_1-eta_2)*(pinf_2+pinf_1)

    d_dp_first_top = ap_palenti*d_bp_dp-bp_palenti*d_ap_dp
    d_dp_first_bottom = ap_palenti**2

    d_dp_first = d_dp_first_top/d_dp_first_bottom
    d_dp_first = -0.5*d_dp_first

    d_apdp_dp = (d_ap_dp)*dp_palenti+ap_palenti*(d_dp_dp)


    !! d/dp (sqrt....)/....

    sqrt_term = bp_palenti**2-4*ap_palenti*dp_palenti
    dp_sqrt_term = 0.5*(sqrt_term)**(-1.5)*(&
                    (2*bp_palenti*d_bp_dp) - &
                    4*d_apdp_dp)
                    
    d_dp_second_top = ap_palenti*dp_sqrt_term-SQRT(sqrt_term)*d_ap_dp
    d_dp_second_bottom = ap_palenti**2
    d_dp_second = d_dp_second_top/d_dp_second_bottom
    d_dp_second = 0.5*d_dp_second


    !! Calculate dT/dp 
    dtemper_dp = d_dp_first+d_dp_second



    !! Calculate as,bs,cs,ds
    as_palenti = (cp_2-cp_1+qp_1-qp_2)/(cp_1-cv_1)
    bs_palenti = (q_2-q_1)/(cp_1-cv_1)
    cs_palenti = (cp_1-cp_2)/(cp_1-cv_1)
    ds_palenti = (cp_2-cv_2)/(cp_1-cv_1)
    

    !! Calculate fprime 
    d_bs_temper = -bs_palenti*tdum_fprime**(-2)*dtemper_dp
    d_cs_temper = cs_palenti*dtemper_dp/tdum_fprime
    d_ds_temper = ds_palenti/(pressdum+pinf_2)
    d_log_temper = 1.00/(pressdum+pinf_1)

    fprimedum = d_bs_temper+d_cs_temper+d_ds_temper-d_log_temper


end subroutine formfprime



subroutine newtonITER(rho_newton,e_newton,newton_res)
    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3

    real,INTENT(OUT) :: newton_res
    real,INTENT(IN) :: rho_newton, e_newton
  
    real :: fguess, fprimeguess, newton_toler, newton_guess, newton_error
    real :: tguess, t_error



    newton_guess = 10 
    
    ! newton_guess = currentpress
    newton_error = 10
    newton_toler = 1e-2


    ! print*, "-------------------------------------------------------------------"
    ! print*, "start iterating ...."

    do while (abs(newton_error)>newton_toler) 

        !!Get f(guess)
        ! print*, "Calculate f(guess) "
        call formC8(newton_guess,rho_newton,e_newton,fguess,tguess)
        ! print*, "fguess = ", fguess

        !!Get fprime(guess)
        ! print*, "Calculate fprime(guess)"
        call formfprime(newton_guess,rho_newton,e_newton,fprimeguess)
        ! print*, "fprime = ", fprimeguess

        newton_guess = newton_guess-(fguess/fprimeguess)

        ! print*, "After 1 iteration, guess = ", newton_guess

        !!Calculate error 
        ! print*, "calculate error"
        call formC8(newton_guess,rho_newton,e_newton,newton_error,t_error)
        ! print*, "error = ", newton_error

    end do 

    newton_res = newton_guess
    ! print*, "Finished iterating, final guess = ", newton_res

    ! print*, "-------------------------------------------------------------------"
    ! print*, ""



end subroutine newtonITER
