!***********************************************************************
      subroutine tvd()
!***********************************************************************
! pull required variables from respective modules
      use globalvar
      use gridvariables
      use TVDfunc
      use mieeos5eqn_ntemp
      use hllc
      use shukla2010, only: tmp
      use RungKutt3, only: rhoY

      ! MAX, PHASE
      real :: ktop, kbottom, ksource

      netFlux = 0.00
      flx = 0.00
      sigspeed = 0.00
      maxspeed = 0.00




! ----------------------------------------------------------------------
!     First DIRECTION 
! ----------------------------------------------------------------------
      do k = 1, NK
      do j = 1, NJ
      do i = 1, NI + 1
         leftstateprim = 0.000
         rightstateprim = 0.000

! Step 1: calculate left/right state primitive variables using 
!     MINMOD slope limiter
! Note: MINMOD slope limiter is not applied to Phi (volume fraction) 
!         the left/right state of Phi is 1st order   

! u left state   
      call sloplim(1, u(i-1,j,k) - u(i-2,j,k) ,&
                    & u(i,j,k) - u(i-1,j,k), tmp)
      leftstateprim(2) = u(i-1,j,k) + 0.5*tmp

! v left state   
      call sloplim(1, v(i-1,j,k) - v(i-2,j,k) ,&
                    & v(i,j,k) - v(i-1,j,k), tmp)
      leftstateprim(3) = v(i-1,j,k) + 0.5*tmp

! w left state   
      call sloplim(1, w(i-1,j,k) - w(i-2,j,k) ,&
                    & w(i,j,k) - w(i-1,j,k), tmp)
      leftstateprim(4) = w(i-1,j,k) + 0.5*tmp

! P left state   
      call sloplim(1, p(i-1,j,k) - p(i-2,j,k) ,&
                    & p(i,j,k) - p(i-1,j,k), tmp)
      leftstateprim(5) = p(i-1,j,k) + 0.5*tmp



! u right state
      call sloplim(1, u(i+1,j,k) - u(i,j,k) ,&
                    & u(i,j,k) - u(i-1,j,k), tmp)
      rightstateprim(2) = u(i,j,k) - 0.5*tmp

! v right state
      call sloplim(1, v(i+1,j,k) - v(i,j,k) ,&
                    & v(i,j,k) - v(i-1,j,k), tmp)
      rightstateprim(3) = v(i,j,k) - 0.5*tmp

! w right state
      call sloplim(1, w(i+1,j,k) - w(i,j,k) ,&
                    & w(i,j,k) - w(i-1,j,k), tmp)
      rightstateprim(4) = w(i,j,k) - 0.5*tmp

! P right state
      call sloplim(1, p(i+1,j,k) - p(i,j,k) ,&
                    & p(i,j,k) - p(i-1,j,k), tmp)
      rightstateprim(5) = p(i,j,k) - 0.5*tmp



! Following do loop is for left/right state of Phi (volume fraction)
      do loop=1,nmat
!!JZ
      !   leftstateprim(6+loop) = phi(i-1,j,k,loop)
      !   rightstateprim(6+loop) = phi(i,j,k,loop)
      call sloplim(1, phi(i-1,j,k,loop) - phi(i-2,j,k,loop) ,&
                    & phi(i,j,k,loop) - phi(i-1,j,k,loop), tmp)
      leftstateprim(6+loop) = phi(i-1,j,k,loop) + 0.5*tmp
      call sloplim(1, phi(i+1,j,k,loop) - phi(i,j,k,loop) ,&
                    & phi(i,j,k,loop) - phi(i-1,j,k,loop), tmp)
      rightstateprim(6+loop) = phi(i,j,k,loop) - 0.5*tmp
      enddo


! Following is for left/right state of rhomat (material density)
! note you do for all materials (1 to nmat)
      do loop=1,nmat
         ! rhomat left state
         call sloplim(1, rhomat(i-1,j,k,loop) - rhomat(i-2,j,k,loop),&
             & rhomat(i,j,k,loop) - rhomat(i-1,j,k,loop), tmp)
         leftstateprim(6+nmat+loop) = rhomat(i-1,j,k,loop) + 0.5*tmp

         ! rhomat right state
         call sloplim(1, rhomat(i+1,j,k,loop) - rhomat(i,j,k,loop),&
             & rhomat(i,j,k,loop) - rhomat(i-1,j,k,loop), tmp)
         rightstateprim(6+nmat+loop) = rhomat(i,j,k,loop) - 0.5*tmp

      enddo


! Mixture density rho
      fiveeqrholeft = 0.00
      fiveeqrhoright = 0.00
      do loop=1,nmat
#if rhoi == 1
         fiveeqrholeft = fiveeqrholeft+leftstateprim(6+nmat+loop)*leftstateprim(6+loop)
         fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)*rightstateprim(6+loop)
#else
         fiveeqrholeft = fiveeqrholeft+leftstateprim(6+nmat+loop)
         fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)
#endif
      enddo
      leftstateprim(1) = fiveeqrholeft
      rightstateprim(1) = fiveeqrhoright


#if  REACT==1 
! Following is for left/right state of species
! note you do for all species (1 to nspec)
      do loop=1,nspec
         ! species left state
         call sloplim(1, rhoY(i-1,j,k,loop) - rhoY(i-2,j,k,loop),&
             & rhoY(i,j,k,loop) - rhoY(i-1,j,k,loop), tmp)
         leftstateprim(6+nmat+nmat+loop) = rhoY(i-1,j,k,loop) + 0.5*tmp

         ! species right state
         call sloplim(1, rhoY(i+1,j,k,loop) - rhoY(i,j,k,loop),&
             & rhoY(i,j,k,loop) - rhoY(i-1,j,k,loop), tmp)
         rightstateprim(6+nmat+nmat+loop) = rhoY(i,j,k,loop) - 0.5*tmp
      enddo
#endif


! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,0)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,0)


! If either state's rho or P_infinity is negative, then reduce to 1st order
      if( ( leftstateprim(1) < 0 .or. &
          & leftstateprim(5) < -1*leftfluidprop(2)) .or. &
          & ( rightstateprim(1) < 0 .or. & 
          & rightstateprim(5) <-1*rightfluidprop(2)) )then

            leftstateprim(1) = rho(i-1,j,k)
            leftstateprim(2) = u(i-1,j,k)
            leftstateprim(3) = v(i-1,j,k)
            leftstateprim(4) = w(i-1,j,k)
            leftstateprim(5) = p(i-1,j,k)

            rightstateprim(1) = rho(i,j,k)
            rightstateprim(2) = u(i,j,k)
            rightstateprim(3) = v(i,j,k)
            rightstateprim(4) = w(i,j,k)
            rightstateprim(5) = p(i,j,k)
      endif


! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,2)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,2)


! Call HLLC function with specified left right states and 
!     mixture properties to get fluxes 
      call hllcsolver(1,0,0,leftstateprim,rightstateprim,&
                  & leftfluidprop,rightfluidprop,flx)

      maxspeed = amax1( sigspeed, maxspeed)


      tmp = delx
      poverr = 0.0000
#if probdim==1

#if GEOMETRY==1
      tmp = delx
      poverr = 0.0000

#elif GEOMETRY==2
      poverr = p(i,j,k)/xc(i,j,k)

      tmp = xc(i,j,k)*delx/(xc(i,j,k) - 0.5*delx)
#elif GEOMETRY==3
#endif

#endif

     

      ! LEFT Face, flux calculation, AXIAL
      if ( i < NI+1) then
      ! For Phi: This calculates div(Phi*U)
        do loop=1,(6 + nmat + nmat+nspec)
          netFlux(i,j,k,loop) = netFlux(i,j,k,loop) + (flx(loop)/tmp)
        enddo
        netFlux(i,j,k,2) = netFlux(i,j,k,2) + poverr
        !netFlux(i,j,k,3) = netFlux(i,j,k,3) + poverr
        do loop=7,(6 + nmat)
            ! For Phi: This does:  udiv(Phi) = div(Phi*u)-Phi*div(u)
            ! so to add K, we do:
            !  udiv(Phi) = div(Phi*u)-(Phi+K)*div(u)


            ! Calculate K for i
            ktop = phi(i,1,1,1)*(1.-phi(i,1,1,1))*((matprop(2,2)*(p(i,j,k)&
            +matprop(3,2)))-(matprop(2,1)*(p(i,j,k)+matprop(3,1))))

            kbottom = ((1.-phi(i,1,1,1))*(matprop(2,1)*(p(i,j,k)+ &
            matprop(3,1))))+(phi(i,1,1,1)*(matprop(2,2)*(p(i,j,k)+matprop(3,2))))

           
            if (kchar == 1) then
                  ksource = ktop/kbottom
            else if (kchar == 2) then
                  ksource = 0.0
            end if

          netflux(i,j,k,loop) = netflux(i,j,k,loop)&
                & - (Phi(i,j,k,loop-6)+ksource)*&
                & flx(6 + nmat + nmat +nspec + 1)/tmp
        enddo
      endif 

      ! print*, 'THIS IS K SOURCE', ksource
      

#if probdim==1

#if GEOMETRY==1
      tmp = delx
#elif GEOMETRY==2
      tmp = xc(i-1,j,k)*delx/(xc(i-1,j,k) + 0.5*delx)
#elif GEOMETRY==3
#endif

#endif


      ! Zero out K before doing i-1

      ktop = 0.0
      kbottom = 0.0
      ksource = 0.0



      !RIGHT FACE: FLux Calc AXIAL
      if ( i > 1) then
            


        do loop=1,(6+nmat+nmat+nspec)
            ! For Phi: This calculates div(Phi*U)
            netFlux(i-1,j,k,loop) = netFlux(i-1,j,k,loop) - &
                & (flx(loop)/tmp)
        enddo
        do loop=7,(6 + nmat)
            ! Calculate K for i-1 
            ktop = phi(i-1,1,1,1)*(1.-phi(i-1,1,1,1))*((matprop(2,2)*(p(i-1,j,k) &
            +matprop(3,2)))-(matprop(2,1)*(p(i-1,j,k)+matprop(3,1))))

            kbottom = ((1.-phi(i-1,1,1,1))*(matprop(2,1)*(p(i-1,j,k)+matprop(3,1))))+&
                  & (phi(i-1,1,1,1)*(matprop(2,2)*(p(i-1,j,k)+matprop(3,2))))
            
          
      
            if (kchar == 1) then
                  ksource = ktop/kbottom
            else if (kchar == 2) then
                  ksource = 0.0
            end if

            

            netflux(i-1,j,k,loop) = netflux(i-1,j,k,loop) + &
                & (Phi(i-1,j,k,loop-6)+ksource)*&
                & flx(6 + nmat + nmat +nspec+ 1)/tmp
        enddo
      endif 

      enddo !enddo for do i=1,NI+1
      enddo !enddo for do j=1,NJ
      enddo !enddo for do k=1,NK
! this concludes the axial direction part of TVD


      


#if probdim==1
! skip to end of function if problem is 1D problem
      goto 555
#endif
! ----------------------------------------------------------------------
!     SECOND DIRECTION 
! ----------------------------------------------------------------------
      do k = 1, NK
      do j = 1, NJ+1
      do i = 1, NI 
! Step 1: calculate left/right state primitive variables using 
!     MINMOD slope limiter
! Note: MINMOD slope limiter is not applied to Phi (volume fraction) 
!         the left/right state of Phi is 1st order   
      leftstateprim = 0.000
      rightstateprim = 0.000

! u left state   
      call sloplim(1, u(i,j-1,k) - u(i,j-2,k) ,&
                    & u(i,j,k) - u(i,j-1,k), tmp)
      leftstateprim(2) = u(i,j-1,k) + 0.5*tmp

! v left state   
      call sloplim(1, v(i,j-1,k) - v(i,j-2,k) ,&
                    & v(i,j,k) - v(i,j-1,k), tmp)
      leftstateprim(3) = v(i,j-1,k) + 0.5*tmp

! w left state   
      call sloplim(1, w(i,j-1,k) - w(i,j-2,k) ,&
                    & w(i,j,k) - w(i,j-1,k), tmp)
      leftstateprim(4) = w(i,j-1,k) + 0.5*tmp

! P left state   
      call sloplim(1, p(i,j-1,k) - p(i,j-2,k) ,&
                    & p(i,j,k) - p(i,j-1,k), tmp)
      leftstateprim(5) = p(i,j-1,k) + 0.5*tmp



! u right state
      call sloplim(1, u(i,j+1,k) - u(i,j,k) ,&
                    & u(i,j,k) - u(i,j-1,k), tmp)
      rightstateprim(2) = u(i,j,k) - 0.5*tmp

! v right state
      call sloplim(1, v(i,j+1,k) - v(i,j,k) ,&
                    & v(i,j,k) - v(i,j-1,k), tmp)
      rightstateprim(3) = v(i,j,k) - 0.5*tmp

! w right state
      call sloplim(1, w(i,j+1,k) - w(i,j,k) ,&
                    & w(i,j,k) - w(i,j-1,k), tmp)
      rightstateprim(4) = w(i,j,k) - 0.5*tmp

! P right state
      call sloplim(1, p(i,j+1,k) - p(i,j,k) ,&
                    & p(i,j,k) - p(i,j-1,k), tmp)
      rightstateprim(5) = p(i,j,k) - 0.5*tmp

! Following do loop is for left/right state of Phi (volume fraction)
      do loop=1,nmat
         leftstateprim(6+loop) = phi(i,j-1,k,loop)
         rightstateprim(6+loop) = phi(i,j,k,loop)
      enddo

! Following if is for left/right state of rhomat (material density) 
! note you only need to do for all materials (1 to nmat)
      do loop=1,nmat
         ! rhomat left state
         call sloplim(1, rhomat(i,j-1,k,loop) - rhomat(i,j-2,k,loop),&
                 & rhomat(i,j,k,loop) - rhomat(i,j-1,k,loop), tmp)
         leftstateprim(6+nmat+loop) = rhomat(i,j-1,k,loop) + 0.5*tmp

         ! rhomat right state
         call sloplim(1, rhomat(i,j+1,k,loop) - rhomat(i,j,k,loop),&
                       & rhomat(i,j,k,loop) - rhomat(i,j-1,k,loop), tmp)
         rightstateprim(6+nmat+loop) = rhomat(i,j,k,loop) - 0.5*tmp
      enddo

#if  REACT==1 
! Following if is for left/right state of species (1 to nspec)
      do loop=1,nspec
         ! species left state
         call sloplim(1, rhoY(i,j-1,k,loop) - rhoY(i,j-2,k,loop),&
                 & rhoY(i,j,k,loop) - rhoY(i,j-1,k,loop), tmp)
         leftstateprim(6+nmat+nmat+loop) = &
                 &rhoY(i,j-1,k,loop) + 0.5*tmp

         ! species right state
         call sloplim(1, rhoY(i,j+1,k,loop) - rhoY(i,j,k,loop),&
                         & rhoY(i,j,k,loop) - rhoY(i,j-1,k,loop), tmp)
         rightstateprim(6+nmat+nmat+loop) = rhoY(i,j,k,loop) - 0.5*tmp
      enddo
#endif


! Following if is for left/right state of mixture rho
! for 5 eqn model and rho-alpha
      fiveeqrholeft = 0.00
      fiveeqrhoright = 0.00
      do loop=1,nmat
#if rhoi == 1
                fiveeqrholeft  = fiveeqrholeft+leftstateprim(6+nmat+loop)  *leftstateprim(6+loop) 
                fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)*rightstateprim(6+loop)
#else
         fiveeqrholeft = fiveeqrholeft + leftstateprim(6+nmat+loop)
         fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)
#endif
      enddo
      leftstateprim(1) = fiveeqrholeft
      rightstateprim(1) = fiveeqrhoright


! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,0)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,0)

! If either state's rho or P_infinity is negative, then reduce to 1st order
      if( ( leftstateprim(1) < 0 .or. &
          & leftstateprim(5) < -1*leftfluidprop(2)) .or. &
          & ( rightstateprim(1) < 0 .or. & 
          & rightstateprim(5) <-1*rightfluidprop(2)) )then

             leftstateprim(1) = rho(i,j-1,k)
             leftstateprim(2) = u(i,j-1,k)
             leftstateprim(3) = v(i,j-1,k)
             leftstateprim(4) = w(i,j-1,k)
             leftstateprim(5) = p(i,j-1,k)

             rightstateprim(1) = rho(i,j,k)
             rightstateprim(2) = u(i,j,k)
             rightstateprim(3) = v(i,j,k)
             rightstateprim(4) = w(i,j,k)
             rightstateprim(5) = p(i,j,k)
      endif
! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,2)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,2)

! Call HLLC function with specified left right states and 
!     mixture properties to get fluxes
      call hllcsolver(0,1,0,leftstateprim,rightstateprim,leftfluidprop,&
             & rightfluidprop,flx)


      maxspeed = amax1( sigspeed, maxspeed)


#if GEOMETRY==2
      poverr = p(i,j,k)/yc(i,j,k)
      tmp = yc(i,j,k)*dely/(yc(i,j,k) - 0.5*dely)
#else
      tmp = dely
      poverr = 0.0000
#endif

      if ( j < NJ+1) then
        do loop=1,(6 + nmat + nmat+nspec)
            netFlux(i,j,k,loop) = netFlux(i,j,k,loop) + (flx(loop)/tmp)
        enddo
        netFlux(i,j,k,3) = netFlux(i,j,k,3) + poverr
        do loop=7,(6 + nmat)
            netflux(i,j,k,loop) = netflux(i,j,k,loop) - &
              & Phi(i,j,k,loop-6)*(flx(6 + nmat +nspec+ nmat + 1)/tmp)
        enddo
      endif

#if GEOMETRY==2
      tmp = yc(i,j-1,k)*dely/(yc(i,j-1,k) + 0.5*dely)
#else
      tmp = dely
#endif

      if ( j > 1) then
        do loop=1,(6 + nmat + nmat+nspec)
          netFlux(i,j-1,k,loop) = netFlux(i,j-1,k,loop)-(flx(loop)/tmp)
        enddo
        do loop=7,(6 + nmat)
          netflux(i,j-1,k,loop) = netflux(i,j-1,k,loop) + &
             & Phi(i,j-1,k,loop-6)*flx(6 + nmat + nmat+nspec + 1)/tmp
        enddo
      endif

      enddo !enddo for do i=1,NI
      enddo !enddo for do j=1,NJ+1
      enddo !enddo for do k=1,NK
! this concludes the transverse direction part of TVD


#if probdim==2
      goto 555
#endif

! ----------------------------------------------------------------------
!     THIRD  DIRECTION 
! ----------------------------------------------------------------------
      do k = 1, NK+1
      do j = 1, NJ
      do i = 1, NI 
! Step 1: calculate left/right state primitive variables using 
!     MINMOD slope limiter
! Note: MINMOD slope limiter is not applied to Phi (volume fraction) 
!         the left/right state of Phi is 1st order   
      leftstateprim = 0.000
      rightstateprim = 0.000

! u left state   
      call sloplim(1, u(i,j,k-1) - u(i,j,k-2) ,&
                    & u(i,j,k) - u(i,j,k-1), tmp)
      leftstateprim(2) = u(i,j,k-1) + 0.5*tmp

! v left state   
      call sloplim(1, v(i,j,k-1) - v(i,j,k-2) ,&
                    & v(i,j,k) - v(i,j,k-1), tmp)
      leftstateprim(3) = v(i,j,k-1) + 0.5*tmp

! w left state   
      call sloplim(1, w(i,j,k-1) - w(i,j,k-2) ,&
                    & w(i,j,k) - w(i,j,k-1), tmp)
      leftstateprim(4) = w(i,j,k-1) + 0.5*tmp

! P left state   
      call sloplim(1, p(i,j,k-1) - p(i,j,k-2) ,&
                    & p(i,j,k) - p(i,j,k-1), tmp)
      leftstateprim(5) = p(i,j,k-1) + 0.5*tmp



! u right state
      call sloplim(1, u(i,j,k+1) - u(i,j,k) ,&
                    & u(i,j,k) - u(i,j,k-1), tmp)
      rightstateprim(2) = u(i,j,k) - 0.5*tmp

! v right state
      call sloplim(1, v(i,j,k+1) - v(i,j,k) ,&
                    & v(i,j,k) - v(i,j,k-1), tmp)
      rightstateprim(3) = v(i,j,k) - 0.5*tmp

! w right state
      call sloplim(1, w(i,j,k+1) - w(i,j,k) ,&
                    & w(i,j,k) - w(i,j,k-1), tmp)
      rightstateprim(4) = w(i,j,k) - 0.5*tmp

! P right state
      call sloplim(1, p(i,j,k+1) - p(i,j,k) ,&
                    & p(i,j,k) - p(i,j,k-1), tmp)
      rightstateprim(5) = p(i,j,k) - 0.5*tmp



! Following do loop is for left/right state of Phi (volume fraction)
! note you only need to do for: material 1 to material (nmat-1)
      do loop=1,nmat
         leftstateprim(6+loop) = phi(i,j,k-1,loop)
         rightstateprim(6+loop) = phi(i,j,k,loop)
      enddo



! Following if is for left/right state of rhomat (material density) 
! note you only need to do for all materials (1 to nmat)
      do loop=1,nmat
         ! rhomat left state
         call sloplim(1, rhomat(i,j,k-1,loop) - rhomat(i,j,k-2,loop),&
                 & rhomat(i,j,k,loop) - rhomat(i,j,k-1,loop), tmp)
         leftstateprim(6+nmat+loop) = rhomat(i,j,k-1,loop) + 0.5*tmp

         ! rhomat right state
         call sloplim(1, rhomat(i,j,k+1,loop) - rhomat(i,j,k,loop),&
                       & rhomat(i,j,k,loop) - rhomat(i,j,k-1,loop),tmp)
         rightstateprim(6+nmat+loop) = rhomat(i,j,k,loop) - 0.5*tmp
      enddo


#if  REACT==1 
! Following if is for left/right state of species (1 to nspec)
      do loop=1,nspec
         ! species left state
         call sloplim(1, rhoY(i,j,k-1,loop) - rhoY(i,j,k-2,loop),&
                 & rhoY(i,j,k,loop) - rhoY(i,j,k-1,loop), tmp)
         leftstateprim(6+nmat+nmat+loop) = rhoY(i,j,k-1,loop) + 0.5*tmp

         ! species right state
         call sloplim(1, rhoY(i,j,k+1,loop) - rhoY(i,j,k,loop),&
                         & rhoY(i,j,k,loop) - rhoY(i,j,k-1,loop), tmp)
         rightstateprim(6+nmat+nmat+loop) = rhoY(i,j,k,loop) - 0.5*tmp
      enddo
#endif


! Following if is for left/right state of mixture rho
! for 5 eqn model and rho-alpha
      fiveeqrholeft = 0.00
      fiveeqrhoright = 0.00
      do loop=1,nmat
#if rhoi == 1
                  fiveeqrholeft  = fiveeqrholeft+leftstateprim(6+nmat+loop)  *leftstateprim(6+loop) 
                  fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)*rightstateprim(6+loop)
#else
          fiveeqrholeft = fiveeqrholeft + leftstateprim(6+nmat+loop)
         fiveeqrhoright = fiveeqrhoright+rightstateprim(6+nmat+loop)
#endif
      enddo
      leftstateprim(1) = fiveeqrholeft
      rightstateprim(1) = fiveeqrhoright


! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,0)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,0)

! If either state's rho or P_infinity is negative, then reduce to 1st order
      if( ( leftstateprim(1) < 0 .or. &
          & leftstateprim(5) < -1*leftfluidprop(2)) .or. &
          & ( rightstateprim(1) < 0 .or. & 
          & rightstateprim(5) <-1*rightfluidprop(2)) )then

             leftstateprim(1) = rho(i,j,k-1)
             leftstateprim(2) = u(i,j,k-1)
             leftstateprim(3) = v(i,j,k-1)
             leftstateprim(4) = w(i,j,k-1)
             leftstateprim(5) = p(i,j,k-1)

             rightstateprim(1) = rho(i,j,k)
             rightstateprim(2) = u(i,j,k)
             rightstateprim(3) = v(i,j,k)
             rightstateprim(4) = w(i,j,k)
             rightstateprim(5) = p(i,j,k)
      endif
! Step 2: Calculate the left and right state mixture properties
! Properties: gamma, p_infinity, speed of sound, E, E + P
! Note in Dr. Zhangs code he calculates the fluid properties 
!     before doing check for negative 
!     density and p_infinity
      call calcfluidprop(leftstateprim,leftfluidprop,i,j,k,2)
      call calcfluidprop(rightstateprim,rightfluidprop,i,j,k,2)

! Call HLLC function with specified left right states and 
!     mixture properties to get fluxes
      call hllcsolver(0,0,1,leftstateprim,rightstateprim,leftfluidprop,&
             & rightfluidprop,flx)


      maxspeed = amax1( sigspeed, maxspeed)


      if ( k < NK+1) then
        do loop=1,(6 + nmat + nmat+nspec)
          netFlux(i,j,k,loop) = netFlux(i,j,k,loop) + (flx(loop)/delz)
        enddo
        do loop=7,(6 + nmat)
          netflux(i,j,k,loop) = netflux(i,j,k,loop) - &
            & Phi(i,j,k,loop-6)*(flx(6 + nmat +nspec+ nmat + 1)/delz)
        enddo
      endif

      if ( k > 1) then
        do loop=1,(6 + nmat + nmat+nspec)
          netFlux(i,j,k-1,loop) = netFlux(i,j,k-1,loop)-(flx(loop)/delz)
        enddo
        do loop=7,(6 + nmat)
          netflux(i,j,k-1,loop) = netflux(i,j,k-1,loop) + &
            & Phi(i,j,k-1,loop-6)*flx(6 + nmat + nmat+nspec + 1)/delz
        enddo
      endif

      enddo !enddo for do i=1,NI
      enddo !enddo for do j=1,NJ
      enddo !enddo for do k=1,NK+1
! this concludes the depth direction part of TVD


555  continue


      end subroutine tvd
!***********************************************************************
