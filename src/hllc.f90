!***********************************************************************
      subroutine hllcsolver(nx,ny,nz,leftstate,&
                           &rightstate,lprop,rprop,flux)
!***********************************************************************
! pull required variables from respective modules
      use globalvar
      use hllc

! Declare the calling variables and specify their intent
      integer, intent(in) :: nx, ny,nz
      real,  intent(in), dimension(6 + nmat + nmat+nspec) :: leftstate
      real,  intent(in), dimension(6 + nmat + nmat+nspec) :: rightstate
      real,  intent(in), dimension(5) :: lprop,rprop
      real, intent(out) , dimension(6 + nmat + nmat + nspec+1) :: flux


      sigspeed = 0.0


! ----------------------------------------------------------------------
!     Preliminary step 1  
! ----------------------------------------------------------------------
! Set the left/right state of primitive variables from input arrays
      rhoL = leftstate(1)
      uL = leftstate(2)
      vL = leftstate(3)
      wL = leftstate(4)
      PL = leftstate(5)

      rhoR = rightstate(1)
      uR = rightstate(2)
      vR = rightstate(3)
      wR = rightstate(4)
      PR = rightstate(5)


! Set the normal & tangential velocities for the left and right states
! Pras: I commented out tangential velocities because it is not needed
! Might need for future problem?
      unL = uL*nx + vL*ny + wL*nz
      !utL = vL*nx + (-1)*uL*ny 
      unR = uR*nx + vR*ny + wR*nz
      !utR = vR*nx + (-1)*uR*ny 


! set D vector (from Toro), used in calculating FL and FR
      D = 0.00
      D(2) = nx
      D(3) = ny
      D(4) = nz


! ----------------------------------------------------------------------
!     STEP 1 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate the left and right state of the fluid speed of sound
      cL = lprop(3)
      cR = rprop(3)


! Calculate the fluid energy at the left and right states
      EL = lprop(4)
      ER = rprop(4)


! ----------------------------------------------------------------------
!     STEP 2 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate the estimate for min and max signal velocities
      SL = amin1( (unL - cL) , (unR - cR) )
      SR = amax1( (unL + cL) , (unR + cR) )


! ----------------------------------------------------------------------
!     STEP 3 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate signal speed in the star region 
      num = PR - PL + rhoL*unL*(SL-unL) - rhoR*unR*(SR-unR)
      den = rhoL*(SL-unL) - rhoR*(SR-unR)
      Sstr = num/den


! ----------------------------------------------------------------------
!     STEP 4 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate pressure in star region
      PStar = PL + rhoL*(Sstr-unL)*(SL-unL)


! ----------------------------------------------------------------------
!     STEP 5 in User Manual - HLLC :  LEFT STATE
! ----------------------------------------------------------------------
! Calculate the conserved variables at left state
      ConsvarL(1) = rhoL
      ConsvarL(2) = rhoL*uL
      ConsvarL(3) = rhoL*vL
      ConsvarL(4) = rhoL*wL
      ConsvarL(5) = EL
      ConsvarL(7:arraydima) = leftstate(7:arraydima)


! Calculate the left state conserved fluxes
      D(5) = unL
      FL(1:arraydima) = unL*ConsvarL(1:arraydima) + PL*D(1:arraydima)
      FL(arraydimb) = unL


! ----------------------------------------------------------------------
!     STEP 5 in User Manual - HLLC :  RIGHT STATE
! ----------------------------------------------------------------------
! Calculate the conserved variables at right state
      ConsvarR(1) = rhoR
      ConsvarR(2) = rhoR*uR
      ConsvarR(3) = rhoR*vR
      ConsvarR(4) = rhoR*wR
      ConsvarR(5) = ER
      ConsvarR(7:arraydima) = rightstate(7:arraydima)


! Calculate the right state conserved fluxes
      D(5) = unR
      FR(1:arraydima) = unR*ConsvarR(1:arraydima) + PR*D(1:arraydima)
      FR(arraydimb) = unR


! ----------------------------------------------------------------------
!     STEP 6 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate the star left state flux
      ULstar(1) = rhoL*(SL-unL)/(SL-Sstr)
      ULstar(2) = ULstar(1)*( Sstr*nx + (1-nx)*uL )
      ULstar(3) = ULstar(1)*( Sstr*ny + (1-ny)*vL )
      ULstar(4) = ULstar(1)*( Sstr*nz + (1-nz)*wL )
      ULstar(5) = (SL*ConsvarL(5) - FL(5) + Sstr*PStar)/(SL-Sstr)
      ULstar(7:arraydima) = leftstate(7:arraydima)*(SL-unL)/(SL-Sstr)


! Calculate the star right state flux
      URstar(1) = rhoR*(SR-unR)/(SR-Sstr)
      URstar(2) = URstar(1)*( Sstr*nx + (1-nx)*uR )
      URstar(3) = URstar(1)*( Sstr*ny + (1-ny)*vR )
      URstar(4) = URstar(1)*( Sstr*nz + (1-nz)*wR )
      URstar(5) = (SR*ConsvarR(5) - FR(5) + Sstr*PStar)/(SR-Sstr)
      URstar(7:arraydima) = rightstate(7:arraydima)*(SR-unR)/(SR-Sstr)


! ----------------------------------------------------------------------
!     STEP 7 in User Manual - HLLC   
! ----------------------------------------------------------------------
! Calculate intermediate flux at star left state
      FLstar(1:arraydima) = FL(1:arraydima) + &
                    & SL*( ULstar(1:arraydima) - ConsvarL(1:arraydima) )
      FLstar(arraydimb) = unL + SL*( (SL-unL)/(SL-Sstr) - 1)


! Calculate intermediate flux at star right state
      FRstar(1:arraydima) = FR(1:arraydima) + & 
                    & SR*( URstar(1:arraydima) - ConsvarR(1:arraydima) )
      FRstar(arraydimb) = unR + SR*( (SR-unR)/(SR-Sstr) - 1)


! ----------------------------------------------------------------------
!     STEP 8 in User Manual - HLLC    
! ----------------------------------------------------------------------
! Calculate the hllc numerical flux
      if (SL > 0.0) then
        flux = FL
      elseif ( (Sstr .ge. 0.0) .AND. (SL < 0.0) ) then
        flux = FLstar
      elseif ( (Sstr < 0.0) .AND. (SR .ge. 0.0) ) then
        flux = FRstar
      elseif (SR < 0.0) then 
        flux = FR
      endif 

! uncomment following if you are trying to debug
      !lep = FL(4)
      !rep = FR(4)


! ----------------------------------------------------------------------
!     Update variable sigspeed
! ----------------------------------------------------------------------
! Find the maximum signal speed between SL and SR and set to sigspeed
      sigspeed  = amax1( abs(SL) , abs(SR) )


      end subroutine hllcsolver
!***********************************************************************
