import numpy as np 
import matplotlib.pyplot as plt

#Water
pinfty1 = 1e9
pinfty2 = 0.0
cp1 = 4267.
cp2 = 1487.
cv1 = 1816.
cv2 = 1040.
gm1 = 2.35
gm2 = 1.43
q1 = -1167e3
q2 = 2030e3
qp1 = 0.
qp2 = -23e3



#Dodecane
# pinfty1 = 4e8
# pinfty2 = 0.0
# cp1 = 2534.
# cp2 = 2005.
# cv1 = 1077.
# cv2 = 1956.
# gm1 = 2.35
# gm2 = 1.025
# q1 = -755e3
# q2 = -237e3
# qp1 = 0.
# qp2 = -24e3




temper = np.linspace(350,480,200)

# Constants
coeff_a = (cp1-cp2+qp2-qp1)/(cp2-cv2)
coeff_b = (q1-q2)/(cp2-cv2)
coeff_c = (cp2-cp1)/(cp2-cv2)
coeff_d = (cp1-cv1)/(cp2-cv2)

# print(coeff_a, coeff_b)


def steffenson(temper,f):
	pguess = 3000.00
	error = 100.0
	tol = 1e-4

	while (np.abs(error) > tol) : 

		y = f(pguess,temper)  


		fprime =  (f(pguess + y,temper) - f(pguess,temper)) / y


		pguess = pguess - (y/fprime)


		


		error = f(pguess,temper)
	


	
	return pguess

# Saurel
g = lambda p,temper: np.log(p+pinfty2)- coeff_a-coeff_b/temper-coeff_c*np.log(temper)-coeff_d*np.log(p+pinfty1)

# # Zein gibbs 
f = lambda p,temper: (gm1*cv1-qp1)*temper-cv1*temper*np.log( (temper**gm1) / (p+pinfty1)**(gm1-1)  ) + q1 \
	-(gm2*cv2-qp2)*temper+cv2*temper*np.log( (temper**gm2) / (p+pinfty2)**(gm2-1)  ) - q2


p1 = np.zeros_like(temper)
p2 = np.zeros_like(temper)

for i, Ti in enumerate(temper):
	p1[i] = steffenson(Ti,g)
	p2[i] = steffenson(Ti,f)



with open('psat.txt', 'w') as file1:
	for pi in p1:
		file1.write("%s\n" %pi)


with open('tsat.txt', 'w') as file2:
	for ti in temper:
		file2.write("%s\n" %ti)


# print(p1/1.e5)




plt.plot(temper, p1/1.e5, 'b-',label='Saurel')
plt.plot(temper, p2/1.e5, '-r',label='Zein')
# plt.title('Water Air')
plt.title('Dodecane')
plt.ylabel('P [atm]')
plt.xlabel('T [K]')
plt.legend()
# plt.savefig('WaterSatPvsT.png', bbox_inches='tight',dpi=100)
plt.savefig('DODECANErSatPvsT.png', bbox_inches='tight',dpi=100)

# plt.show()
