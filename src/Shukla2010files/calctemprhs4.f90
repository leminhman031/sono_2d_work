!***********************************************************************
      subroutine calctemprhs4(currphi)
!***********************************************************************

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: temp


      integer, intent(in) :: currphi 



      psi = 0.00
      netrho = 0.00
      call BC(2)


! reset temp array to zero for array parts that correspond to netrho
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
          temp(i,j,k,2) = 0.000
          tmp = abs(phi(i,j,k,currphi))**alpha
          psi(i,j,k,currphi) = tmp/&
            &(tmp + abs(1-phi(i,j,k,currphi))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG

      call BC(5)

! ----------------------------------------------------------------------
!     Loop through Axial Direction 
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI+1

! 1st need to calculate Phi, its derivatives, and normal
! vector at leftcell face
! phi at leftcell face is calculated from Psi at leftcell face
! phim is phi at leftcell face, and psim is psi at leftcell face
        psim = 0.5*(psi(i-1,j,k,currphi)+psi(i,j,k,currphi))
        tmp = abs(psim)**(1.0/alpha)
        phim = 0.5*(phi(i-1,j,k,currphi)+phi(i,j,k,currphi))
! calculate psi derivatives at leftcell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at leftcell face
          psix = (psi(i,j,k,currphi) - psi(i-1,j,k,currphi))/delx
          psiy = 0.00
          psiz = 0.00
#if probdim==2
          psiy = 0.25*(psi(i,j+1,k,currphi)&
                    & - psi(i,j-1,k,currphi) + &
                     & psi(i-1,j+1,k,currphi)&
                    & - psi(i-1,j-1,k,currphi))/dely
#elif probdim==3
          psiy = 0.25*(psi(i,j+1,k,currphi)&
                    & - psi(i,j-1,k,currphi) + &
                     & psi(i-1,j+1,k,currphi)&
                    & - psi(i-1,j-1,k,currphi))/dely

          psiz = 0.25*(psi(i-1,j,k+1,currphi)&
                    & - psi(i-1,j,k-1,currphi)+&
                     & psi(i,j,k+1,currphi)&
                    & - psi(i,j,k-1,currphi))/delz
#endif

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at 
        phix = tmp2*psix/alpha
        phiy = tmp2*psiy/alpha
        phiz = tmp2*psiz/alpha 

! magnitude of gradient of phi
        tmp = sqrt(phix**2 + phiy**2+phiz**2 + eps*eps)

! calculate normal vector 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody from Shukla 2010 
!     paper page 7419
        rho_x = (T(i,j,k,0) - T(i-1,j,k,0))/delx
        rho_y = 0.00
        rho_z = 0.00
#if probdim==2
        rho_y = 0.25*(T(i,j+1,k,0)&
                  & - T(i,j-1,k,0)&
                  & + T(i-1,j+1,k,0) - &
                   &  T(i-1,j-1,k,0))/dely
#elif probdim==3
        rho_y = 0.25*(T(i,j+1,k,0)&
                  & - T(i,j-1,k,0)&
                  & + T(i-1,j+1,k,0) - &
                   &  T(i-1,j-1,k,0))/dely

        rho_z = 0.25*(T(i-1,j,k+1,0) - T(i-1,j,k-1,0) + &
                    & T(i,j,k+1,0) - T(i,j,k-1,0))/delz
#endif

! calculate fbar from Shukla 2010 paper page 7419
        tmp2 = -1.0*lsrho*(nx*rho_x + ny*rho_y + nz*rho_z)

! calculate first term of rhs density correction equation 23 from 
!     Shukla 2010 paper page 7419
         rhom = 0.5*(T(i-1,j,k,0)+T(i,j,k,0))

        if(i<NI+1) then
          netrho(i,j,k,1) = netrho(i,j,k,1) + rhom/delx  
          netrho(i,j,k,2) = netrho(i,j,k,2) + tmp2/delx 
        endif
        if(i>1) then
          netrho(i-1,j,k,1) = netrho(i-1,j,k,1) - rhom/delx
          netrho(i-1,j,k,2) = netrho(i-1,j,k,2) - tmp2/delx
          temp(i-1,j,k,2) = &
          &(1.0-2.0*phi(i-1,j,k,currphi))*rhonxcent(i-1,j,k,currphi) &
      & *netrho(i-1,j,k,1) +rhonxcent(i-1,j,k,currphi)*netrho(i-1,j,k,2)
        endif

      enddo !i=1,NI+1
      enddo !j=1,NJ
      enddo !k=1,NK

      netrho = 0.00
#if probdim==1
      goto 114
#endif

! ----------------------------------------------------------------------
!     Loop through TRANSVERSE DIRECTION
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ+1
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!     at bot cell face
! phi at bot cell face is calculated from Psi at bot cell face
! phim is phi at bot cell face, and psim is psi at bot cell face
        psim = 0.5*(psi(i,j-1,k,currphi)+psi(i,j,k,currphi))
        tmp = abs(psim)**(1.0/alpha)
        phim = 0.5*(phi(i,j-1,k,currphi)+phi(i,j,k,currphi))
! calculate psi derivatives at bot cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at bot cell face
        psix = 0.25*(psi(i+1,j,k,currphi) - psi(i-1,j,k,currphi) + &
                & psi(i+1,j-1,k,currphi) - psi(i-1,j-1,k,currphi))/delx

        psiy = (psi(i,j,k,currphi) - psi(i,j-1,k,currphi))/dely
        psiz = 0.00

#if probdim==3
        psiz = 0.25*(psi(i,j-1,k+1,currphi) - psi(i,j-1,k-1,currphi) + &
                   & psi(i,j,k+1,currphi) - psi(i,j,k-1,currphi))/delz
#else
        psiz = 0.00
#endif

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at 
!     bot cell face ()
        phix = tmp2*psix/alpha 
        phiy = tmp2*psiy/alpha
        phiz = tmp2*psiz/alpha  
        if (j==1) then 
          phix = 0.000 
        endif

! magnitude of gradient of phi 
        tmp = sqrt(phix**2 + phiy**2 +phiz**2+ eps*eps)

        if( tmp==0.0) then
         print*,"trans direction: ",i,j,phix,phiy,eps,tmp
         stop
        endif

! calculate normal vector at 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody at  from Shukla 2010 
!     paper page 7419
        rho_z = 0.00
        rho_y = (T(i,j,k,0) - T(i,j-1,k,0))/dely
        rho_x = 0.25*(T(i+1,j,k,0) - T(i-1,j,k,0) + T(i+1,j-1,k,0) - &
               &  T(i-1,j-1,k,0))/delx
#if probdim==3
        rho_z = 0.25*(T(i,j-1,k+1,0) - T(i,j-1,k-1,0) + &
                   & T(i,j,k+1,0) - T(i,j,k-1,0))/delz
#endif

! calculate fbar from Shukla 2010 paper page 7419
        tmp2 = -1*lsrho2*(nx*rho_x + ny*rho_y + nz*rho_z)

! calculate first term of rhs density correction equation 23 from 
!     Shukla 2010 paper page 7419
         rhom = 0.5*(T(i,j-1,k,0)+T(i,j,k,0))

        if(j<NJ+1) then
          netrho(i,j,k,1) = netrho(i,j,k,1) + rhom/dely  
          netrho(i,j,k,2) = netrho(i,j,k,2) + tmp2/dely 
        endif
        if(j>1) then
          netrho(i,j-1,k,1) = netrho(i,j-1,k,1) - rhom/dely
          netrho(i,j-1,k,2) = netrho(i,j-1,k,2) - tmp2/dely
          temp(i,j-1,k,2) = temp(i,j-1,k,2) + & 
          (1.0-2.0*phi(i,j-1,k,currphi))*rhonycent(i,j-1,k,currphi)*&
                        & netrho(i,j-1,k,1) &
                        + rhonycent(i,j-1,k,currphi)*netrho(i,j-1,k,2)
        endif
      enddo !i=1,NI
      enddo !j=1,NJ+1
      enddo !k=1,NK

      netrho = 0.00
#if probdim==2
      goto 114
#endif

! ----------------------------------------------------------------------
!     THIRD (DEPTH) DIRECTION  
! ----------------------------------------------------------------------
      do k=1,NK+1
      do j=1,NJ
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!     at bot cell face
! phi at back cell face is calculated from Psi at back cell face
! phim is phi at back cell face, and psim is psi at back cell face
        psim = 0.5*(psi(i,j,k-1,currphi)+psi(i,j,k,currphi))
        tmp = abs(psim)**(1.0/alpha)
        phim = 0.5*(phi(i,j,k-1,currphi)+phi(i,j,k,currphi))
! calculate psi derivatives at back cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at back cell face
        psiz = (psi(i,j,k,currphi) - psi(i,j,k-1,currphi))/delz

        psiy = 0.25*(psi(i,j+1,k,currphi) - psi(i,j-1,k,currphi) + &
               & psi(i,j+1,k-1,currphi) - psi(i,j-1,k-1,currphi))/dely

        psix = 0.25*(psi(i+1,j,k,currphi) - psi(i-1,j,k,currphi) + &
               & psi(i+1,j,k-1,currphi) - psi(i-1,j,k-1,currphi))/delz

        tmp = abs(phim*(1.0-phim))**(1.0-alpha)
        tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
        tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at 
!     back cell face ()
        phix = tmp2*psix/alpha 
        phiy = tmp2*psiy/alpha 
        phiz = tmp2*psiz/alpha 


! magnitude of gradient of phi 
        tmp = sqrt(phix**2 + phiy**2 +phiz**2+ eps*eps)


! calculate normal vector at 
        nx = phix/tmp
        ny = phiy/tmp
        nz = phiz/tmp

! calculate drhodx and drhody at  from Shukla 2010 
!     paper page 7419
        rho_z = (T(i,j,k,0) - T(i,j,k-1,0))/delz

        rho_y = 0.25*(T(i,j+1,k,0) - T(i,j-1,k,0) + &
                   & T(i,j+1,k-1,0) - T(i,j-1,k-1,0))/dely

        rho_x = 0.25*(T(i+1,j,k,0) - T(i-1,j,k,0) + &
                   & T(i+1,j,k-1,0) - T(i-1,j,k-1,0))/delz

! calculate fbar from Shukla 2010 paper page 7419
        tmp2 = -1*lsrho*(nx*rho_x + ny*rho_y+ nz*rho_z)

! calculate first term of rhs density correction equation 23 from 
!     Shukla 2010 paper page 7419
         rhom = 0.5*(T(i,j,k-1,0)+T(i,j,k,0))

        if(k<NK+1) then
          netrho(i,j,k,1) = netrho(i,j,k,1) + rhom/delz  
          netrho(i,j,k,2) = netrho(i,j,k,2) + tmp2/delz 
        endif
        if(k>1) then
          netrho(i,j,k-1,1) = netrho(i,j,k-1,1) - rhom/delz
          netrho(i,j,k-1,2) = netrho(i,j,k-1,2) - tmp2/delz
          temp(i,j,k-1,2) = temp(i,j,k-1,2) + & 
         (1.0-2.0*phi(i,j,k-1,currphi))*rhonzcent(i,j,k-1,currphi)*&
                        & netrho(i,j,k-1,1) &
                        + rhonzcent(i,j,k-1,currphi)*netrho(i,j,k-1,2)
        endif

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK+1



114   continue 


      end subroutine calctemprhs4
!***********************************************************************
