!***********************************************************************
      subroutine shukla2010five
!***********************************************************************
! This subroutine applies Shukla's 2010 interface correction scheme


! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: temp



      temp = 0.00
      tmp = 0.00
      psi = 0.00
      res = 1e5

      tol =tau**2


      call BC(2)
      call BC(4)
      call BC(3)


! ----------------------------------------------------------------------
!     Calculate cell center Psi with knowing Phi for (nmat) Phi
! ----------------------------------------------------------------------
! This is for (nmat) phi
      do lp = 1, nmat
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
          tmp = abs(phi(i,j,k,lp))**alpha
          psi(i,j,k,lp) = tmp/(tmp + abs(1-phi(i,j,k,lp))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG
      enddo ! lp=1,nmat


! ----------------------------------------------------------------------
!     Loop through and calculate Psix and Psiy for center NX,NY
! ----------------------------------------------------------------------
! This is done for (nmat) phi
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! calculate psi derivatives at cell center from Shukla 2010 paper page 7419
! these will be used to calculate normal vector at cell center
          psix = 0.5*(psi(i+1,j,k,lp) - psi(i-1,j,k,lp))/delx
          psiy = 0.5*(psi(i,j+1,k,lp) - psi(i,j-1,k,lp))/dely
#if probdim==3
          psiz = 0.5*(psi(i,j,k+1,lp) - psi(i,j,k-1,lp))/delz
#else
          psiz = 0.00
#endif
          tmp = sqrt( psix**2 + psiy**2 + psiz**2 + eps*eps)

          phinxcent(i,j,k,lp) = psix/tmp
          phinycent(i,j,k,lp) = psiy/tmp
          phinzcent(i,j,k,lp) = psiz/tmp

          rhonxcent(i,j,k,lp) = psix/tmp
          rhonycent(i,j,k,lp) = psiy/tmp
          rhonzcent(i,j,k,lp) = psiz/tmp
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat


! ----------------------------------------------------------------------
!     Begin Interface Reinitialization
! ----------------------------------------------------------------------
      restotal = 1e5
      niter_reinit = 0

      do while( (niter_reinit < iter_reinit) .AND. (restotal> tol) )

      call BC(2)
      call BC(4)
! SET ORIGINAL PHI FIELD TO TEMPORARY ARRAY
      tempphi = phi
! SET ORIGINAL RHOMAT FIELD TO TEMPORARY ARRAY
! Assign 0-stage RK values of RHOMAT to temp array
      temprhomat = rhomat

! following is case/switch code based on rk_reinit
! if rk_reinit=2 then do RK2 scheme
! if rk_reinit=3 then do RK3 scheme

      select case (rk_reinit)
!     2 STAGE R-K SCHEME
      case (2)
!!!!! DO 1ST STAGE OF RK2

      do lp = 1, nmat
! calculate RHS of phi correction equation
        if(lp.ne.nmed)call calcphirhs(lp)
! calculate RHS of rho correction equation
        call calcrhorhs5(lp)
      enddo

      do lp = 1, nmat
      do k=1,NK 
      do j=1,NJ 
      do i=1,NI
         phi(i,j,k,lp) = tempphi(i,j,k,lp) + tau*tempphi2(i,j,k,lp)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat-1

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 1st stage phi
      call updatelastphi


! DO 1ST STAGE OF RK2 FOR DENSITY
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                    & + tau*temp(i,j,k,lp+nmat)
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat
! apply rho boundary condition using 1st stage rho
      call BC(4) 

!!!!! DO 2nd STAGE OF RK2
      do lp = 1, nmat
! calculate RHS of phi correction equation
        if(lp.ne.nmed)call calcphirhs(lp)
! calculate RHS of rho correction equation
        call calcrhorhs5(lp)
      enddo


      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         phi(i,j,k,lp) = 0.5*tempphi(i,j,k,lp) +&
                        & 0.5*phi(i,j,k,lp) +&
                        & 0.5*tau*tempphi2(i,j,k,lp)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat-1


      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 2nd stage phi
      call updatelastphi


! DO 2nd STAGE OF RK2 for density
      do lp = 1, nmat
      do k=1,NK 
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rhomat(i,j,k,lp) = 0.5*temprhomat(i,j,k,lp) +&
                           & 0.5*rhomat(i,j,k,lp) +&
                           & 0.5*tau*temp(i,j,k,lp+nmat) 
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat
! apply rho boundary condition using 2nd stage rho
      call BC(4)



!_______________________________________________________
!     3 STAGE R-K SCHEME
      case (3)
      do lp = 1, nmat
! calculate RHS of phi correction equation
        if(lp.ne.nmed)call calcphirhs(lp)
! calculate RHS of rho correction equation
        call calcrhorhs5(lp)
      enddo

! DO 1ST STAGE OF RK3 FOR DENSITY
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rhomat(i,j,k,lp) = temprhomat(i,j,k,lp)&
                            & + tau*temp(i,j,k,lp+nmat)
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat
! apply rho boundary condition using 1st stage rho
      call BC(4) 

!!!!! DO 1ST STAGE OF RK3
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         phi(i,j,k,lp) = tempphi(i,j,k,lp) + tau*tempphi2(i,j,k,lp)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat-1

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 1st stage phi
      call updatelastphi

      do lp = 1, nmat
! calculate RHS of phi correction equation
        if(lp.ne.nmed)call calcphirhs(lp)
! calculate RHS of rho correction equation
        call calcrhorhs5(lp)
      enddo

! DO 2nd STAGE OF RK3 for density
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rhomat(i,j,k,lp) = 0.75*temprhomat(i,j,k,lp) +&
                           & 0.25*rhomat(i,j,k,lp) +&
                           & 0.25*tau*temp(i,j,k,lp+nmat) 
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat
! apply rho boundary condition using 2nd stage rho
      call BC(4)


!!!!! DO 2nd STAGE OF RK3
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         phi(i,j,k,lp) = 0.75*tempphi(i,j,k,lp) +&
                        & 0.25*phi(i,j,k,lp) +&
                        & 0.25*tau*tempphi2(i,j,k,lp)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat-1

      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 2nd stage phi
      call updatelastphi

      do lp = 1, nmat
! calculate RHS of phi correction equation
        if(lp.ne.nmed)call calcphirhs(lp)
! calculate RHS of rho correction equation
        call calcrhorhs5(lp)
      enddo

      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,nmed)<(1.0-phitol)) .AND. &
              (phi(i,j,k,nmed)>phitol) ) then
            rhomat(i,j,k,lp) = (1.0/3.0)*temprhomat(i,j,k,lp) +&
                           & (2.0/3.0)*rhomat(i,j,k,lp) +&
                           & (2.0/3.0)*tau*temp(i,j,k,lp+nmat)
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat
! apply rho boundary condition using 2nd stage rho
      call BC(4)


!!!!! DO 3rd STAGE OF RK3
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         phi(i,j,k,lp) = (1.0/3.0)*tempphi(i,j,k,lp) +&
                         & (2.0/3.0)*phi(i,j,k,lp) +&
                         & (2.0/3.0)*tau*tempphi2(i,j,k,lp)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat-1


      where (phi<0.00) phi=0.0000
      where (phi>1.00) phi=1.0000
! apply phi boundary condition using 3rd stage phi
      call updatelastphi

      end select


!**********************************************************
! CALCULATE MAXIMUM RESIDUE OCCURING 
      res1 = 0.0
      do lp = 1, nmat
      if(lp.ne.nmed) then
        do k=1,NK
        do j=1,NJ
        do i=1,NI
          if (res1 < abs(tempphi(i,j,k,lp)-phi(i,j,k,lp))) then
            res1 = abs(tempphi(i,j,k,lp)-phi(i,j,k,lp))
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo !k=1,NK
      endif
      enddo !lp=1,nmat-1


      res2 = 0.0
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         if (res2 < abs(temprhomat(i,j,k,lp)-rhomat(i,j,k,lp))) then
              res2 = abs(temprhomat(i,j,k,lp)-rhomat(i,j,k,lp))
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat

!**********************************************************

      niter_reinit = niter_reinit + 1
      resphi = res1
      resrho = res2
      restotal = max(res1,res2)

      enddo !end_do_while loop


! set used arrays to 0 before leaving function
      tempphi = 0.0000

! set used arrays to 0 before leaving function
      temprho = 0.0000
      temprhomat = 0.0000
      temp = 0.0000
      phinxcent = 0.0000
      phinycent = 0.0000
      phinzcent = 0.0000
      rhonycent = 0.0000
      rhonxcent = 0.0000
      rhonzcent = 0.0000


      end subroutine shukla2010five
!***********************************************************************
