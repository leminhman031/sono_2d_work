!***********************************************************************
      subroutine zz_rho(dl_mass,phatmass)
!***********************************************************************
! This subroutine applies Shukla's 2010 interface correction scheme
! this scheme has correction factor for mass

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: temp

      real, intent(inout) :: dl_mass, phatmass

      temp = 0.00
      tmp = 0.00

! SET ORIGINAL RHO FIELD TO TEMPORARY ARRAY
! Assign 0-stage RK values of RHO to temp array
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         temprho(i,j,k,1) = rho(i,j,k)
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k

! following is case/switch code based on rk_reinit
! if rk_reinit=2 then do RK2 scheme
! if rk_reinit=3 then do RK3 scheme

      select case (rk_reinit)
!   2 STAGE R-K SCHEME
      case (2)
!!!!! DO 1ST STAGE OF RK2
! calculate RHS of rho correction equation
      call calcrhorhs4 
! DO 1ST STAGE OF RK2 FOR DENSITY
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,comphi)<(1.0-phitol)) .AND. &
              (phi(i,j,k,comphi)>phitol) ) then
            rho(i,j,k) = temprho(i,j,k,1)&
                  & + tau*temp(i,j,k,2)+ tau*dl_mass
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition using 1st stage rho
      call BC(3) 


!!!!! DO 2nd STAGE OF RK2
! calculate RHS of rho correction equation
      call calcrhorhs4 

! DO 2nd STAGE OF RK2 for density
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,comphi)<(1.0-phitol)) .AND. &
              (phi(i,j,k,comphi)>phitol) ) then
            rho(i,j,k) = 0.5*temprho(i,j,k,1) +&
                     & 0.5*rho(i,j,k) +&
                     & 0.5*tau*temp(i,j,k,2)+ 0.5*tau*dl_mass
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition using 2nd stage rho
      call BC(3)




!_______________________________________________________
!     3 STAGE R-K SCHEME
      case (3)
!!!!! DO 1ST STAGE OF RK3

! calculate RHS of rho correction equation
      call calcrhorhs4 

! DO 1ST STAGE OF RK3 FOR DENSITY
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,comphi)<(1.0-phitol)) .AND. &
              (phi(i,j,k,comphi)>phitol) ) then
            rho(i,j,k) = temprho(i,j,k,1)&
                & + tau*temp(i,j,k,2)+ tau*dl_mass
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition using 1st stage rho
      call BC(3) 


!!!!! DO 2nd STAGE OF RK3
! calculate RHS of rho correction equation
      call calcrhorhs4 

! DO 2nd STAGE OF RK3 for density
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,comphi)<(1.0-phitol)) .AND. &
              (phi(i,j,k,comphi)>phitol) ) then
            rho(i,j,k) = 0.75*temprho(i,j,k,1) +&
                     & 0.25*rho(i,j,k) +&
                     & 0.25*tau*temp(i,j,k,2)+ 0.25*tau*dl_mass
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition 
      call BC(3)


!!!!! DO 3rd STAGE OF RK3
! calculate RHS of rho correction equation
      call calcrhorhs4 
      do k=1,NK
      do j=1,NJ 
      do i=1,NI
         if( (phi(i,j,k,comphi)<(1.0-phitol)) .AND. &
              (phi(i,j,k,comphi)>phitol) ) then
            rho(i,j,k) = (1.0/3.0)*temprho(i,j,k,1) +&
                     & (2.0/3.0)*rho(i,j,k) +&
                  & (2.0/3.0)*tau*temp(i,j,k,2)+ (2.0/3.0)*tau*dl_mass
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
! apply rho boundary condition 
      call BC(3)

       end select


!  CALCULATE PARTICLE MASS
      phatmass = 0.0000
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         phatmass = phatmass+ rho(i,j,k)*vol(i,j,k)*phi(i,j,k,1)
      enddo
      enddo
      enddo

! set used arrays to 0 before leaving function
      temprho = 0.0000
      temp = 0.0000




      end subroutine zz_rho
!***********************************************************************
