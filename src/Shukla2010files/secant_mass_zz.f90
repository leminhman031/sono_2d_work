!***********************************************************************
      subroutine secant_mass_zz()
!***********************************************************************
! This subroutine applies Shukla's interface correction scheme from 2010 
! this scheme has correction factor for  mass

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp
      use RungKutt3, only: temp


      temp = 0.00
      tmp = 0.00
      psi = 0.00
      res = 1e5

      tol =tau**2


      call BC(2)
      call BC(4)
      call BC(3)


! ----------------------------------------------------------------------
!     Calculate cell center Psi with knowing Phi for (nmat) Phi
! ----------------------------------------------------------------------
! This is for (nmat) phi
      do lp = 1, nmat
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
          tmp = abs(phi(i,j,k,lp))**alpha
          psi(i,j,k,lp) = tmp/(tmp + abs(1-phi(i,j,k,lp))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG
      enddo ! lp=1,nmat


! ----------------------------------------------------------------------
!     Loop through and calculate Psix and Psiy for center NX,NY,NZ
! ----------------------------------------------------------------------
! This is done for (nmat) phi
      do lp = 1, nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
! calculate psi derivatives at cell center
! from Shukla 2010 paper page 7419
! these will be used to calculate normal vector at cell center
          psix = 0.5*(psi(i+1,j,k,lp) - psi(i-1,j,k,lp))/delx
          psiy = 0.5*(psi(i,j+1,k,lp) - psi(i,j-1,k,lp))/dely
#if probdim==3
          psiz = 0.5*(psi(i,j,k+1,lp) - psi(i,j,k-1,lp))/delz
#else
          psiz = 0.00
#endif
          tmp = sqrt( psix**2 + psiy**2 + psiz**2 + eps*eps)

          phinxcent(i,j,k,lp) = psix/tmp
          phinycent(i,j,k,lp) = psiy/tmp
          phinzcent(i,j,k,lp) = psiz/tmp

          rhonxcent(i,j,k,lp) = psix/tmp
          rhonycent(i,j,k,lp) = psiy/tmp
          rhonzcent(i,j,k,lp) = psiz/tmp
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK
      enddo !lp=1,nmat

! ----------------------------------------------------------------------
!     Begin Iteration to correct phi/rho while conserving mass
! ----------------------------------------------------------------------
      restotal = 1e5
      niter_reinit = 0

      do while( (niter_reinit < iter_reinit) .AND. (restotal> tol) )

      tempphi3=phi
      temprhomat3=rhomat
      temprho3=rho

!  SET PARTICLE vol/mass BEFORE CORRECTION
         do lp=1,num_particles
         if(giter.gt.0) then 
           g1(lp) = origvol(lp)
           g2(lp) = origmass(lp)
         else 
           pmass(lp) = 0.0000
           pvol(lp) = 0.0000
           do k=1,NK
           do j=1,NJ
           do i=1,NI
              pvol(lp) = pvol(lp) + phi(i,j,k,nmed+lp)*vol(i,j,k)
              pmass(lp) = pmass(lp) + rhomat(i,j,k,nmed+lp)*vol(i,j,k)
           enddo
           enddo
           enddo
           g1(lp) = pvol(lp)
           g2(lp) = pmass(lp)
           origvol(lp)  = g1(lp)
           origmass(lp) = g2(lp)
         endif
         enddo



!!!!!!!! integrate for phi with no constraint
      lamda_vol=0.0
      call zz_phi(lamda_vol,pvol)
      call updatelastphi

!!!!!!!! secant method for finding lamda_mass

      if(giter.eq.0)lamda_mass=1.e-5

      ! first guess for secant method
      lamda0=lamda_mass
      rhomat=temprhomat3
      rho=temprho3
      call zz_rhomat(lamda0,pmass)
      do lp=1,num_particles
        vlm0(lp) = (g2(lp) - pmass(lp))/g2(lp) ! TLJ
      enddo

      ! 2nd guess for secant method
      lamda1=lamda0*0.9
      rhomat=temprhomat3
      rho=temprho3
      call zz_rhomat(lamda1,pmass)
      do lp=1,num_particles
        vlm1(lp) = (g2(lp) - pmass(lp))/g2(lp) ! TLJ
      enddo


! originally set error really high so that dowhile loop begins
      error = 1e5  
      vtol = 1.0E-8
      lamiter = 0
    ! Iterate by secant method
      do while(error> vtol .AND. lamiter < 100 )
        do lp=1,num_particles
          lamdanew(lp) = lamda1(lp) - vlm1(lp)*&
                      & (lamda1(lp)-lamda0(lp))/(vlm1(lp)-vlm0(lp)) 
          lamda0(lp) = lamda1(lp) ! set new lamda0 as old lamda1
! set new lamda1 as the calculated lamdanew
          lamda1(lp) = lamdanew(lp) 
          vlm0(lp) = vlm1(lp)  ! set the new v0 as the old v1
        enddo

      rhomat=temprhomat3
      rho=temprho3
      call zz_rhomat(lamdanew,pmass)
      error = 0.0
      do lp=1,num_particles
        vlm1(lp) = (g2(lp) - pmass(lp))/g2(lp) ! TLJ
        if(abs(vlm1(lp)).gt.error) error=abs(vlm1(lp))
      enddo

      lamiter = lamiter + 1

      enddo ! end secent method on lamda

      lamda_mass=lamdanew

!!!!!!!! calculate the maximum residue occurring
      res1 = 0.0
      do lp = 1, nmat
      if(lp.ne.nmed) then
        do k=1,NK
        do j=1,NJ
        do i=1,NI
          if (res1 < abs(tempphi3(i,j,k,lp)-phi(i,j,k,lp))) then
            res1 = abs(tempphi3(i,j,k,lp)-phi(i,j,k,lp))
          endif
        enddo !i=1,NI
        enddo !j=1,NJ
        enddo !k=1,NK
      endif
      enddo !lp=1,nmat-1

      res2 = 0.0
      do lp=1,nmat
      do k=1,NK
      do j=1,NJ
      do i=1,NI
         if (res2 < abs(temprhomat3(i,j,k,lp)-rhomat(i,j,k,lp))) then
           res2 = abs(temprhomat3(i,j,k,lp)-rhomat(i,j,k,lp))
         endif
      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k
      enddo 


      niter_reinit = niter_reinit + 1
      resphi = res1
      resrho = res2
      restotal = max(res1,res2)


      enddo !end_do_while loop

!  CALCULATE PARTICLE VOL/mass
         do lp=1,num_particles
           pmass(lp) = 0.0000
           pvol(lp) = 0.0000
           do k=1,NK
           do j=1,NJ
           do i=1,NI
              pvol(lp) = pvol(lp) + phi(i,j,k,nmed+lp)*vol(i,j,k)
              pmass(lp) = pmass(lp) + rhomat(i,j,k,nmed+lp)*vol(i,j,k)
           enddo
           enddo
           enddo
         enddo

      if (giter.eq.0) then
        print*,'Mass Correction Only, GITER :',giter
        do lp=1,num_particles
        print*,'  Particle number   :',lp
        print*,'    lamda_mass      :',lamda_mass(lp)
        print*,'    lamda_vol      :',lamda_vol(lp)
        print*,'    particle VOL    :',pvol(lp)
        print*,'    particle MASS   :',pmass(lp)
        enddo
      endif

      end subroutine secant_mass_zz
!***********************************************************************
