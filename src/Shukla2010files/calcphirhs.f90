!***********************************************************************
      subroutine calcphirhs(currphi)
!***********************************************************************
! calculates right hand side of Shukla 2010 phi correction equation


! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010

      integer, intent(in) :: currphi 


      psi = 0.00
      netphi = 0.00


      call BC(2)
! ----------------------------------------------------------------------
!     Calculate Psi with knowing Phi for (nmat-1) Phi
! ----------------------------------------------------------------------
      do k=(-NG+1),NK+NG
      do j=(-NG+1),NJ+NG
      do i=(-NG+1),NI+NG
        tempphi2(i,j,k,currphi)=0.00
        tmp = abs(phi(i,j,k,currphi))**alpha
        psi(i,j,k,currphi) = tmp/(&
                            &tmp + abs(1-phi(i,j,k,currphi))**alpha)
      enddo !i=(-NG+1),NI+NG
      enddo !j=(-NG+1),NJ+NG
      enddo !k=(-NG+1),NK+NG



! ----------------------------------------------------------------------
!     Loop through Axial Direction and calculate f (shukla2010 pg7419)
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ
      do i=1,NI+1
! 1st need to calculate Phi, its derivatives, and normal vector 
!     at left cell face
! phi at left cell face is calculated from Psi at left cell face
! phim is phi at left cell face, and psim is psi at left cell face
          psim = 0.5*(psi(i-1,j,k,currphi)+psi(i,j,k,currphi))
          tmp = abs(psim)**(1.0/alpha)
          phim = tmp/(tmp + abs(1.0-psim)**(1.0/alpha))

! calculate psi derivatives at left cell face from Shukla 2010 
!     paper page 7419
! these will be used to calculate phim derivatives at left cell face
          psix = (psi(i,j,k,currphi) - psi(i-1,j,k,currphi))/delx
          psiy = 0.00
          psiz = 0.00
#if probdim==2
          psiy = 0.25*(psi(i,j+1,k,currphi) - &
                     & psi(i,j-1,k,currphi) + &
                     & psi(i-1,j+1,k,currphi) - &
                     & psi(i-1,j-1,k,currphi))/dely
#elif probdim==3
          psiy = 0.25*(psi(i,j+1,k,currphi) - psi(i,j-1,k,currphi) + &
                     & psi(i-1,j+1,k,currphi) - &
                     & psi(i-1,j-1,k,currphi))/dely

          psiz = 0.25*(psi(i-1,j,k+1,currphi) - psi(i-1,j,k-1,currphi)+&
                     & psi(i,j,k+1,currphi) - psi(i,j,k-1,currphi))/delz
#endif

          tmp = abs(phim*(1.0-phim))**(1.0-alpha)
          tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
          tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at left cell face
          phix = tmp2*psix/alpha 
          phiy = tmp2*psiy/alpha
          phiz = tmp2*psiz/alpha 

! this tmp is f(i-0.5,j,k) from shukla 2010 page 7419
          tmp = phim*(1.0-phim) - lsphi*sqrt(phix**2+phiy**2+phiz**2)

          if(i>1) then
            netphi(i-1,j,k,currphi) = netphi(i-1,j,k,currphi) - tmp/delx
          endif
          if(i<NI+1) then
            netphi(i,j,k,currphi) = netphi(i,j,k,currphi) + tmp/delx
          endif
          if(i>1) then
            tempphi2(i-1,j,k,currphi) = phinxcent(i-1,j,k,currphi)*&
                    &netphi(i-1,j,k,currphi)
          endif

      enddo !i=1,NI+1
      enddo !j=1,NJ
      enddo !k=1,NK

 
      netphi = 0.000
#if probdim==1
      goto 111
#endif

! ----------------------------------------------------------------------
!     Loop through transverse Direction and calculate g 
!          (shukla2010 pg7419)
! ----------------------------------------------------------------------
      do k=1,NK
      do j=1,NJ+1
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!       at bot cell face
! phi at bot cell face is calculated from Psi at bot cell face
! phim is phi at bot cell face, and psim is psi at bot cell face
          psim = 0.5*(psi(i,j-1,k,currphi)+psi(i,j,k,currphi))
          tmp = abs(psim)**(1.0/alpha)
          phim = tmp/(tmp + abs(1.0-psim)**(1.0/alpha))

! calculate psi derivatives at bot cell face from Shukla 2010 
!       paper page 7419
! these will be used to calculate phim derivatives at bot cell face
          psix = 0.25*(psi(i+1,j,k,currphi) - psi(i-1,j,k,currphi) + &
                     & psi(i+1,j-1,k,currphi) -&
                     & psi(i-1,j-1,k,currphi))/delx

          psiy = (psi(i,j,k,currphi) - psi(i,j-1,k,currphi))/dely

#if probdim==3
          psiz = 0.25*(psi(i,j-1,k+1,currphi) - psi(i,j-1,k-1,currphi)+&
                     & psi(i,j,k+1,currphi) - psi(i,j,k-1,currphi))/delz
#else
          psiz = 0.00
#endif

          tmp = abs(phim*(1.0-phim))**(1.0-alpha)
          tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
          tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at bot cell face
          phix = tmp2*psix/alpha 
          phiy = tmp2*psiy/alpha 
          phiz = tmp2*psiz/alpha 

! this tmp is g(i,j-0.5,k) from shukla 2010 page 7419
          tmp = phim*(1.0-phim) - lsphi*sqrt(phix**2+phiy**2+phiz**2)

! update the netphi array
          if(j>1) then
            netphi(i,j-1,k,currphi) = netphi(i,j-1,k,currphi) - tmp/dely
          endif
          if(j<NJ+1) then
            netphi(i,j,k,currphi) = netphi(i,j,k,currphi) + tmp/dely
          endif
          if(j>1) then
            tempphi2(i,j-1,k,currphi) = tempphi2(i,j-1,k,currphi) + &
               &  phinycent(i,j-1,k,currphi)*netphi(i,j-1,k,currphi)
          endif

      enddo !i=1,NI
      enddo !j=1,NJ+1
      enddo !k=1,NK


      netphi = 0.000
#if probdim==2
      goto 111
#endif


! ----------------------------------------------------------------------
!     THIRD (DEPTH) DIRECTION    (shukla2010 pg7419 h)
! ----------------------------------------------------------------------
      do k=1,NK+1
      do j=1,NJ
      do i=1,NI

! 1st need to calculate Phi, its derivatives, and normal vector 
!       at back cell face
! phi at back cell face is calculated from Psi at back cell face
! phim is phi at back cell face, and psim is psi at back cell face
          psim = 0.5*(psi(i,j,k-1,currphi)+psi(i,j,k,currphi))
          tmp = abs(psim)**(1.0/alpha)
          phim = tmp/(tmp + abs(1.0-psim)**(1.0/alpha))

! calculate psi derivatives at back cell face from Shukla 2010 
!       paper page 7419
! these will be used to calculate phim derivatives at back cell face
          psiz = (psi(i,j,k,currphi) - psi(i,j,k-1,currphi))/delz

          psiy = 0.25*(psi(i,j+1,k,currphi) - psi(i,j-1,k,currphi) + &
                     & psi(i,j+1,k-1,currphi) -&
                     & psi(i,j-1,k-1,currphi))/dely

          psix = 0.25*(psi(i+1,j,k,currphi) - psi(i-1,j,k,currphi) + &
                     & psi(i+1,j,k-1,currphi) -&
                     & psi(i-1,j,k-1,currphi))/delx

          tmp = abs(phim*(1.0-phim))**(1.0-alpha)
          tmp1 = abs(phim)**alpha + abs(1.0-phim)**alpha
          tmp2 = tmp*(tmp1*tmp1)

! phix and phiy are x and y derivatives of phi at back cell face
          phix = tmp2*psix/alpha 
          phiy = tmp2*psiy/alpha 
          phiz = tmp2*psiz/alpha 

! this tmp is h(i,j,k-0.5) from shukla 2010 page 7419
          tmp = phim*(1.0-phim) - lsphi*sqrt(phix**2+phiy**2+phiz**2)

! update the netphi array
          if(k>1) then
            netphi(i,j,k-1,currphi) = netphi(i,j,k-1,currphi) - tmp/delz
          endif
          if(k<NK+1) then
            netphi(i,j,k,currphi) = netphi(i,j,k,currphi) + tmp/delz
          endif
          if(k>1) then
            tempphi2(i,j,k-1,currphi) = tempphi2(i,j,k-1,currphi) + &
               &  phinzcent(i,j,k-1,currphi)*netphi(i,j,k-1,currphi)
          endif

      enddo !i=1,NI
      enddo !j=1,NJ
      enddo !k=1,NK+1


111   continue

      end subroutine calcphirhs
!***********************************************************************
