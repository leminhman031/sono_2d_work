   
subroutine calctemper

    use globalvar
    use gridvariables
    use Shukla2010
    use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
    use RungKutt3
    use mieeos5eqn_ntemp

    real,dimension(1:nmat) :: internal_e
    ! real :: cv_mix
    
    ! fluidprop(1) = 2.35
    ! fluidprop(2) = 4e3

    do i = 1,NI
    do j = 1,NJ
    do k = 1,NK

        
        
        do imat = 1,nmat
                

            ! Calculate Cv mixture  

            ! cv_mix = matprop(1,1)*phi(i,1,1,1)+matprop(1,2)*phi(i,1,1,2)

            ! EQ 41A use fluid prop, cvmix
            internal_e(imat) = (p(i,j,k)+fluidprop(1)*fluidprop(2))/((fluidprop(1)-1)*(rho(i,j,k))) &
                &+ matprop(8,imat)

            T41a(i,j,k,imat) = internal_e(imat)/matprop(1,imat)

            
            !EQ 41B use fluid prop, cvmix
            T41b(i,j,k,imat) = (p(i,j,k)+fluidprop(2))/(rho(i,j,k)*(fluidprop(1)-1)*matprop(1,imat))  

                
    
        end do
    end do
    end do
    end do


    


end subroutine calctemper










