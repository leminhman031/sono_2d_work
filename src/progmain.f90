!***********************************************************************
      subroutine progmain()
!***********************************************************************
! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010, only: resphi,resrho
      use mieeos5eqn_ntemp
      use RungKutt3

      real :: start, finish,rkstart,rkfin,interm,remdt,taumul
      integer:: nwrite,indt,tauint,elapsint,tint,rsttint

      real :: dummy_C

! PRINT THE DATE AND TIME FOR START OF PROGMAIN
      call date_and_time(TIME=time)
      print '(a,a)', 'Start Time: ',time 
      print*, ' '
      print*, 'PROGMAIN STARTED'
      print*, ' '

      ! print*, 'Use source term in Phi or not? Please input 1 for Yes or 2 for No'
      ! read*, kchar 
      kchar = 2



! INITIALIZE, SET TIMET/GITER AND FILE NAME DEPENDING 
!     IF RESTARTFLAG=TRUE
      if (restartflag==1) then
         call initialflowfield
         call readrestart
         giter = giter+1
      else
         call initialflowfield
         timeT = 0.00
         giter = 0
      endif
      taus=tscale

      ! stop

! Format descriptors for printed variables (fstr2), table title (fstr)
      fstr = '(a6,8x,a5,12x,a6,8x,a12,6x,a6,6x,a9,7x,a8,9x,&
              a13,3(4x,a))'
      fstr2 ='(1x,i5,2(4x,es14.8),7x,i3,7x,es11.5,&
             &4x,es11.5,8x,f12.6,3x,&
              f10.3,7x,i2,7x,es11.5)'

      call writefile

      print*,' '
      print*,' note TimeT and DeltaT are both nondim'
      write(*,fstr) 'ITER_#','TimeT','DeltaT','niter_reinit','resPHI',&
      &'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'

      call cpu_time(start)
      tauint = 1
      tint = 1
      rsttint = 1
      elapsint = -1

      ! ORIGINAL DO WHILE 

      !  do while( ((timeT*tscale/taus)<finaltime).AND.&
      !        &(giter<=maxiter) .AND.& 
      !        &((timeT*tscale/taus)<10000.20*tautooutlet) )





      !MAX, PHASE TRANS
      do while( ((timeT*tscale)<finaltime).AND.&
            &(giter<=maxiter))
            

      

      call cpu_time(rkstart)
      call rk3

      call cpu_time(rkfin)

      timeT = timeT + deltat




! WRITE OUTPUT TO THE TIME DATAFILE
      open(unit=35,file=timefile,status='old',position='append',&
           action='write')
      write(35,*)giter,(rkfin-rkstart),niter_reinit,resphi,&
                 &resrho,timeT,deltat,maxspeed,niterT,resT
      close(35,status='keep')


! WRITE OUTPUT TO THE CD DATAFILE
      call updatemixrho
      if(num_particles.gt.0)then
        do m=1,num_particles
          call writedrag_singleparticle(m+nmed)
        enddo
      endif



! if statements for when to print out table title and variables 
!     to window
      if( mod(giter,prntline)==0) then
         call cpu_time(interm)
         write(*,fstr2) giter,(timeT),deltat,niter_reinit,&
           &resphi,resrho,maxspeed,(interm-start),niterT,resT
      endif
      if( (mod(giter,prntttle)==0) .and. (mod(giter,outiter)/=0) ) then
         print*,' '
         print*,' note TimeT and DeltaT are both nondim'
         write(*,fstr) 'ITER_#','TimeT','DeltaT',&
                        &'niter_reinit','resPHI',&
      &'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
      endif

! if statement for when to write datafile for plotting later
! writes based on t/taus
      elapsint = elapsint+1
      taumul = timeT*tscale/taus/outtau
      indt = floor(taumul)
      remdt = taumul-real(indt)
!      if( (giter>0) .and. indt.eq.tauint )then
!          tauint = tauint + 1
!          elapsint = 0
!          print*,' '
!          call writefile
!          print*,'ITER and t/taus is ',giter,(timeT*tscale/taus)
!          print*,' '
!          write(*,fstr) 'ITER_#','TimeT','DeltaT',&
!                 &'niter_reinit','resPHI',&
!      &'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
!      endif

      call cpu_time(interm)
      indt = floor(interm-start/3600)
      !if( (giter>0) .and. elapsint.ne.0 .and. indt.eq.tint )then
      !!    tint = tint + 1
      !    elapsint = 0
      !    print*,' '
      !    call writefile
      !    print*,'ITER and t/taus is ',giter,(timeT*tscale/taus)
      !    print*,' '
      !    write(*,fstr) 'ITER_#','TimeT','DeltaT',&
      !           &'niter_reinit','resPHI',&
      !&'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
      !endif

      if( (giter>0)  &
         & .and. (mod(giter,outiter)==0))  then
          print*,' '
          call writefile
          print*,'ITER and t/taus is ',giter,(timeT*tscale/taus)
          print*,' '
          write(*,fstr) 'ITER_#','TimeT','DeltaT',&
                 &'niter_reinit','resPHI',&
      &'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
      endif

! if statement for when to write restart file
      call cpu_time(interm)
      indt = floor(interm-start/3600)
      !if( (giter>0) .and. indt.eq.rsttint )then
      !    rsttint = rsttint + 1
      !    print*,' '
      !    call writerestart
      !    print*,' Elapsed time (hrs) is ',interm-start/3600
      !    print*,' '
      !    write(*,fstr) 'ITER_#','TimeT','DeltaT',&
      !           &'niter_reinit','resPHI',&
      !&'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
      !endif
      if( (giter>0) .and. (mod(giter,outrst)==0) ) then 
         print*,' '
         call writerestart
         print*,' '
         write(*,fstr) 'ITER_#','TimeT','DeltaT',&
                &'niter_reinit','resPHI',&
      &'   resRHO','maxspeed','TimeTaken (s)','nTcorr','resT'
      endif

! if statements to get out of do while loop if near walltime
      call cpu_time(interm)
      if( (interm-start) > walltime) then
         print*,' '
         print*,'Input walltime hit: ',walltime,'seconds'
         call writerestart
         call writefile
         print*,' '
         exit
      endif



     
            
       
      
           giter = giter+1  !THIS ENDS THE BIG TIME LOOP

      enddo

      
    !DO i = 1,NI 
        !if ((phi(i,1,1,1)<(1.0-phiSaureltol)) &
            !&.AND. (phi(i,1,1,1)>(phiSaureltol))) then

                !PRINT*, "INTERFACE YQMIX ", yqmix 
                !STOP 

        !end if 
    !END DO 


      print*,' '
      call cpu_time(finish)
      print '("Elapsed Time = ",f25.3," seconds.")',finish-start
      print*,' '
      call date_and_time(TIME=time)
      print '(a,a)', 'Finish Time: ',time
      call writefile
      call writerestart
      print*,' '

      if (kchar == 1) then 
            print*, 'PROGMAIN FINISHED. WE MODIFIED PHI SOURCE'
      else
            print*, 'PROGMAIN FINISHED. PHI SOURCE = 0'
      end if
                   
      print*, ' '


      ! print*,'PHI1 = ' , phi(667,1,1,1)
      ! print*, 'T1 =',T(667,1,1,1)
      ! print*, 'GAMMA1 =', matprop(2,1)
      ! print*, 'P = ',p(667,1,1)
      ! print*, 'PINFTY = ', matprop(3,1)


      ! dummy_C = (1-(matprop(2,1)*T(667,1,1,1)))&
      !       /(matprop(2,1)*(matprop(3,1)+p(667,1,1)))

      ! print*, 'C1 = ', dummy_C

      end subroutine progmain
!***********************************************************************
