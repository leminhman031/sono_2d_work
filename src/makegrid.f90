!***********************************************************************
      subroutine makegrid
!***********************************************************************
 
!  get variables for grid generation from Module gridvariables
      USE gridvariables
      USE globalvar
      USE Shukla2010, only: lsphi,lsrho,tau


      print*, 'MAKING GRID'

!  calculate cell's axial and transverse width, which are constants
      
      delx = (xmax-xmin)/(NI)
      

#if probdim==1
      dely = delx
      delz = delx
#elif probdim==2
      dely = (ymax-ymin)/NJ
      delz = delx
#elif probdim==3
      dely = (ymax-ymin)/NJ
      delz = (zmax-zmin)/NK
#endif



      print*, '  deltax is  ', delx
      print*, '  deltay is  ', dely
      print*, '  deltaz is  ', delz

! calculate length scale for phi and rho 
      lsphi = 0.500*amin1(delx,dely,delz)
      lsrho = 0.500*amin1(delx,dely,delz)

      print*, '  lsphi is  ', lsphi
      print*, '  lsrho is  ', lsrho

! calculate tau

      tau = 0.2*amin1(delx,dely,delz)

! calculate the (x,y,z) coordinate of 
! left/back/bottom walls of cell (i,j,k)
! and calculate the (x,y,z) coordinate of the center of cell (i,j,k)
! note that the loops go to NI+1,NJ+1,NK+1 
! because we need the right/top/front wall 
! for the last cell, which is left/back/bottom wall 
! of the cell "ahead" of it 
      do k = 1,NK+1
      do j = 1,NJ+1
      do i = 1,NI+1

         if (i<NI+1 .AND. j<NJ+1 .AND. k<NK+1) then
            x(i,j,k)  = xmin + (i-1)*delx
            xc(i,j,k) = x(i,j,k) + 0.5*delx

            y(i,j,k)  = ymin + (j-1)*dely
            yc(i,j,k) = y(i,j,k) + 0.5*dely

            z(i,j,k)  = zmin + (k-1)*delz
            zc(i,j,k) = z(i,j,k) + 0.5*delz
         else
            x(i,j,k) = x(i-1,j,k) + delx
            y(i,j,k) = y(i,j-1,k) + dely
            z(i,j,k) = z(i,j,k-1) + delz
         endif

      enddo 
      enddo 
      enddo 

! write out the cell centers so that matlab can read them in
      do i=1,NI
        write(555,*) xc(i,1,1)
      enddo
      close(555)
      do i=1,NJ
        write(556,*) yc(1,i,1)
      enddo
      close(556)
      do i=1,NK
        write(557,*) zc(1,1,i)
      enddo
      close(557)

! calculate the volume of each cell depending if 2D or axisym geometry
#if GEOMETRY==1 

! cartesian geometry
      vol = dely*delx*delz



#else

      do k = 1,NK
      do j = 1,NJ
      do i = 1,NI
#if probdim==2
! 2d axisym cylinder
         vol(i,j,k) = yc(i,j,k)*dely*delx*2*pie
#endif
      enddo
      enddo 
      enddo 
#endif 

      print*, 'FINISHED MAKING GRID'
      print*, ' '
      print*, ' '

      end subroutine makegrid
!***********************************************************************
