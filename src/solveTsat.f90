subroutine solveTsat(idum,jdum,kdum,tsatout)

    use globalvar
    use gridvariables
    use mieeos5eqn_ntemp
    use RungKutt3
    

    INTEGER, intent(in):: idum,jdum,kdum
    REAL,intent(out) :: tsatout
    REAL :: gibbs1, gibbs2
    REAL :: error

    REAL :: toler = 1e-4

    tsatout = 0.00

    do 
    
        gibbs1 = (matprop(2,1)*matprop(1,1)-matprop(9,1))*tsatout-tsatout*matprop(1,1)&
        *LOG10((tsatout**(matprop(2,1)))/(p(idum,jdum,kdum)+matprop(3,1))**(matprop(2,1)-1))&
        + matprop(8,1)

        gibbs2 = (matprop(2,2)*matprop(1,2)-matprop(9,2))*tsatout-tsatout*matprop(1,2)&
        *LOG10((tsatout**(matprop(2,2)))/(p(idum,jdum,kdum)+matprop(3,2))**(matprop(2,2)-1))&
        + matprop(8,2)

        print*, gibbs1
        print*, gibbs2
    
        error = abs(gibbs2-gibbs1)

        if (error .LE. toler) then 
            exit
        end if

        tsat = tsat + 100
    end do



    
    






end subroutine solveTsat