subroutine gibbsreturn(idum,jdum,kdum,tkin,fdum)

    real,intent(in) :: tkin,idum,jdum,kdum
    real,intent(out) :: fdum
    real :: gibbs1dummy, gibbs2dummy

    gibbs1dummy = (matprop(2,1)*matprop(1,1)-matprop(9,1))*tkin-tkin*matprop(1,1)&
        *LOG((tkin**(matprop(2,1)))/(p(idum,jdum,kdum)+matprop(3,1))**(matprop(2,1)-1))&
        + matprop(8,1)

    gibbs2dummy = (matprop(2,2)*matprop(1,2)-matprop(9,2))*tkin-tkin*matprop(1,2)&
    *LOG((tkin**(matprop(2,2)))/(p(idum,jdum,kdum)+matprop(3,2))**(matprop(2,2)-1))&
    + matprop(8,2)

    fdum = abs(gibbs2dummy-gibbs1dummy)

    
        







end subroutine gibbsreturn