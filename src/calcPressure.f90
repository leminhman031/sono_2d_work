subroutine calcPressure(yqmixdum, rhoTotalEdum,rhodum, rhosqusqdum, gMdum,pinftyMdum,pdum)
    use globalvar
    use gridvariables
    use Shukla2010
    use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var
    use calculateTsat


    real,intent(in)  :: yqmixdum, rhoTotalEdum,rhodum,rhosqusqdum,gMdum,pinftyMdum 
    real,intent(out) :: pdum 
    real :: rhousq 

    rhousq = rhosqusqdum/rhodum 


    pdum = (gMdum-1.0)*(rhoTotalEdum-0.5*rhousq-rhodum*yqmixdum)&
        - gMdum*pinftyMdum

    !pdum = (gMdum-1.0)*(rhoTotalEdum-0.5*rhousq)&




end subroutine


