subroutine palentiTest

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var
    use calculateTsat
                                             

    !! VARIABLES FOR FILE 
    CHARACTER(*), PARAMETER :: fileplace = "/home/maxle/sonov2/"
    real,DIMENSION(100) :: psat_arr, tsat_arr



    !! VARIABLES FOR PHASE 
    integer :: indexSat
    real :: tsatCell, tCell, psatCell
    real :: interTol = 1e-6 


    real :: temperC2, alpha1_star_star


    real,dimension(2) :: rhok_star


    !!**************  READING IN SAT TABLE ***********************  !

    open(12,file=fileplace//"psat.txt")
    read(12,*) psat_arr(:)
    close(12)


    open(13,file=fileplace//"tsat.txt")
    read(13,*) tsat_arr(:)
    CLOSE(13)
                                                                                                                                         


    !!**************  SCALE SAT TABLE ***********************  !

    psat_arr(:) = psat_arr(:)/pscale   ! Pa to Non dim
    tsat_arr(:) = tsat_arr(:)/300      ! Kelvin to Non dim



    !! Testing using Palenti 

    PRINT*, " " 
    PRINT*, 'Inside Pelanti solver .....'
    PRINT*, " " 
    
    DO i = 1,NI
        IF (p(i,1,1)<30.00 .AND. p(i,1,1)>0.1) THEN  !! if uniform case, turn this off

            print*, "Interpolate to get Tsat .... "
            print*, "i = ", i 
            print*, "P(i,1,1) = ", p(i,1,1) 

            indexSat = int(      (p(i,1,1) -psat_arr(1))/(0.30202) + 1.00) 

            print*, "indexSat = ", indexSat 


            !! SAT PROPERTIES AT CELL 
            psatCell = psat_arr(indexSat)
            tsatCell = tsat_arr(indexSat) 

            tCell = T(i,1,1,2)



            !! THERMO RELAXATION, EQ C.4 
            alpha1_star_star = matprop(1,1)*(matprop(2,1)-1)*(p(i,1,1)+matprop(3,2)*(rhomat(i,1,1,1)))&
                                /  ((matprop(1,1)*(matprop(2,1)-1)*(p(i,1,1)+matprop(3,2)*(rhomat(i,1,1,1)))) &
                                + matprop(1,2)*(matprop(2,2)-1)*(p(i,1,1)+matprop(3,1))*(rhomat(i,1,1,2)))


            
           
                                
            temperC2 = (p(i,1,1)+matprop(3,1)*(alpha1_star_star)) &
                        /((matprop(2,1)-1)*matprop(1,1)*rhomat(i,1,1,1))


            !! Setting T1 and T2 to be equilibrium T            
            T(i,1,1,1) = temperC2 
            T(i,1,1,2) = temperC2 
            !! Check Tliquid > Tsat, metastable 
            IF (T(i,1,1,2) > tsatCell) THEN
                PRINT*, "Passed Tcheck "
                PRINT*, "Tliquid ", T(i,1,1,2), "TsatCell = ", tsatCell

                IF ((phi(i,1,1,2)<(1.0-interTol)) &
                    .AND. (phi(i,1,1,2)>(interTol))) THEN
                    
                    PRINT*, "Passed Phi check"
                    PRINT*, "Calculate equilibrium density and phi... "


                    !! Equation C.9 
                    rhok_star(1) = (p(i,1,1)+matprop(3,1))/((matprop(2,1)-1)*matprop(1,1)*T(i,1,1,1))
                    rhok_star(2) = (p(i,1,1)+matprop(3,2))/((matprop(2,2)-1)*matprop(1,2)*T(i,1,1,2))


                    phi(i,1,1,1) = (rho(i,1,1)- rhok_star(2))/(rhok_star(1)-rhok_star(2)) 

                    ! phi(i,1,1,2) = 1.00 - phi(i,1,1,1)




                    PRINT*, "Pelanti properties... " 
                    PRINT*, 'rho1* = ', rhok_star(1), 'rho2* = ', rhok_star(2)
                    PRINT*, 'rho1*-rho2*', rhok_star(1)-rhok_star(2)
                    ! PRINT*, 'phi1* = ', phi(i,1,1,1), 'phi2* = ', phi(i,1,1,2)
                   
                    ! call SLEEP(1)
                END IF 
            END IF 
        END IF 

        print*, "FINISHED PHASE TRANS" 
        print*, "Tliquid = ", T(i,1,1,2)
        print*, "Tvapor = ", T(i,1,1,1)


    END DO 




end subroutine palentiTest

