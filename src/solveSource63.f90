subroutine solveSource63()
    
    use globalvar
    use gridvariables
    use mieeos5eqn_ntemp
    use RungKutt3
    use appendixBvar
    use source63var

    


    do i = 1,NI
    do j = 1,NJ
    do k = 1,NK
        if (  (phi(i,j,k,1)<(1.0-phiSaureltol)) & 
        &.AND. (phi(i,j,k,1)>(phiSaureltol))) then
         ! Calculate 
        s_alpha1_first = (phi(i,j,k,1)*phi(i,j,k,2))/((phi(i,j,k,1)*phasic_rho(1)*phasic_c(1)**2)+&
        (phi(i,j,k,1)*phasic_rho(1)*phasic_c(1)**2))&
        *((matprop(2,1)-1)/(phi(i,j,k,1))+(matprop(2,2)-2)/(phi(i,j,k,2)))

        s_alpha1_second_top = (phasic_c(1)**2/phi(i,j,k,1))+(phasic_c(2)**2/phi(i,j,k,2))

        s_alpha1_second_bottom = (phasic_rho(1)*phasic_c(1)**2/phi(i,j,k,1)) + &
                                (phasic_rho(2)*phasic_c(2)**2/phi(i,j,k,2))

        s_alpha1_second = s_alpha1_second_top/s_alpha1_second_bottom

        s_alpha1 = (s_alpha1_first*q1) + (ydot1*s_alpha1_second*rho(i,j,k))

        s_y1 = rho(i,j,k)*ydot1

        s_y2 = -rho(i,j,k)*ydot1


        end if
        


    end do
    end do
    end do











end subroutine solveSource63