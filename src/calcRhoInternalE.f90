subroutine calcrhoInternalE(rhoTotalEdum,rhosqusqdum,rhodum,rhoInternalEdum)

      use globalvar
      use gridvariables
      use Shukla2010
      use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
      use RungKutt3
      use mieeos5eqn_ntemp
      use appendixBvar
      use source66var
      use source63var
      use calculateTsat


      real,intent(in)  :: rhoTotalEdum, rhosqusqdum, rhodum 

      real,intent(out) :: rhoInternalEdum 



      rhoInternalEdum = rhoTotalEdum - 0.5*(rhosqusqdum/rhodum) !! rhoe = rhoE - 1/2 rho u ^2 



end subroutine

