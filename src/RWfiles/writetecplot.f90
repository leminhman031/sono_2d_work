!***********************************************************************
      subroutine writetecplot()
!***********************************************************************
! this subroutine outputs datafile in techplot format

      use globalvar
      use gridvariables
      use Shukla2010
      use RungKutt3

      character(len=50) :: fname
      character(len=50) :: fnametest 
      real :: ss






! make schlieren array which is temp(i,j,k,2)
      call makesch


      write(istr,'(I6.6)') giter
      fname = 'tecplotoutput/tecplotdatafile_'//trim(istr)//'.dat'
      open(unit=50,file=fname,status='new',action='write')
      ! write(50,*)"%taus",timeT*tscale/taus
      write(50,*)"%taus",timeT*taus

      fnametest = 'tecplotoutput/testdata_'//trim(istr)//'.txt'
      open(unit  = 51, file=fnametest,status='new',action='write')  

#if probdim==1
      !write(50,*)"%Columns: X, Rho, u,&
           !& P, T(0:1:nmat), Sch, Phi(1:nmat),&
           !&  Rhomat1(1:nmat), RhoY(1:nspec)/Rho, Soundspeed "
      do i=1,NI
        !write(50,*)xc(i,1,1),rho(i,1,1),u(i,1,1),p(i,1,1),&
        !&T(i,1,1,1:nmat),temp(i,1,1,2),&
        !&phi(i,1,1,1:nmat),rhomat(i,1,1,1:nmat),&
        !&rhoY(i,1,1,1:nspec)/rho(i,j,1),soundspeed(i,1,1)


        write(50,*) xc(i,1,1),rho(i,1,1),u(i,1,1),p(i,1,1),&
            &T(i,1,1,1:nmat),phi(i,1,1,1:nmat),rhomat(i,1,1,1:nmat)

      ! write(50,*) xc(i,1,1),phi(i,1,1,1:nmat)
    

      !    write(51,*)xc(i,1,1), pstar_arr(i,1,1)
      enddo






#elif probdim==2
      write(50,*)"%Columns: X, Y, Rho, u,&
                & v, P, T(0:1:nmat), Sch, Phi(1:nmat),&
           &  Rhomat1(1:nmat), RhoY(1:nspec)/Rho, Soundspeed "
      do i=1,NI
      do j=1,NJ
        write(50,*)xc(i,j,1),yc(i,j,1),rho(i,j,1),&
                  &u(i,j,1),v(i,j,1),p(i,j,1),&
                  &T(i,j,1,0:nmat),temp(i,j,1,2),&
                  &phi(i,j,1,1:nmat),rhomat(i,j,1,1:nmat),&
                  &rhoY(i,j,1,1:nspec)/rho(i,j,1),soundspeed(i,j,1)
      enddo 
      enddo
#elif probdim==3
      write(50,*)"%Columns: X, Y, Z, Rho, u,&
                & v, w, P, T(0:1:nmat), Sch, Phi(1:nmat),&
                &  Rhomat1(1:nmat), RhoY(1:nspec)/Rho "
      do i=1,NI
      do j=1,NJ
      do k=1,NK
        write(50,*)xc(i,j,k),yc(i,j,k),zc(i,j,k),&
        &rho(i,j,k),u(i,j,k),v(i,j,k),w(i,j,k),p(i,j,k),&
        &T(i,j,k,0:nmat),temp(i,j,k,2),phi(i,j,k,1:nmat),&
        &rhomat(i,j,k,1:nmat),rhoY(i,j,k,1:nspec)/rho(i,j,k)
      enddo 
      enddo
      enddo
#endif

      close(50,status='keep')
      close(51,status='keep')
      print*,'datafile written: '//fname//' '
      temp = 0.00000
      psi  = 0.00000
      phinxcent = 0.0000000
      phinycent = 0.0000000
      rhonxcent = 0.00000
      rhonycent = 0.0000000


      end subroutine writetecplot
!***********************************************************************
