!***********************************************************************
      subroutine readrestart()
!***********************************************************************
! this subroutine reads restartfile


      use globalvar
      use gridvariables
      use RungKutt3


      character(len=50) :: fname

! open the restart file
      write(istr,'(I0)') rstiter
      fname = 'restartfile_'//trim(istr)//'.dat'
      open(UNIT=90,FILE=fname,ACTION='READ')

      read(90,*)timeT,giter,deltat

      do k=1,NJ
      do j=1,NJ
      do i=1,NI
        read(90,*)rho(i,j,k),u(i,j,k),v(i,j,k),w(i,j,k),p(i,j,k),&
        phi(i,j,k,1:nmat),rhomat(i,j,k,1:nmat),rhoY(i,j,k,1:nspec)
      enddo 
      enddo
      enddo

        close(90,status='keep')
        print*,'restart file read: '//fname//' ' 

        call BC(1)



      end subroutine readrestart
!***********************************************************************