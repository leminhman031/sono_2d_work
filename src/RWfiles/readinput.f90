!***********************************************************************
      subroutine readinput
!***********************************************************************

! pull required variables from respective modules
      use globalvar
      use gridvariables
      use Shukla2010
      use mieeos5eqn_ntemp


      write(6,*), 'READING INPUT FILE:'

! open the filenames list-file to get input file name
      open(UNIT=10,FILE='filenames.list',ACTION='READ')
          read(10,*) inputfile
         do while (scan(inputfile,'!')==1)
          read(10,*) inputfile
        enddo
      close(10)

! open the input file for this case
      open(UNIT=10,FILE=inputfile,ACTION='READ')

!----------------------------------------------------------------------
! Read in values from RESTART SECTION
      read(10,*) ! skip line
      read(10,*) restartflag
      read(10,*) rstiter
      write(6,*), '  RESTART section done '


!----------------------------------------------------------------------
! Read in values from OUTPUT SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) fmtout
      read(10,*) prntline
      read(10,*) prntttle
      write(6,*), '  OUTPUT section done '


!----------------------------------------------------------------------
! Read in values from NUMERICS SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) walltime
      read(10,*) maxiter
      read(10,*) outiter
      read(10,*) outrst
      read(10,*) outtau
      read(10,*) iter_reinit
      read(10,*) iterT
      read(10,*) intiter
      read(10,*) finaltime
      read(10,*) deltat
      read(10,*) CFL
      read(10,*) rk_reinit
      read(10,*) kfish
      write(6,*), '  NUMERICS section done '


!----------------------------------------------------------------------
! Read in values from NONDIMENSIONALIZATION SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) lscale
      read(10,*) pscale
      read(10,*) rscale
      read(10,*) Tempscale




      write(6,*), '  NON-DIM section done '

! calculate the remaining non-dim scales
      tscale  = lscale*sqrt(rscale/pscale) ! time scale
      vscale  = sqrt(pscale/rscale)        ! velocity scale
      cvscale = pscale/(rscale*Tempscale)  ! heat capacity scale


      print*, "pscale = ", pscale 
      print*, "rscale =", rscale
      print*, "cvscale =",cvscale 
      print*, "Tempscale", Tempscale   

       

!----------------------------------------------------------------------
! Read in values from GRID SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) xmin
      read(10,*) xmax
      read(10,*) ymin
      read(10,*) ymax
      read(10,*) zmin
      read(10,*) zmax
      read(10,*) NI
      read(10,*) NJ
      read(10,*) NK
      read(10,*) NG
      write(6,*), '  GRID section done '

#if ROCPACK==1
      dlscale=289.143/0.13061672
      dlscale=dlscale/2.0
      dlscale=dlscale*lscale
      xmin=xmin*dlscale
      xmax=xmax*dlscale
      ymin=ymin*dlscale
      ymax=ymax*dlscale
      write(6,*),xmin,xmax,ymin,ymax
#endif


!----------------------------------------------------------------------
! Read in values from FLOWFIELD SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) nshock
      read(10,*) initpres
      read(10,*) xshock
      read(10,*) pshock
      write(6,*), '  FLOWFIELD section done '

!----------------------------------------------------------------------
! Read in values from PROBLEM VARIABLES
      read(10,*) ! skip line
      read(10,*) ! skip line
      read(10,*) nmat
      read(10,*) num_particles 
      read(10,*) nspec 
      nmed = nmat - num_particles

! Allocate arrays for particle mass/vol/shape correction
      if(num_particles.gt.0)then
        allocate( pmass(1:num_particles) )    !
        allocate( pvol(1:num_particles) )    !
        allocate( xc0(1:num_particles) )    !
        allocate( yc0(1:num_particles) )    !
        allocate( zc0(1:num_particles) )    !
        allocate( g1(1:num_particles) )    !
        allocate( g2(1:num_particles) )    !
        allocate( vlm0(1:num_particles) )    !
        allocate( vlm1(1:num_particles) )    !
        allocate( origvol(1:num_particles) )    !
        allocate( origmass(1:num_particles) )    !
        allocate( radius(1:num_particles) )    !
        allocate( norml(1:num_particles) )    !
        allocate( lamda0(1:num_particles) )    !
        allocate( lamda1(1:num_particles) )    !
        allocate( lamda_vol(1:num_particles) )    !
        allocate( lamda_mass(1:num_particles) )    !
        allocate( lamdanew(1:num_particles) )    !
        allocate( partcentr(3,1:num_particles) ) ! (x,y,z,numpart)
        allocate( partcentr_old(3,1:num_particles) )
      endif 
      write(6,*), '  PROBLEM VARIABLES section done '

!----------------------------------------------------------------------
! Read in values from MATERIALS SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
! Allocate array for material properties
      allocate( matstr(nmat) )     ! for material names

      !allocate matprop 
      allocate( matprop(10,nmat) )! 
      allocate( eos(nmat) ) ! eos tag 
      allocate( dpidr(nmat) ) ! for mie-g eos
! Begin reading string_tags for materials
      do i=1,nmat
         read(10,*) matstr(i)
      enddo
! pull material properties from materials_list file
      write(6,*), '    ...reading materials.list for properties'
      call readmaterials
      write(6,*), '  MATERIALS section done '

      initpres = initpres/pscale



!----------------------------------------------------------------------
! Read in values from REACTION SECTION
      read(10,*) ! skip line
      read(10,*) ! skip line
      if(nspec.eq.0) then
          read(10,*) str1
         do while (scan(str1,'!')==0)
          read(10,*) str1
        enddo
      else
        read(10,*) EaRu ! call this A
        read(10,*) qheat
        read(10,*) Tswitch ! call this n
        read(10,*) ! skip line
        read(10,*) ! skip line
      endif
      Tswitch=Tswitch/tempscale
      !EaRu = EaRu
      theta = EaRu/Tempscale
      !beta = 16.4
      !beta = 4.0/1.4
      !qheat = qheat/cvscale/Tempscale
      beta = qheat/cvscale/Tempscale
      write(6,*),'    Tswitch=',Tswitch
      write(6,*),'    theta=',theta
      write(6,*),'    beta=',beta
      write(6,*), '  REACTION section done '



!----------------------------------------------------------------------
! Read in values from PARTICLE SECTION
      if (num_particles.gt.0) then
         do n=1,num_particles
            read(10,*) xc0(n),yc0(n),zc0(n),radius(n)
            write(6,*)'n: ',n,' (xc0,yc0,zc0) = ',&
                        &xc0(n),yc0(n),zc0(n),' radius = ',radius(n)
         enddo
      endif
      write(6,*), '  PARTICLE section done '


      close(10)
!----------------------------------------------------------------------





      write(6,*) 'FINISHED READING INPUT FILE'
      write(6,*) ' '
      write(6,*) ' '
      write(6,*),'_____________________________________________________'
      write(6,*),' Dimensional Scalings  '
      write(6,*),'  length scale (m)             : ',lscale
      write(6,*),'  pressure scale (Pa)          : ',pscale
      write(6,*),'  density scale (kg/m^3)       : ',rscale
      write(6,*),'  temperature scale (K)        : ',tempscale
      write(6,*),'  time scale (s)               : ',tscale
      write(6,*),'  velocity scale (m/s)         : ',vscale
      write(6,*),'  heat capacity scale (J/kg-K) : ',cvscale
      write(6,*) ' '
      write(6,*)'    # of Materials : ',nmat
      write(6,*)'    # of Particles : ',num_particles
      write(6,*)'    # of Mediums   : ',nmed
      write(6,*)'    # of Species   : ',nspec
      write(6,*),'_____________________________________________________'
      write(6,*) ' '
      write(6,*) ' '
      write(6,*) ' '
      write(6,*) '_____________________________________________________'
      write(6,*) ' '

      do i=1,nmat
         write(istr,'(I6)') i
         write(6,*), 'MATERIAL '//trim(adjustl(istr))//' PROPERTIES '
         write(6,*), ' Material Tag           :   ',matstr(i)
         if(eos(i)==2) then
            write(6,*)' Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(6,*)' Gruneisen Gamma        :',matprop(2,i)-1.0
            write(6,*)' Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(6,*)' e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(6,*)' Sound Speed (m/s)      :',matprop(7,i)*vscale
            write(6,*)' Gruneisen-s            :',matprop(6,i)
            write(22,*)'% Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(22,*)'% Gruneisen Gamma        :',matprop(2,i)-1.0
            write(22,*)'% Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(22,*)'% e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(22,*)'% Sound Speed (m/s)      :',matprop(7,i)*vscale
            write(22,*)'% Gruneisen-s            :',matprop(6,i)
         elseif(eos(i)==1) then
            write(6,*)' Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(6,*)' Stiffened Gamma        :',matprop(2,i)
            write(6,*)' P_Infinity (Pa)        :',matprop(3,i)*pscale
            write(6,*)' Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(6,*)' e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(6,*)' Sound Speed (m/s)      :',matprop(7,i)*vscale
            write(22,*)'% Specific Heat (J/kg-K) :',matprop(1,i)*cvscale
            write(22,*)'% Stiffened Gamma        :',matprop(2,i)
            write(22,*)'% P_Infinity (Pa)        :',matprop(3,i)*pscale
            write(22,*)'% Rho_0 (kg/m3)          :',matprop(4,i)*rscale
            write(22,*)'% e_0 (J/kg)             :',matprop(5,i)&
                                                 *cvscale*Tempscale
            write(22,*)'% Sound Speed (m/s)      :',matprop(7,i)*vscale
         endif
         write(6,*)
      enddo

      write(6,*) '_____________________________________________________'
      write(6,*) ' '


      end subroutine readinput
!***********************************************************************
