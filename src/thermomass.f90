subroutine thermomass()
    ! This function calculates the coefficient at the interface

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
    use mieeos5eqn_ntemp
    use appendixBvar



    ! For calculating gibbs 
    real :: dum1, dum2, dum3


    


    !Coefficients
    !Ck, Dk
    real,dimension(1:nmat) :: coeff_C, coeff_D
    
    !Coefficient A
    real,dimension(1:nmat) :: coeff_Adum1, coeff_Adum2
    real :: coeff_A



    !Coefficient B
    real,dimension(1:nmat) :: coeff_Bdum1, coeff_Bdum2
    real :: coeff_B

    !Coefficient A'
    real,dimension(1:nmat) :: coeff_Aprime_dum
    real :: coeff_Aprime

    ! Coefficient B'
    real :: coeff_Bprime
    real,dimension(1:nmat) :: coeff_Bprime_dum



    !Phasic speed of sound 
    real,dimension(1:nmat) :: phasic_c

    !Phasic rho
    real,dimension(1:nmat) :: phasic_rho

    !Phasic enthalpy

    real,dimension(1:nmat) :: phasic_h


    ! DEFINE SMALL NUMBER, USED IN PHI REGION

    real :: tiny = 1e-8
    real :: phixleft, phixright




  
    do i = 1,NI
        if (xc(i,1,1) == 0.75) then
            phixright = i
        else if (xc(i,1,1) == 0.75) then
            phixleft = i
        end if
    end do 

    ! print*, 'COORDINATES OF PHI FOR THERMOMASS', phixleft, phixright
    



    

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !         MAIN CODE                                          !

    ! Calculate GIBBS

    do imat = 1,nmat
        do k = 1,NK
            do j = 1,NJ
                do i = 1,NI

                    dum1 = (matprop(2,imat)*matprop(1,imat)-matprop(9,imat))*T(i,j,k,imat)

                    dum2 = matprop(1,imat)*T(i,1,1,imat)
                    
                    dum3 = log10((T(i,j,k,imat)**(matprop(2,imat)))/((p(i,j,k)+matprop(3,imat))**(matprop(2,imat)-1)))

                    gibbs(imat,i) = dum1-(dum2*dum3)+matprop(8,imat)         

                end do
            end do
        end do
    end do


    ! FORM COEFFICIENTS Ck, Dk
    do imat = 1,nmat
        do k = 1,NK
            do j = 1,NJ

                do i = 1,NI

                    
                    !Ck = (1-gammak*Tk)/(gammak*(p+pinftyk))

                    coeff_C(imat) = (1-matprop(2,imat)*T(i,j,k,imat))/(matprop(2,imat)*(p(i,j,k)+matprop(3,imat))) 

                    ! Dk = (qk-gk)/(Tk)
                    coeff_D(imat) = matprop(8,imat)-gibbs(imat,i)/T(i,j,k,imat)
                    
                    

                    ! Calculate phasic rho
                    ! rhok = rhokphik/(phik+tiny) => for numerical reasons
                    phasic_rho(imat) = rhomat(i,j,k,imat)/(phi(i,j,k,imat)+tiny)


                    ! Calculate phasic c
                    ! ck = sqrt(gamma*(p+pinfty)/rhok)
                    phasic_c(imat) = sqrt((matprop(2,imat)*(p(i,j,k)+matprop(3,imat)))/phasic_rho(imat))

                    !Calculate Adum1, 2nd bracket term in A coefficient = coeff for A'

                    coeff_Adum1(imat) = ((matprop(2,imat)-1)/(matprop(2,imat)*(p(i,j,k)+matprop(3,imat))))

                    !Calculate Adum2, last two terms in A coefficient


                    coeff_Adum2(imat) = 1/(matprop(1,imat)*matprop(2,imat)*(rhomat(i,j,k,imat)+tiny))

                    ! coeff_Adum2(imat) = 1/(matprop(1,imat)*matprop(2,imat)*(phi(i,j,k,imat)+tiny)*phasic_rho(imat))


                    ! Calculate phasic enthalpy

                    phasic_h(imat) = matprop(10,imat)*T(i,j,k,imat)


                    ! !Coeff Bdum1, 2nd bracket term in B coefficient
                    coeff_Bdum1(imat) = (rho(i,j,k)*soundspeed(i,j,k)**2)/phasic_rho(imat)-((fluidprop(1)-1)*phasic_h(imat))

                    !Coeff Bdum2, last bracket term in B coefficient

                    coeff_Bdum2(imat) = 1/(matprop(1,imat)*matprop(2,imat)*(matprop(2,imat)-1)*phasic_rho(imat))


                    !Coeff Aprime dum

                    coeff_Aprime_dum(imat) = (1+coeff_D(imat)/(matprop(1,imat)*matprop(2,imat)))*(1/(phi(i,j,k,imat)&
                        *phasic_rho(imat)))

                    ! Coeff Bprime_dum

                    coeff_Bprime_dum(imat) = (1+(coeff_D(imat))/(matprop(1,imat)*matprop(2,imat)))*(1/((matprop(2,imat)-1)&
                        *phasic_rho(imat)))


                end do

            end do
        end do
    end do



    



    !FORM COEFFICIENTS A, B, A' and B'

    do k = 1,NK
        do j = 1,NJ
            do i = 1,NI

                ! A COEFF
                ! coeff_A = -((coeff_C(1)-coeff_C(2))*rho(i,j,k)*soundspeed(i,j,k)**2)*(coeff_Adum1(1)-coeff_Adum1(2))&
                !     +coeff_Adum2(1)+coeff_Adum2(2)

                coeff_A = -(coeff_C(1)-coeff_C(2))*fluidprop(1)*(p(i,j,k)+fluidprop(2))*(coeff_Adum1(1)-coeff_Adum1(2))&
                    +coeff_Adum2(1)+coeff_Adum2(2)


                ! ! B COEFF

                coeff_B = -((coeff_C(1)-coeff_C(2))*rho(i,j,k))*(coeff_Bdum1(1)-coeff_Bdum1(2))-rho(i,j,k)*(fluidprop(1)-1)&
                    *(phasic_h(1)-phasic_h(2))*(coeff_Bdum2(1)-coeff_Bdum2(2))


                ! ! A PRIME COEFF

                coeff_Aprime = (coeff_D(1)*coeff_C(1)-coeff_D(2)*coeff_C(2))*rho(i,j,k)*soundspeed(i,j,k)**2&
                    *(coeff_Adum1(1)-coeff_Adum1(2))-coeff_Aprime_dum(1)-coeff_Aprime_dum(2)

                ! ! B PRIME COEFF

                coeff_Bprime = (coeff_D(1)*coeff_C(1)-coeff_D(2)*coeff_C(2))*rho(i,j,k)*(coeff_Bdum1(1)-coeff_Bdum1(2))&
                    +rho(i,j,k)*(fluidprop(1)-1)*(phasic_h(1)-phasic_h(2))*(coeff_Bprime_dum(1)+coeff_Bprime_dum(2))

        end do
    end do
end do



    



    


    

    
    

    
    







    

end subroutine thermomass