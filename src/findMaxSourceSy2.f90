subroutine findMaxSourceSy2(idum,jdum,kdum,deltatdum)

  !FINDING THE MAXIMUM SOURCE FOR dRho2, Phi2
  use globalvar
  use gridvariables
  use Shukla2010
  use RungKutt3
  use mieeos5eqn_ntemp
  use appendixBvar
  use source66var
  use source63var

  integer,intent(in) :: idum,jdum,kdum
  real,intent(in) :: deltatdum
  



      ! CALCULATE Smax_y2
      if (s_y2 > 0.0) then   
            s_max_y2 = -((rhomat(idum,jdum,kdum,2)/(phi(idum,jdum,kdum,2)))*(1-phi(idum,jdum,kdum,2)))&
            /deltatdum
      else
            s_max_y2 = rhomat(idum,jdum,kdum,2)/deltatdum
      end if          





end subroutine findMaxSourceSy2
