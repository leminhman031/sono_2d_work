subroutine pelantiSolveITER

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var
    use calculateTsat
    use palentiVar                                             

    !! VARIABLES FOR FILE 
    ! CHARACTER(*), PARAMETER :: fileplace = ""
    ! real,DIMENSION(100) :: psat_arr, tsat_arr



    !! VARIABLES FOR PHASE 
    integer :: indexSat
    real :: tsatCell, tCell, psatCell
    real :: interTol = 1e-2 !1e-6 

    real,dimension(2) :: rhok_star

    real :: mixture_internal_e_per_vol 

    real :: tstar

    real :: pstar 

    open(unit = 42,file='/home/maxle/sonov2/star.dat',action='write',status='old')



    DO i = 1, NI

       IF ((phi(i,1,1,2)<(1.0-interTol)) &
            .AND. (phi(i,1,1,2)>(interTol))) THEN
                              
          
                IF (p(i,1,1)<30.00) THEN               

                
                mixture_internal_e_per_vol = rhoE(i,1,1) - 0.5*rhou(i,1,1)**2/rho(i,1,1)
                ! call sleep(1)
                call iterSolve(i,rho(i,1,1),mixture_internal_e_per_vol,pstar)
                               
                print*, ""
                ! RECALCULATE ap, bp, dp USING P* from ITER SOLVER 
                print*, "Recalculate ap,bp,dp using P* from iter solver..."
                call pelantiC6(pstar,rho(i,1,1),mixture_internal_e_per_vol,tstar)
                print*, "Input for calculations of T*"
                print*, "rho = ", rho(i,1,1)
                print*, "e =", mixture_internal_e_per_vol

                print* ,"P* = ", pstar, " T* = ", tstar
                ! STOP 
                write(42,*) tstar*Tempscale, pstar       
                                           
                tCell = T(i,1,1,2)
                          

                !$ Check Tliquid > Tsat, metastable 
                IF (tCell > tstar) THEN                   
                    
                    !! C9 Pelanti 
                    rhok_star(1) = (pstar+matprop(3,1))/((matprop(2,1)-1)*matprop(1,1)*tstar)
                    rhok_star(2) = (pstar+matprop(3,2))/((matprop(2,2)-1)*matprop(1,2)*tstar)
                    ! rhok_star(1) = 0.99*rho(i,1,1)                
                
                
                ! Dr.Zhang fixes                    
                    IF (rho(i,1,1) > rhok_star(1)) THEN 
                    
                        p(i,1,1) = pstar
                        phi(i,1,1,2) = (rho(i,1,1)- rhok_star(1))/(rhok_star(2)-rhok_star(1)) 
                        phi(i,1,1,1) = 1.00 - phi(i,1,1,2)

                        rhomat(i,1,1,1) = rhok_star(1)*phi(i,1,1,1)
                        rhomat(i,1,1,2) = rho(i,1,1) - rhomat(i,1,1,1) !rhok_star(2)*phi(i,1,1,2)
                    ! ELSE 
                        
                    !     ! phi(i,1,1,2) = 1e-4
                    !     ! phi(i,1,1,1) = 1.00 - phi(i,1,1,2)
                    !     ! rhomat(i,1,1,1) = rho(i,1,1)
                    !     ! rhomat(i,1,1,2) = rho(i,1,1) - rhomat(i,1,1,1)


                    !     phi(i,1,1,2) = 1e-4 
                    !     phi(i,1,1,1) = 1.00 - phi(i,1,1,2)
                    !     rhomat(i,1,1,1) = rho(i,1,1)
                    !     rhomat(i,1,1,2) = rho(i,1,1) - rhomat(i,1,1,1)

                    ENDIF
                 
                    ! rhomat(i,1,1,2) = rhok_star(2)*phi(i,1,1,2)
                    ! rhomat(i,1,1,1) = rho(i,1,1) - rhomat(i,1,1,2)

                    print*, "rho*1 = ", rhok_star(1), "rho*2 = ", rhok_star(2)
                    print*, "phi1* = ", phi(i,1,1,1), "phi2* = ", phi(i,1,1,2)

                    print*, "pstar = ", pstar


                END IF !!Tcell 
            END IF!! PRESSURE
         END IF !! PHI
    END DO  !! DO I 

    
  
   
    close(42)
    



end subroutine pelantiSolveITER



subroutine pelantiC6(pdum,rhodum,edum,tcode) 

    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3


    real,intent(in) :: pdum,rhodum,edum 
    real,intent(out) :: tcode

    real :: ap_1, ap_2, ap_3 
    real :: bp_1, bp_2, bp_3



    real :: gm1, gm2 
    real :: cv1, cv2 
    real :: cp1, cp2 
    real :: pinf1, pinf2
    real :: qu1, qu2  

    real :: discri, b_sq, four_a_d

    ! real :: tcode 

    cv1 = matprop(1,1)
    cv2 = matprop(1,2)


    gm1 = matprop(2,1)
    gm2 = matprop(2,2)

    pinf1 = matprop(3,1)
    pinf2 = matprop(3,2)

    cp1 = matprop(10,1)
    cp2 = matprop(10,2)


    qu1 = matprop(8,1)
    qu2 = matprop(8,2)
    
 

    ap_1 = rhodum*cv1*cv2
    ap_2 = (gm1-1.0)*(pdum+gm2*pinf2)
    ap_3 = (gm2-1.0)*(pdum+gm1*pinf1)

    ap_palenti = ap_1*(ap_2-ap_3)


    bp_1 = edum*((gm2-1.0)*cv2*(pdum+pinf1)-(gm1-1.0)*cv1*(pdum+pinf2))   !! From Palenti 
    ! bp_1 = rhodum*edum*((pdum+pinf2)*cv1*(pdum+gm1*pinf1)-(pdum+pinf1)*cv2*(pdum+gm2*pinf2)) !! Max derives
    bp_2 = rhodum*((gm1-1)*cv1*qu2*(pdum+pinf2)-(gm2-1)*cv2*qu1*(pdum+pinf1))
    bp_3 = cv1*(pdum+pinf2)*(pdum+gm1*pinf1)-cv2*(pdum+pinf1)*(pdum+gm2*pinf2)


    bp_palenti = bp_1 + bp_2 + bp_3

    dp_palenti = (qu1-qu2)*(pdum+pinf2)*(pdum+pinf1)      !! From Palenti

    ! dp_palenti = (qu1-qu2)*(pdum+pinf2)*(pdum+pinf1)-(rhodum*edum)*(qu2-qu1)*(pdum+pinf1)*(pdum+pinf2) !! Max derives

    b_sq = bp_palenti**2
    four_a_d = 4*ap_palenti*dp_palenti


    discri = b_sq-four_a_d

    ! if (discri < 0.0) then 
    !     print*, "Error negative discriminant ", discriminant
    !     STOP 
    ! endif  
        


    tcode = (-bp_palenti+SQRT(discri))/(2.0*ap_palenti)

    print*, ap_palenti, bp_palenti, dp_palenti
    print*, b_sq, four_a_d, discri




end subroutine pelantiC6




subroutine formC8(pdum,rho_dum,e_dum,c8return,temper_return)


    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3

    real,intent(out) :: c8return,temper_return
    real,intent(in) :: pdum,rho_dum,e_dum
    
    real :: cp_l, cp_v
    real :: cv_l, cv_v 
    real :: gm_l, gm_v
    real :: q_l, q_v
    real :: qp_l, qp_v 
    real :: pinf_l, pinf_v 


    cp_v = matprop(10,1)
    cp_l = matprop(10,2)

    cv_v = matprop(1,1)
    cv_l = matprop(1,2)

    gm_v = matprop(2,1)
    gm_l = matprop(2,2)

    q_v = matprop(8,1)
    q_l = matprop(8,2)

    qp_v = matprop(9,1)
    qp_l = matprop(9,2)

    pinf_v = matprop(3,1)
    pinf_l = matprop(3,2)


     !! form ap,bp,dp 
    call pelantiC6(pdum,rho_dum, e_dum,temper_return)

    !! Calculate palenti variables equation 11b
    as_palenti = (cp_l-cp_v+qp_v-qp_l)/(cp_v-cv_v)
    bs_palenti = (q_l-q_v)/(cp_v-cv_v)
    cs_palenti = (cp_v-cp_l)/(cp_v-cv_v)
    ds_palenti = (cp_l-cv_l)/(cp_v-cv_v)
    

    !! c8
    c8return = as_palenti + bs_palenti/(temper_return) + cs_palenti*log(temper_return*300.)+&
                ds_palenti*log((pdum+pinf_l)*pscale)&
            - log((pdum+pinf_v)*pscale)


    ! c8return = -as_palenti - bs_palenti/(temper_return) - cs_palenti*log(temper_return*300.)-&
    !             ds_palenti*log((pdum+pinf_l)*pscale)&
    !             + log((pdum+pinf_v)*pscale)

    ! print*,"---------------------------"
    ! print*,"inside formC8 "
    ! print*, "input p = ", pdum
    ! print*, "ap = ", ap_palenti
    ! print*, "bp = ", bp_palenti
    ! print*, "dp = ", dp_palenti
    ! print*, "temper C7 = ", temper_return
    ! print*, "c8return = ", c8return
    ! print*,"---------------------------"



end subroutine formC8




! subroutine iterSolve(counter,rhodummy,edummy,iterRes)

!     use palentiVar
!     use globalvar
!     use gridvariables
!     use Shukla2010
!     use RungKutt3
  
!     real :: toler, guess, errorITER, fprime, f1, f2, t1, t2,t_error, errorDIFF
!     real,intent(out) :: iterRes 
!     real,intent(in) :: rhodummy, edummy
!     integer, intent(in) :: counter 




!     guess = 1.00
!     errorITER = 20.00
!     toler = 1e-2 

!     print*, ""
!     print*, "--------------------------------------------------------------------------"
!     print*, ""
!     print*, "INSIDE ITER SOLVER AT i = ", counter
!     print*, "INPUT rho0 = ", rhodummy, " e0 = ", edummy
!     print*, "Guess before = ", guess
!     do while (abs(errorITER)>toler)      


!         !!form F
!         print*, "calculate f(guess) " 
!         call formC8(guess,rhodummy,edummy,f1,t1)
!         print*, "fguess = ", f1, " tguess = ", t1

!         print*, ""
!         print*, "calculate f(f(guess))"
!         call formC8(guess+f1,rhodummy,edummy,f2,t2)
!         print*, "f(guess+h) = ", f2, " t(guess+h) = ", t2
        
!         !!calculate Fprime
!         print*, ""
!         print*, "calculate fprime" 
!         ! fprime = (f2-f1)/f1
!         fprime = (f2/f1)-1.0
!         print*, "fprime = ", fprime

!         print*, ""
!         print*, "calculate new guess"
!         guess = guess-(f1/fprime)
!         print*, "guess = ", guess

!         print*, "" 
!         print*, "calculate error.."
!         ! call formC8(guess,rhodummy,edummy,errorDIFF,t_error)
!         ! errorITER = abs(errorDIFF)/guess
!         ! print*, "error diff = ", errorDIFF, "erroITER = ", errorITER, " tolerance = ", toler


!         call formC8(guess,rhodummy,edummy,errorITER,t_error)
!         if (abs(errorITER)>toler) then 
!             print*, ""
!             print*, "CONTINUE ITERATING...."
!         end if

        
!     end do 


!     iterRes = guess 
!     print*, "Guess after = ", iterRes
!     print*, "FINISHED ITER SOLVER..."
!     print*, ""
!     print*, "--------------------------------------------------------------------------"




! end subroutine iterSolve





subroutine iterSolve(counter,rhodummy,edummy,iterRes)

    use palentiVar
    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
  
    real :: toler, guess, errorITER, fprime, f1, f2, t1, t2,t_error
    real,intent(out) :: iterRes 
    real,intent(in) :: rhodummy, edummy
    integer, intent(in) :: counter 




    guess = 1
    errorITER = 20
    toler = 1e-2 

    print*, ""
    print*, "--------------------------------------------------------------------------"
    print*, ""
    print*, "INSIDE ITER SOLVER AT i = ", counter
    print*, "INPUT rho0 = ", rhodummy, " e0 = ", edummy
    print*, "Guess before = ", guess
    do while (abs(errorITER)>toler)      


        !!form F
        print*, "calculate f(guess) " 
        call formC8(guess,rhodummy,edummy,f1,t1)
        print*, "fguess = ", f1, " tguess = ", t1

        print*, ""
        print*, "calculate f(f(guess))"
        call formC8(guess+f1,rhodummy,edummy,f2,t2)
        print*, "f(guess+h) = ", f2, " t(guess+h) = ", t2
        
        !!calculate Fprime
        print*, ""
        print*, "calculate fprime" 
        ! fprime = (f2-f1)/f1
        fprime = (f2/f1)-1.0
        print*, "fprime = ", fprime

        print*, ""
        print*, "calculate new guess"
        guess = guess-(f1/fprime)
        print*, "guess = ", guess

        print*, "" 
        print*, "calculate error.."
        call formC8(guess,rhodummy,edummy,errorITER,t_error)
        print*, "error = ", errorITER, " tolerance = ", toler

        if (abs(errorITER)>toler) then 
            print*, ""
            print*, "CONTINUE ITERATING...."
        end if
        
    end do 


    iterRes = guess 
    print*, "Guess after = ", iterRes
    print*, "FINISHED ITER SOLVER..."
    print*, ""
    print*, "--------------------------------------------------------------------------"




end subroutine iterSolve
