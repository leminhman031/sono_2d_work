subroutine solveSource6663(idum,jdum,kdum,dtdum)

    use globalvar
    use gridvariables
    use Shukla2010
    use RungKutt3
    use mieeos5eqn_ntemp
    use appendixBvar
    use source66var
    use source63var

    integer, intent(in) ::idum,jdum,kdum
    real, intent(in) :: dtdum
       
    real :: top1, top2,rho_ydot1 

    real :: q1_part1, q1_part2
    real :: y1_part1, y1_part2  

    real :: delta_temp, delta_gibbs  
  
    

    !****************************** SOURCE 6.6 ************************!

    delta_temp = (T(idum,jdum,kdum,2) - T(idum,jdum,kdum,1))
    delta_gibbs = (gibbs(2) - gibbs(1))

    q1_part1 = -((coeff_Bprime)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
                *(delta_temp/dtdum)  

    q1_part2 = ((coeff_B)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
      *(delta_gibbs/dtdum)
  
  
    q1 = -q1_part1 + q1_part2


    y1_part1 = ((coeff_Aprime)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
                 *(delta_temp/dtdum)  
    y1_part2 = ((coeff_A)/(coeff_A*coeff_Bprime-coeff_Aprime*coeff_B))&
                *(delta_gibbs/dtdum)    

    ydot1 = y1_part1-y1_part2   

    !****************************** END SOURCE 6.6 ********************!

    !**************************** EQUATION 6.3 ************************!


    ! COEFF IN FRONT OF Q1 
    s_alpha1_first = (phi(idum,jdum,kdum,1)*phi(idum,jdum,kdum,2))/((phi(idum,jdum,kdum,2)&
                    *matprop(2,1)*(p(idum,jdum,kdum)+matprop(3,1)))&
                    +(phi(idum,jdum,kdum,1)*matprop(2,2)*(p(idum,jdum,kdum)+matprop(3,2))))&
                    *(((matprop(2,1)-1)/(phi(idum,jdum,kdum,1)))+((matprop(2,2)-1)/(phi(idum,jdum,kdum,2))))


    !s_alpha1_first = (phi(idum,jdum,kdum,1)*phi(idum,jdum,kdum,2))/((phi(idum,jdum,kdum,2)&
                        !*phasic_rho(1)*phasic_c(1)**2)&
                        !+(phi(idum,jdum,kdum,1)*phasic_rho(2)*phasic_c(2)**2))&
                        !*(((matprop(2,1)-1)/(phi(idum,jdum,kdum,1)))+((matprop(2,2)-1)&
                        !/(phi(idum,jdum,kdum,2))))


    ! COEFF IN FRONT OF rhoYdot1
      
    top1 =  ((phasic_c(1)**2)/phi(idum,jdum,kdum,1))
    top2 = ((phasic_c(2)**2)/phi(idum,jdum,kdum,2))
    s_alpha1_second_top = top1 + top2



    s_alpha1_second_bottom = ((matprop(2,1)*(p(idum,jdum,kdum)+matprop(3,1)))/phi(idum,jdum,kdum,1))&
            +((matprop(2,2)*(p(idum,jdum,kdum)+matprop(3,2)))/phi(idum,jdum,kdum,2))   

    
    !s_alpha1_second_bottom = (phasic_rho(1)*phasic_c(1)**2)/phi(idum,jdum,kdum,1)&
                            !+(phasic_rho(2)*phasic_c(2)**2)/phi(idum,jdum,kdum,2)

    s_alpha1_second = s_alpha1_second_top/s_alpha1_second_bottom


    rho_ydot1 =  rho(idum,jdum,kdum)*ydot1 


    s_alpha1 = (s_alpha1_first*q1) + (s_alpha1_second*rho_ydot1)

    s_y1= rho_ydot1 

    s_y2 = -1.0*rho_ydot1

    print*, "" 
    print*, "FROM SOLVE SOURCE 6663" 
    print*, "AT i = ", idum
    print*, "Y1 = ", ydot1
    print*, "RHO YDOT1", rho_ydot1 
    print*, "S_y1 = ", s_y1 
    print*, "RHO = ", rho(idum,jdum,kdum)



end subroutine solveSource6663
