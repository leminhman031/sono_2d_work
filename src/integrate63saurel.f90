subroutine integrate63saurel()

      ! INTEGRATE USING RK3
  use globalvar
  use gridvariables
  use Shukla2010
  use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
  use RungKutt3
  use mieeos5eqn_ntemp
  use appendixBvar
  use source66var
  use source63var



  do i = 1,NI
  do j = 1,NJ
  do k = 1,NK



      ! INTEGRATING 6.3
  if (abs(s_max_alpha1)>abs(s_alpha1) &
  .AND. abs(s_max_y1)>abs(s_y1)) then

        ! print*, 'INSIDE THE INTEGRATION SUBROUTINE'
        !USING HYDROTIME STEP
        
        ! GETTING 3KS AT HYDRO TIME STEP

        ! Get K1
        call solveSource6663(deltat,s_alpha1_k1,s_y1_k1,s_y2_k1)

        ! Get K2
        call solveSource6663(0.5*deltat,s_alpha1_k2,s_y1_k2,s_y2_k2)

        ! Get K3
        call solveSource6663(2.00*deltat,s_alpha1_k3,s_y1_k3,s_y2_k3)





        ! SOLVING 6.3 AT HYDRO TIME STEP 

        !dPhi1/dt
        phi(i,j,k,1) = phi(i,j,k,1)+(1.00/6.00)*(s_alpha1_k1+4.00*s_alpha1_k2+s_alpha1_k3)*deltat


        ! drhomat1/dt
        rhomat(i,j,k,1) = rhomat(i,j,k,1)+(1.00/6.00)*(s_y1_k1+4.00*s_y1_k2+s_y1_k3)*deltat
        
        ! drhomat2/dt
        rhomat(i,j,k,2) = rhomat(i,j,k,2)+(1.00/6.00)*(s_y2_k1+4.00*s_y2_k2+s_y2_k3)*deltat

        else
        !USING CHEMICAL TIMESTEP 

        !Calculate the chemical timestep
        
        r_alpha1 = s_max_alpha1/s_alpha1
        dtPHASE = (s_max_alpha1/s_alpha1)*deltat*0.5
        dt_ratio = 2.00/r_alpha1

      

        !IF WE HAVE WHOLE RATIO

        if (mod(2.00,r_alpha1)==0) then
              ! DO INTEGRATION UNTIL WE REACH THE HYDRO TIME STEP 
              do integralcount = 1,int(dt_ratio)
                    ! GETTING 3KS AT CHEMICAL TIME WHOLE RATIO

                    ! Get K1
                    call solveSource6663(dtPHASE,s_alpha1_k1,s_y1_k1,s_y2_k2)

                    ! Get K2
                    call solveSource6663(0.5*dtPHASE,s_alpha1_k2,s_y1_k2,s_y2_k2)

                    ! Get K3
                    call solveSource6663(2.00*dtPHASE,s_alpha1_k3,s_y1_k3,s_y2_k3)

                    ! SOLVING 6.3 AT HYDRO TIME STEP 

                    !dPhi1/dt
                    phi(i,j,k,1) = phi(i,j,k,1)+(1.00/6.00)*&
                          (s_alpha1_k1+4.00*s_alpha1_k2+s_alpha1_k3)*dtPHASE


                    ! drhomat1/dt
                    rhomat(i,j,k,1) = rhomat(i,j,k,1)+(1.00/6.00)*(s_y1_k1+4.00*s_y1_k2+s_y1_k3)*dtPHASE
                    
                    ! drhomat2/dt
                    rhomat(i,j,k,2) = rhomat(i,j,k,2)+(1.00/6.00)*(s_y2_k1+4.00*s_y2_k2+s_y2_k3)*dtPHASE
                                            
              end do
        ! ELSE WE DONT HAVE A WHOLE NUMBER RATIO 
        else
              !FIRST LOOP OVER WHOLE RATIO TIMES (REPEAT OF ABOVE)
              do integralcount = 1,int(dt_ratio)
                    ! GETTING 3KS AT CHEMICAL TIME WHOLE RATIO

                    ! Get K1
                    call solveSource6663(dtPHASE,s_alpha1_k1,s_y1_k1,s_y2_k2)

                    ! Get K2
                    call solveSource6663(0.5*dtPHASE,s_alpha1_k2,s_y1_k2,s_y2_k2)

                    ! Get K3
                    call solveSource6663(2.00*dtPHASE,s_alpha1_k3,s_y1_k3,s_y2_k3)

                    ! SOLVING 6.3 AT HYDRO TIME STEP 

                    !dPhi1/dt
                    phi(i,j,k,1) = phi(i,j,k,1)+(1.00/6.00)*&
                          (s_alpha1_k1+4.00*s_alpha1_k2+s_alpha1_k3)*dtPHASE


                    ! drhomat1/dt
                    rhomat(i,j,k,1) = rhomat(i,j,k,1)+(1.00/6.00)*(s_y1_k1+4.00*s_y1_k2+s_y1_k3)*dtPHASE
                    
                    ! drhomat2/dt
                    rhomat(i,j,k,2) = rhomat(i,j,k,2)+(1.00/6.00)*(s_y2_k1+4.00*s_y2_k2+s_y2_k3)*dtPHASE
                                            
              end do

              ! THEN LOOP OVER THE EXTRA TIMES

              ! Get K1
              call solveSource6663(dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1)),s_alpha1_k1,s_y1_k1,s_y2_k2)

              ! Get K2
              call solveSource6663(0.5*dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1)),s_alpha1_k2,s_y1_k2,s_y2_k2)

              ! Get K3
              call solveSource6663(2.00*dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1)),s_alpha1_k3,s_y1_k3,s_y2_k3)

              ! SOLVING 6.3 AT HYDRO TIME STEP 

              !dPhi1/dt
              phi(i,j,k,1) = phi(i,j,k,1)+(1.00/6.00)*&
                    (s_alpha1_k1+4.00*s_alpha1_k2+s_alpha1_k3)*dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1))


              ! drhomat1/dt
              rhomat(i,j,k,1) = rhomat(i,j,k,1)+&
                    (1.00/6.00)*(s_y1_k1+4.00*s_y1_k2+s_y1_k3)*dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1))
              
              ! drhomat2/dt
              rhomat(i,j,k,2) = rhomat(i,j,k,2)+&
                    (1.00/6.00)*(s_y2_k1+4.00*s_y2_k2+s_y2_k3)*dtPHASE*(mod(2.00,r_alpha1)/(r_alpha1))
        end if
      end if
  end do
  end do
  end do


  
                                




end subroutine