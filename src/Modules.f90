!***********************************************************************
      MODULE globalvar
!***********************************************************************
!  This module creates global variables  

      implicit none

!  walltime is the max time to run the simulation for (in seconds)
!  maxiter is the maximum iteration number for simulation to run to
!  outiter is iteration frequency to write output datafiles
!  outrst is iteration frequency to write restart datafile
!  intiter sets iteration frequency for shukla2010 interface correction
!     to all interfaces in domain (for rho correction)
!  prntline is iteration frequency to print new line in terminal
!  prntttle is iteration frequency to print new title line in terminal
!  outtau is (t/tau) frequency to output flowfield datafile
!     it is used in progmain.f90
      integer, save :: walltime, maxiter,intiter
      integer, save :: outiter,outrst,prntline,prntttle
      real :: outtau

!  GITER is the global iteration count, niter_reinit and niterT are
!    both used in shukla functions to keep track of RK iteration
      integer :: giter,niter_reinit,niterT

!  nmat is the number of materials
!  nspec is the number of species(for reactions)
!  nmed is number of mediums
!  num_particles is number of particles
!  nshock is material that initial shock is in
!  xc0, yc0, zc0, is center location of particles
!  origvol, origmass, is original mass and volume of particle
!  pvol, pmass, is mass and volume of particle
!  radius is radius of particle (nondim)
!  norml is the normalizer used in calculating drag coeff
!  g1, g2, vlm0, vlm1 are used in secant method for mass/vol conserv
!  g1 is volume, g2 is mass, vlm0 and vlm1 are functions being 
!    iterated on in secant method 
      integer, save :: nspec,nmat,num_particles, nmed,nshock
      real, dimension(:), allocatable :: xc0,yc0,zc0,g1,g2,vlm0,vlm1
      real, dimension(:), allocatable :: origvol,origmass,radius,norml
      real, dimension(:), allocatable :: pmass,pvol

! arraydima/b is the dimension for the arrays in hllcsolver subroutine
! arraydima is the dimension for arrays: ULstar, URstar, Consvar
! arraydimb is the dimension for arrays: FL, FR, FLstar, FRstar
      integer,save :: arraydima, arraydimb
 
!  finaltime is the final time of the simulation
!  deltat is the time step for the simulation
!  CFL is the value of CFL condition used to recalculate deltat
!  taus is shock-particle timescale 
!  deltat is timestep size, timeT is current simulation time
!  tautooutlet is time required for shock to reach the 
!    outlet boundary of problem domain (used in progmain.f90 to 
!    stop simulation before shock hits boundary, if needed)
      real, save ::  finaltime, CFL, taus
      real :: deltat, timeT
      real :: tautooutlet


! stuff for reaction
      real :: EaRu,qheat,Tswitch,theta,beta
      real :: tfluid,factor,ww,cfluid

! pi is the constant pi found from pi=4*atan(1.0)
      real, parameter :: pie=3.1415926535897932384626433832795


! RK_reinit sets the max stage for RK scheme for reinitialization
      integer, save :: rk_reinit

! res1, res2, restotal, are used in shukla files in order
!  to keep track of various residuals of reinitialization RK schemes
      real :: res1,res2,restotal
 

!  Following variables are scales for non-dimensionalization
!     lscale is length scale
!     pscale is pressure scale
!     rscale is density scale
!     Tempscale is temperature scale
!     tscale is time scale
!     vscale is velocity scale
!     cvscale is heat capacity scale
!  note: lscale, pscale, rscale, Tscale are defined by user 
!     in input file and used to derive rest of the scales
      real, save :: lscale, pscale, rscale, Tempscale
      real, save :: tscale, vscale, cvscale

!  Following are arrays to hold the primitive variables
!     rho is mixture density
!     u is axial velocity
!     v is transverse velocity
!     p is pressure
!     phi is the volume fraction
!     rhomat is the material density
!  note: rho, u, v, and p are 3D arrays 
!        phi and rhomat are 4D arrays 
!  the 4th dimension corresponds to material # 
!  Soundspeed is array to keep track of mixture sound speed  
!  psi is used in shukla files for calculating normal vectors
      real, dimension(:,:,:), allocatable :: rho, u, v, w,p,pstar_arr,p_deriv_arr
      real, dimension(:,:,:,:), allocatable :: T,phi,rho_k, rhomat, psi, T41a, T41b
      real, dimension(:,:,:), allocatable :: soundspeed

      !To add source term for Phi equation

      real, dimension(:,:,:),allocatable :: unow,rhonow,rhounow,u_next_1st

      
      

! sigspeed is the max signal speed between 
! SL and SR found from the hllc solver
! maxspeed is the maximum signal speed in 
! the computational domain, which
! is updated in TVD after each calling of hllc()
      real, save :: sigspeed, maxspeed

! matprop is array for material properties for m materials
! 1st index corresponds to properties, 
! 2nd index corresponds to material
! eos is array holding the integer tag for each EOS
      real, dimension(:,:), allocatable :: matprop
      integer, save, dimension(:), allocatable :: eos



! Following is for calcfluidprop.f90, used in intermediate calc
      real :: lastphi,dumgam, dumpinf

! fmtout to select what output format when writing datafiles
! restartflag to select if restarting from a restartfile
! rstiter is iteration value of restart file used for restarting
      integer, save :: fmtout, restartflag, rstiter


! initpres is the initial pressure
! pshock is the post-shock pressure
! xshock is the initial position of shock at t=0
      real, save :: initpres, pshock, xshock




! kfish is multiplier to fisher shape correction
      real, save ::  kfish
      integer :: lp,lp1,lp2


! these are character strings for outputting in progmain.f90 
! and making datafiles in makefiles()
      character(10) :: time, istr
! fstr and fstr2 are format strings for output of 
!     variables in terminal window
! these are used in progmain.f90
! timefile and cdfile are strings for the name of the output 
!     files for drag and terminal
! these are set in makefiles() and used in progmain.f90 and writefile() 
! matstr is the string tag for each material used in readmaterial.f90
      character(len=90) :: fstr,fstr2,timefile,cdfile,interfacefile,&
                            taufile,tau2file,wvespdfile,str1,inputfile
      character(len=20),dimension(:),allocatable :: matstr
      character(len=20) :: tmpstr,tmpstr2


! stuff for related to shape/mass/volume correct
! lamda0 and lamda1 correspond to 1st and 2nd guess for secant method
! lamdanew is used in secant method
! lamda_vol and lamda_mass are corrections to preserve particle mass/vol
      real, dimension(:), allocatable :: lamda0,&
                                         lamda1,&
                                         lamda_vol,&
                                         lamda_mass,lamdanew
! partcentr and partcentr_old hold the x,y,z center location for 
!  each particle
      real,dimension(:,:),allocatable :: partcentr,partcentr_old
! phistart is original particle phi field, phimoved is used in 
! shapecorrection, it is the translated phistart field 
      real, dimension(:,:,:,:), allocatable :: phimoved,phistart
! following is used in shape correction
      real :: xcnew,delbarx,remx,xtmp,vtol
      real :: ycnew,delbary,remy,ytmp
      real :: zcnew,delbarz,remz,ztmp
      integer :: ishiftx,ishifty,ishiftz

! to be used for turning on and off phi source term 

      real,save :: kchar

! tolerance for Phi to constraint the integration of Appendix B
      real :: phiSaureltol = 1e-2

! tolereance for phi to keep it from going negative, used in initial


      real :: tiny = 1e-8

! yqmix to be used in calcfluidprop 

      real :: yqmix 

      END MODULE globalvar
!***********************************************************************


!***********************************************************************
      MODULE gridvariables
!***********************************************************************
!  This module creates variables required for grid 
!     generation, to be defined 
!  from the input file, which is caseinput.f90

      implicit none
 
!  NI is # of divisions in axial direction 
!  NJ is # of divisions in transverse direction
!  NG is # of ghost cells outside of problem domain
      integer, save :: NI, NJ, NK,NG
 
!  the problem domain spans [xmin,xmax] in axial direction
!  and [ymin,ymax] in transverse direction
      real, save :: xmin, xmax, ymin, ymax, zmin, zmax
 
!  xc is cell center x coordinate of cell (i,j)
!  yc is cell center y coordinate of cell (i,j)
!  x is x coordinate of left face of cell (i,j)
!  y is y coordinate of bottom face of cell (i,j)
      real, dimension(:,:,:), allocatable :: xc, yc, zc, x, y,z,vol

 
!  deltax is axial width of cell (i,j,k)
!  deltay is transverse width of cell (i,j,k)
!  deltaz is depth of cell (i,j,k)
      real, save :: delx, dely,delz

!  following two variables are grid i and j location of 
!     maximum wave speed
!  last two are the normal vectors to tell if its axial 
!     or transverse direction
      integer :: iloc, jloc, nxcell, nycell
 

      END MODULE gridvariables
!***********************************************************************



!***********************************************************************
      MODULE mieeos5eqn_ntemp
!***********************************************************************
!  This module creates required variables  5eqn Mie EOS pinfinity calc
!  

      implicit none

! following for mie EOS 5eqn
      real :: r0,c0,s0,mies,gm,e0,rhoeos,cv
      real :: pii,a,fi,g,b,dadr,dbdr,p0,dpidrfluid
      real, dimension (:), allocatable  :: dpidr

! lastphileft and lastphiright is the phi value for the last material
! fiveeqrholeft and fiveeqrhoright are left and right states for 
!     rho which is recalculated
! from rhomat when five eqn and rho alpha flags are on
      real :: lastphileft, lastphiright
      real :: fiveeqrholeft, fiveeqrhoright

! following for temperature correction, copied variable names 
!     from DrZhang's code
      real :: es,ddv,v0,sv,si
      real :: vv0,phv,fv,dv,rholoop,ev,ts
      integer :: im,imat,ies

! iterT is iterations for temperature correction, resT is residual
      integer,save :: iterT
      real :: resT



      END MODULE mieeos5eqn_ntemp
!***********************************************************************



!***********************************************************************
      MODULE Shukla2010
!***********************************************************************
!  This module creates required variables for shukla2010 
!     phi and rho correction
      implicit none

! variables used for intermediate calculations 
      real :: tmp, tmp1, tmp2
      real :: nx,ny,nz
      real :: psix, psiy, psiz,psim
      real :: phix, phiy, phiz, phim
      real :: rho_m, rho_x, rho_y, rho_z
      real :: tol, res, tau
      real :: lamda,error,errorm
      integer :: sigiter, lamiter,dliter,loopflag


! parameters related to shukla correction
      real, save :: lsphi, lsrho 
      real :: resphi,resrho
      integer,save :: iter_reinit
      real, parameter :: eps=1e-10
      real, parameter :: alpha=0.100
      real, parameter :: phitol=0.01000


! for bilinear interpolation
      real :: x1,x2,y1,y2,m
      real :: fq11,fq12,fq21,fq22,fxy1,fxy2,fxy

! arrays used in shukla  and related functions
      real, dimension(:,:,:,:), allocatable :: netphi,netrho,tmprho
      real, dimension(:,:,:,:), allocatable :: rhonxcent,&
                                               rhonycent,&
                                               rhonzcent,&
                                               phinxcent,&
                                               phinycent,&
                                               phinzcent
      real, dimension(:,:,:,:), allocatable :: temprho,&
                                               tempphi,&
                                               tempphi2,&
                                               tempphi3,&
                                               temprhomat,&
                                               temprhomat3
      real, dimension(:,:,:), allocatable :: temprho3

      END MODULE Shukla2010
!***********************************************************************



!***********************************************************************
      MODULE TVDfunc
!***********************************************************************
!  This module creates variables required for the TVD function,  
!  which passes left & right state variables to HLLC solver
!  and uses output of HLLC solver to update net axial 
!     and transverse fluxes

      implicit none
 
      real, dimension(:,:,:,:), allocatable :: netFlux
      real, dimension(:), allocatable::leftstateprim,rightstateprim,flx
      ! real, dimension(5) :: leftfluidprop, rightfluidprop, fluidprop
      !Max phase.  
      real, dimension(7) :: leftfluidprop, rightfluidprop, fluidprop
      real :: poverr

! following is for printing out left/right material prop at max 
!     sig speed location
      real :: gl,gr,pil,pir,rol,ror
      real :: axvl,axvr,trvl,trvr,locx,locy
      real :: pll,prr,sosl,sosr,sigl,sigr
      real :: sspd,lphi1,lphi2,rphi1,rphi2
      real :: le,re,lep,rep
 

      END MODULE TVDfunc
!***********************************************************************



!***********************************************************************
      MODULE RungKutt3
!***********************************************************************
!  This module creates variables required Runga-Kutta 3 (RK3.f90) 
!     time iteration function  
!  

      implicit none

! temp is a array to temporarily store the conservative 
!     variables for 1st step of RK3
! the last dimension is size 6+nmat+nmat+nspec, 
! the first 6 are: rho,u,v,w,p,T
! the 1st nmat addition corresponds to nmat Phi 
! the 2nd  nmat addition corresponds to nmat rhomat
! the nspec addition corresponds to # species in RhoY
      real, dimension(:,:,:,:), allocatable :: temp

      real, dimension(:), allocatable :: state

! arrays for conservative variables   
      real, dimension(:,:,:), allocatable :: rhou
      real, dimension(:,:,:), allocatable :: rhov
      real, dimension(:,:,:), allocatable :: rhow
      real, dimension(:,:,:), allocatable :: rhoE
      real, dimension(:,:,:,:), allocatable :: rhoY


! fluid prop is array to hold the fluid mixture properties   
      real, dimension(5) :: fluidprop


      END MODULE RungKutt3
!***********************************************************************






!***********************************************************************
      MODULE hllc
!***********************************************************************
!  This module creates variables required for the HLLC Solver function,  

      implicit none

! Declare the local variables/arrays for this subroutine
! num and den are temp variables meaning numerator and denominator
      real, dimension(:), allocatable :: ULstar, URstar
      real, dimension(:), allocatable :: FL, FR, FLstar, FRstar
      real, dimension(:), allocatable :: D, ConsvarL, ConsvarR
      real :: rhoL, rhoR, uL, uR, vL, vR, wL, wR, PL, PR, PStar
      real :: cL,cR, EL, ER, SL, SR, Sstr
      real :: unL, unR, utL, utR, num, den
      real :: gamleft, pinfleft, dpidrleft
      real :: gamright, pinfright, dpidrright






      END MODULE hllc
!***********************************************************************



!***********************************************************************
      MODULE appendixBvar
!***********************************************************************

      !create variables to use in the thermoass solver

      implicit none
      ! real,dimension(:,:),allocatable :: gibbs

      real, dimension(:),allocatable :: coeff_C, coeff_D
      real :: appB_coeffA = 0.0, appB_coeffB = 0.0

      ! phasic gibbs
      ! real :: gibbsdum1 = 0.0, gibbsdum2 = 0.0, gibbsdum3 = 0.0

      real,dimension(:),allocatable :: gibbsdum1, gibbsdum2,gibbsdum3,gibbs

      ! phasic rho 

      real,dimension(:),allocatable :: phasic_rho

      ! phasic c square  

      real,dimension(:),allocatable :: phasic_c

      ! phasic h 

      real,dimension(:),allocatable :: phasic_h


      ! Coeff A variable

      real,dimension(:),allocatable :: coeff_Adum1, coeff_Adum2
      real :: coeff_A = 0.0
 

      ! Coeff B variable

      real,dimension(:),allocatable :: coeff_Bdum1, coeff_Bdum2
      real :: coeff_B = 0.0




      ! Coeff A prime variable

      real,dimension(:),allocatable :: coeff_Aprime_dum
      real :: coeff_Aprime = 0.0
      real :: Aprime_first = 0.0, Aprime_second = 0.0 


      ! Coeff B prime variable

      real,dimension(:),allocatable :: coeff_Bprime_dum
      real :: coeff_Bprime = 0.0
      real :: Bprime_first = 0.0, Bprime_second = 0.0 

     





      END MODULE appendixBvar
!***********************************************************************




!***********************************************************************
      MODULE source66var
!***********************************************************************

      implicit none
       ! Q1 and Ydot1 

      real :: q1 = 0.0, ydot1 = 0.0

      END MODULE source66var

!***********************************************************************







!***********************************************************************
      MODULE source63var
!***********************************************************************


      implicit none
      ! real,dimension(:,:,:),allocatable :: s_alpha1_first, s_alpha1_second ! 1st and 2nd term of the source in dalpha1/dt


      ! real,dimension(:,:,:),allocatable :: s_alpha1_second_top,s_alpha1_second_bottom



      real :: s_alpha1_first,s_alpha1_second,s_alpha1_second_top,s_alpha1_second_bottom


      ! real,dimension(:,:,:),allocatable :: s_alpha1, s_y1, s_y2

      real :: s_alpha1 = 0.0, s_y1 = 0.0, s_y2 = 0.0
      real :: s_max_alpha1 = 0.0, s_max_y1 = 0.0, s_max_y2 = 0.0
      ! real,dimension(:),allocatable :: dtPHASE

      real :: dtPHASE = 0.0


      real :: dt_ratio = 0.0

      real :: r_PHASE = 0.0

      real :: r_y1  = 0.0

      integer :: integralcount
      
      real :: extra_dt = 0.0

      END MODULE source63var

!***********************************************************************



!***********************************************************************
      MODULE calculateTsat
!***********************************************************************

      real :: tsat

      END MODULE calculateTsat

!***********************************************************************


!***********************************************************************
      MODULE palentiVar 
!***********************************************************************

      real :: as_palenti,bs_palenti,cs_palenti,ds_palenti  
      real :: ap_palenti,bp_palenti, dp_palenti 


      END MODULE palentiVar

!***********************************************************************

