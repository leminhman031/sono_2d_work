!***********************************************************************
      subroutine MG_shockstates(lp,pshk,uleft,rleft,tleft,Us,cleft)
!***********************************************************************
! This subroutine sets the shocked states for a MG EOS

! pull required variables from respective modules
      use globalvar, only: matprop,rscale,vscale,cvscale,&
          &pscale,Tempscale,initpres

      integer, intent(in) :: lp
      real, intent(in) :: pshk
      real, intent(inout) :: uleft,rleft,tleft,Us,cleft

      common /coeff1/ p00,v00,e00,c00
      common /coeff2/ g,s

      rho00 = matprop(4,lp)*rscale
      c00 = matprop(7,lp)*vscale
      cv = matprop(1,lp)*cvscale
      s = matprop(6,lp)
      g = matprop(2,lp)-1.0
      p00 = initpres*pscale
      T00 = Tempscale

      e00 = cv*T00
      v00 = 1.0/rho00

      p = pshk
      v = v00
      call adiabat(v,p)
      rho = 1/v

      e = eos(p,v)
!     compute es
         es = 0.
         ddv = (v-v00)/199.
         do i = 1,199
            vv=v00+(i-1)*ddv
            phi = 1. - vv/v00
            phv = p00 + (c00**2)*phi/((1.-s*phi)**2)/v00
            fv = phv*(1.-0.5*G*(v00-vv)/vv)-0.5*G*p00*(v00-vv)/vv
            es = es + (fv-G*e00/vv)*(vv**G)*ddv
         enddo

         es = e00*((v00/v)**G) - es/(v**G)
         Ts = T00*((v00/v)**G)


      temp = (e-es)/cv+Ts

      d = sqrt(v00*(p-p00)/(1.0-v/v00))
      u = d - rho00*d*v

      phi = 1. - v/v00
      phv = p00 + (c00**2)*phi/((1.-s*phi)**2)/v00
      dphdv= ((1.0+s*phi)/(s*phi-1.0))*((c00)/((1.-s*phi))/v00)**2
      cleft= sqrt(v*( p*(g+1.0)-phv-0.50*g*(phv+p00)+&
                      v00*dphdv*(0.50*g*phi+phi-1.0) ))

      uleft = u
      rleft = rho
      tleft = temp
      Us = d

      end subroutine MG_shockstates
!
!
!
      subroutine adiabat(v,p)
!
!     This subroutine calculates the shock adiabat
!     Note that the equation is dimensional
!
      common /coeff1/ p00,v00,e00,c00
!
!
      v0 = v
      e0 = eos(p,v0)
      f0 = e0 - e00 + 0.5d0*(p+p00)*(v0-v00)
      v1 = v0*0.91
      i = 0
  10  continue
        i = i + 1
        e1 = eos(p,v1)
        f1 = e1 - e00 + 0.5d0*(p+p00)*(v1-v00)
        v2 = v1 - f1*(v1-v0)/(f1-f0)
        if (abs(v2-v1).lt.1.0d-8) go to 20
           v0 = v1
           f0 = f1
           v1 = v2
           if (i.gt.600) then
              write(6,*)'warning01: did not converge',y0,f0,f1
              stop
              goto 20
           endif
           goto 10
  20  continue
      v = v2

      end subroutine adiabat
!
!
!
      real function eos(p,v)

      common /coeff1/ p00,v00,e00,c00
      common /coeff2/ g,s
!
!     Mie_Gruneisen EOS
!
      phi = 1.0d0-v/v00

      pH = c00*c00*phi/((1.0d0-s*phi)**2)/v00 + p00

      f = pH*(1.0d0-0.5d0*g*(v00/v-1.0d0)) &
          - g*p00*(v00-v)/(2.0*v)

      eos = e00 + (v/g)*(p-f)


      end function eos
