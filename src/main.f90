!***********************************************************************
      program main
!***********************************************************************

      !character,intent(in) :: inputname
      print*, ' '
      print*, 'ROCSDT STARTED'
      print*, ' '

     
     


! read required values from the case input file: input.txt
      call readinput




! Allocate memory for arrays from all Modules
      call alloc

! create grid for computation
      call makegrid

! create output files for data
      call makefiles

! call main program of simulation
      call progmain

      print*, ' '
      print*, 'ROCSDT FINISHED'
      print*, ' '

      

! deallocate memory for arrays from all Modules
      call dealloc
      


      end program main
!***********************************************************************
