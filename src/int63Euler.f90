subroutine int63Euler(idum,jdum,kdum)

  ! INTEGRATE USING JUST FORWARD EULER
  use globalvar
  use gridvariables
  use Shukla2010
  use TVDfunc, only: netFlux,lphi1,lphi2,rphi1,rphi2,locx,locy
  use RungKutt3
  use mieeos5eqn_ntemp
  use appendixBvar
  use source66var
  use source63var

  integer,intent(in) :: idum,jdum,kdum





    ! INTEGRATING 6.3
    if (abs(s_max_alpha1)>abs(s_alpha1) &
      .AND. abs(s_max_y1)>abs(s_y1)) then

        !INTEGRATE USING HYDRO STEP

        ! GET SOURCE TERM AT HYDRO TIME STEP

        call solveSource6663(idum,jdum,kdum,deltat)

        phi(idum,jdum,kdum,1) = phi(idum,jdum,kdum,1) + deltat*s_alpha1
        rhomat(idum,jdum,kdum,1) = rhomat(idum,jdum,kdum,1) + deltat*s_y1
        rhomat(idum,jdum,kdum,2) = rhomat(idum,jdum,kdum,2) + deltat*s_y2 
      
        else !not satisfied Smax, S relationship
        
        !INTEGRATE USING CHEMCICAL TIMESTEP

        !Calculate the chemical timestep
        
        
        r_alpha1 = s_max_alpha1/s_alpha1
        dtPHASE = r_alpha1*deltat*0.5

        dt_ratio = r_alpha1
        ! dt_ratio = 2.00/r_alpha1

        

      !  !IF WE HAVE WHOLE RATIO

      !   if (mod(2.00,r_alpha1)==0) then
      !     ! DO INTEGRATION UNTIL WE REACH THE HYDRO TIME STEP 
      !     do integralcount = 1, int(dt_ratio)

      !         ! GET SOURCE TERM AT CHEMICAL TIMESTEP

      !         call solveSource6663(idum,jdum,kdum,dtPHASE)

      !         phi(idum,jdum,kdum,1) = phi(idum,jdum,kdum,1) + dtPHASE*s_alpha1
      !         rhomat(idum,jdum,kdum,1) = rhomat(idum,jdum,kdum,1) + dtPHASE*s_y1
      !         rhomat(idum,jdum,kdum,2) = rhomat(idum,jdum,kdum,2) + dtPHASE*s_y2 
      !     end do

      !   else !don't have whole ratio

      !     extra_dt = (mod(2.00,r_alpha1)/r_alpha1)

      !     print*, 'THIS IS DT_RATIO', dt_ratio
      !     print*, 'THIS IS THE EXTRA DT AMOUNT', extra_dt
          
      !     ! FIRST: DO INTEGRATION TO REACH THE WHOLE PART
      !     do integralcount = 1, int(dt_ratio)

      !       ! GET SOURCE TERM AT CHEMICAL TIMESTEP

      !       call solveSource6663(idum,jdum,kdum,dtPHASE)
      !       phi(idum,jdum,kdum,1) = phi(idum,jdum,kdum,1) + dtPHASE*s_alpha1
      !       rhomat(idum,jdum,kdum,1) = rhomat(idum,jdum,kdum,1) + dtPHASE*s_y1
      !       rhomat(idum,jdum,kdum,2) = rhomat(idum,jdum,kdum,2) + dtPHASE*s_y2 

      !     end do
          
      !     ! SECOND: DO INTEGRATION ON REMAINING
      !   end if

          ! GET SOURCE AT REMAINING
          ! call solveSource6663(idum,jdum,kdum,dtPHASE)

          ! phi(idum,jdum,kdum,1) = phi(idum,jdum,kdum,1) + dtPHASE*s_alpha1*extra_dt
          ! rhomat(idum,jdum,kdum,1) = rhomat(idum,jdum,kdum,1) + dtPHASE*s_y1*extra_dt
          ! rhomat(idum,jdum,kdum,2) = rhomat(idum,jdum,kdum,2) + dtPHASE*s_y2*extra_dt
          
    end if



end subroutine int63Euler