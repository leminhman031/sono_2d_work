subroutine findMaxSource(idum,jdum,kdum,deltatdum)

  !FINDING THE MAXIMUM SOURCE TERM, EQUATION 6.7
  use globalvar
  use gridvariables
  use Shukla2010
  use RungKutt3
  use mieeos5eqn_ntemp
  use appendixBvar
  use source66var
  use source63var

  integer,intent(in) :: idum,jdum,kdum
  real,intent(in) :: deltatdum
  



 

      ! CALCULATE Smax_alpha1
      if (s_alpha1 > 0.0) then 
            s_max_alpha1 = (1.00-phi(idum,jdum,kdum,1))/deltatdum
      else
            s_max_alpha1 = -1.00*phi(idum,jdum,kdum,1)/deltatdum
      end if


      ! CALCULATE Smax_y1
      if (s_y1 > 0.0) then
            s_max_y1 = (rhomat(idum,jdum,kdum,1)/(phi(idum,jdum,kdum,1)))*(1-phi(idum,jdum,kdum,1))/deltatdum
            
            !s_max_y1 = (phasic_rho(1)*(1-phi(idum,jdum,kdum,1)))/deltatdum
      else
            s_max_y1 = -1.00*rhomat(idum,jdum,kdum,1)/deltatdum
      end if           




end subroutine findMaxSource
