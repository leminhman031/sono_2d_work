subroutine testtemper(pdum,rhodum,gammadum,pinfdum, cvdum, qdum, tdum)
    use globalvar
    use gridvariables
    real,intent(in) :: pdum,rhodum,gammadum,pinfdum,cvdum
    real,intent(out) :: tdum


    ! tdum = (pdum+pinfdum)/(rhodum*(gammadum-1)*cvdum)

    real :: gammaM1 

    gammaM1 = gammadum-1.0

    ! EQ 4.1a
    tdum = (1.0/cvdum)*(((pdum+gammadum*pinfdum)/(gammaM1*rhodum))+qdum)
    

    
    




end subroutine testtemper