program satSolve
    implicit none

    !------------------  INITIALIZING ------------------------------------------------!
    ! Dodecane Properties


    ! Temperature 
    real*8 :: pstart , pend                            ! ATM
    real*8 :: tstart, tend 
    real*8 :: tdiff
    real*8 :: numT
    integer :: numTint
    real*8,dimension(:),ALLOCATABLE :: temper_arr     !To store temperature

    ! Pressure 
    real*8,DIMENSION(:),ALLOCATABLE :: p_arr

    ! Other variables (loop)
    integer :: i    

    real*8 :: leftRES, rightRES    


    !! GIBBS 
    real*8,DIMENSION(:),ALLOCATABLE :: g_liquid, g_vapor 



    !! SCALING 
    real*8 :: cvscale, tempscale, pscale, rscale, gibbs_scale  
    !-----------------------  CALCULATIONS -------------------------------------------!
    ! allocate temperature arr
    numT = 100

    pstart = 0.1e5            ! 0.1 atm in Pa  
    pend   = 30e5              ! 10 atm in Pa 
    tdiff = (pend-pstart)/(numT-1)


    ! tstart = 300
    ! tend = 500

    ! tdiff = (tend-tstart)/(numT-1)


    numTint = int(numT)
    ALLOCATE(p_arr(numTint))
    p_arr(1) = pstart

    do i = 2,numTint
         p_arr(i) = p_arr(i-1) + tdiff
    end do




    ! numTint = int(numT)
    ! ALLOCATE(temper_arr(numTint))
    ! temper_arr(1) = tstart

    ! do i = 2,numTint
    !      temper_arr(i) = temper_arr(i-1) + tdiff
    ! end do




    !allocate Temperature arr
    ALLOCATE(temper_arr(numTint))
   

    ! GETTING Psat 
    do i = 1,numTint
        call steffensonBW(p_arr(i),temper_arr(i))
    end do

  


    ! CALCULATE GIBBS 

    ALLOCATE(g_liquid(numTint))
    ALLOCATE(g_vapor(numTint)) 


    do i = 1,numTint
        call calcgibbs(temper_arr(i),p_arr(i),g_liquid(i),g_vapor(i))
    end do 



    pscale = 1e5
    rscale = 1000
    tempscale = 300 
    cvscale = pscale/(rscale*tempscale) 
    gibbs_scale = cvscale*tempscale 
  



    ! !-----------------  PRINTING OUT VARIABLES  -------------------------------------------!

    

    open(unit = 21, file = 'sat.dat', action = 'write')

    open(unit = 22, file = 'psat.txt', action = 'write')
    open(unit = 23, file = 'tsat.txt', action = 'write')



    temper_arr(:) = temper_arr(:)
    p_arr(:) = p_arr(:)/1e5
   
   ! Write out to the file. Pressure (Pascal), T(kelvin)
    write(*,*) "PRESSURE, TEMPERATURE, GIBBS LIQUID, GIBBS VAPOR" 
    do i = 1,numTint 
        write(21,*)  temper_arr(i), p_arr(i)
        write(22,*) p_arr(i)   
        write(23,*) temper_arr(i) 
        print*, temper_arr(i), p_arr(i)
    end do


    close(21)
    close(22)
    close(23)


    



    !! Deallocating
    DEALLOCATE(temper_arr)
    DEALLOCATE(p_arr)

    ! call system('gnuplot plotSAT')
    ! call system('eog --fullscreen TsatPsat.png')
 
  


end program satSolve





!--- FOR GOING BACKWARD, ACCEPT P THEN GET T-----!

subroutine steffensonBW(pressdum,result)
    implicit none
   
    real*8,INTENT(IN) :: pressdum
    real*8,INTENT(OUT) :: result
    real*8  :: tguess
    real*8 :: error
    real*8 :: tol = 1e-5
    real*8 :: fprime
    real*8 :: y,y2

    tguess = 300.00
    error = 1.00


    do while (abs(error)>tol) 
        call f(pressdum, tguess,y)


        call f(pressdum,tguess+y,y2)


        fprime = (y2-y)/y

        tguess = tguess - (y/fprime)

        call f(pressdum,tguess,error)


    end do

    result = tguess

end subroutine steffensonBW






! Function accepts temper, p and other coeff a, b, c , d

subroutine f(pdum,tdum, res)
    implicit none

    ! Water properties
     !real*8 :: pinfty1 = 1e9,pinfty2 = 0.0
     !real*8 :: cp1 = 4267,cp2 = 1487
     !real*8 :: cv1 = 1816, cv2 = 1040
     !real*8 :: gm1 = 2.35,gm2 = 1.43
     !real*8 :: q1 = -1167e3,q2 = 2030e3
     !real*8 :: qp1 = 0.0, qp2 = -23e3

    !   DODECANE SAUREL 
    !  real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    !  real*8 :: cp1 = 2534, cp2 = 2005
    !  real*8 :: cv1 = 1077,cv2 = 1956.00
    !  real*8 :: gm1 = 2.35,gm2 = 1.025
    !  real*8 :: q1 = -755.269e3,q2 = -237.547e3
    !  real*8 :: qp1 = 0.0,qp2 = -24.4e3 

    !   DODECANE PALENTI
    ! real*8 :: pinfty1 = 4e8,pinfty2 = 0.0
    ! real*8 :: cp1 = 2534.00, cp2 = 2005.00
    ! real*8 :: cv1 = 1077.7,cv2 = 1956.45
    ! real*8 :: gm1 = 2.35,gm2 = 1.025
    ! real*8 :: q1 = -755.269e3,q2 = -237.547e3
    ! real*8 :: qp1 = 0.0,qp2 = -24.4e3


    ! USER VARIED
    real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    real*8 :: cp1 = 2534, cp2 = 2005
    real*8 :: cv1 = 1077,cv2 = 1956.00
    real*8 :: gm1 = 2.35,gm2 = 1.025
    real*8 :: q1 = -755e3,q2 = -237e3   
    real*8 :: qp1 = 0,qp2 = -24.485e3 





    ! Saurel Variables coefficients
    real*8 :: adum,bdum,cdum,ddum

    real*8,intent(in) :: tdum, pdum
    real*8,intent(out) :: res

    !calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    bdum = (q1-q2)/(cp2-cv2)
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)



    ! res = log(pdum+pinfty2)-adum-(bdum/tdum)-cdum*log(tdum)-ddum*log(pdum+pinfty1)
    res = -log(pdum+pinfty2)+adum+(bdum/tdum)+cdum*log(tdum)+ddum*log(pdum+pinfty1)

    ! res = -log((pdum+pinfty2)*1e5)+adum+(bdum/tdum)+cdum*log(tdum*300.)+ddum*log((pdum+pinfty1)*1e5)


end subroutine f




subroutine check43(tdum,pdum,lhs,rhs)
    implicit none
    real*8,intent(out) :: lhs,rhs
    real*8,intent(in)  :: tdum,pdum

    real*8 :: adum, bdum, cdum, ddum

    !   DODECANE SAUREL 
     real*8 :: pinfty1 = 4e8,pinfty2=0.0 
     real*8 :: cp1 = 2534, cp2 = 2005
     real*8 :: cv1 = 1077,cv2 = 1956.00
     real*8 :: gm1 = 2.35,gm2 = 1.025
     real*8 :: q1 = -755e3,q2 = -237e3
     real*8 :: qp1 = 0.0,qp2 = -24e3 

    !   DODECANE PALENTI
    !real*8 :: pinfty1 = 4e8,pinfty2 = 0.0
    !real*8 :: cp1 = 2534.00, cp2 = 2005.00
    !real*8 :: cv1 = 1077.7,cv2 = 1956.45
    !real*8 :: gm1 = 2.35,gm2 = 1.025
    !real*8 :: q1 = -755.269e3,q2 = -237.547e3
    !real*8 :: qp1 = 0.0,qp2 = -24.4e3




    





    ! 	calculate coefficients
    adum = (cp1-cp2+qp2-qp1)/(cp2-cv2)
    
    bdum = (q1-q2)/(cp2-cv2)
    
    cdum = (cp2-cp1)/(cp2-cv2)
    ddum = (cp1-cv1)/(cp2-cv2)


    ! print*, adum, bdum, cdum, ddum

    lhs = log(pdum+pinfty2)
    rhs = adum+(bdum/tdum)+cdum*log(tdum)+ddum*log(pdum+pinfty1)

    print*, lhs, rhs 


end subroutine check43

	




















! Subroutine for Steffenson's Method


subroutine steffenson(temperdum,result)
     implicit none
     real*8 :: pinfty1 = 1e9,pinfty2 = 0.0
     real*8 :: cp1 = 4267,cp2 = 1487
     real*8 :: cv1 = 1816, cv2 = 1040
     real*8 :: coeff_d


     real*8,INTENT(IN) :: temperdum
     real*8,INTENT(OUT) :: result
     real*8  :: pguess 
     real*8 :: error
     real*8 :: tol = 1e-2
     real*8 :: fprime
     real*8 :: y,y2

     pguess = 300.00
     error = 100.00

     coeff_d = (cp1-cv1)/(cp2-cv2)


     do while (abs(error)>tol) 
         call f(pguess, temperdum,y)


         ! call f(pguess+y,temperdum,y2)


         ! fprime = (y2-y)/y

         fprime = (1/(pguess+pinfty2))-coeff_d/(pguess+pinfty1)

         pguess = pguess - (y/fprime)

         call f(pguess,temperdum,error)


     end do

     result = pguess

end subroutine steffenson










subroutine zein(pdum,tdum, res)
    implicit none

    ! Water properties
    real*8 :: pinfty1 = 1e9,pinfty2 = 0.0
    real*8 :: cp1 = 4267,cp2 = 1487
    real*8 :: cv1 = 1816, cv2 = 1040
    real*8 :: gm1 = 2.35,gm2 = 1.43
    real*8 :: q1 = -1167e3,q2 = 2030e3
    real*8 :: qp1 = 0.0, qp2 = -23e3

    !Dodecane properties
    ! real*8 :: pinfty1 = 4e8,pinfty2 = 0.0
    ! real*8 :: cp1 = 2534, cp2 = 2005.
    ! real*8 :: cv1 = 1077,cv2 = 1956.
    ! real*8 :: gm1 = 2.35,gm2 = 1.025
    ! real*8 :: q1 = -755e3,q2 = -237e3
    ! real*8 :: qp1 = 0.0,qp2 = -24e3


    real*8,intent(in) :: tdum, pdum
    real*8,intent(out) :: res

    res = -(gm2*cv2-qp2)*tdum+cv2*tdum*log( (tdum**gm2)/( (pdum+pinfty2)**(gm2-1)  )   ) - q2 &
            +(gm1*cv1-qp1)*tdum-cv1*tdum*log( (tdum**gm1)/( (pdum+pinfty1)**(gm1-1)  )   ) + q1
    ! print*, res

end subroutine zein






subroutine calcgibbs(temper,pdum,gi_liquid, gi_vapor)

    implicit none 
    real*8,intent(inout)  :: temper, pdum
    real*8,intent(out) :: gi_liquid,gi_vapor 
    !   DODECANE SAUREL 
    real*8 :: pinfty1 = 4e8,pinfty2=0.0 
    real*8 :: cp1 = 2534, cp2 = 2005
    real*8 :: cv1 = 1077,cv2 = 1956.00
    real*8 :: gm1 = 2.35,gm2 = 1.025
    real*8 :: q1 = -755e3,q2 = -237e3
    real*8 :: qp1 = 0.0,qp2 = -24e3 


    real*8 :: logLiquid, logVapor 



    temper = temper/300
    pdum = pdum/1e5
    pinfty1 = pinfty1/1e5 
    pinfty2 = pinfty2/1e5 

    logLiquid = log( (temper**gm1)/(pdum+pinfty1)**(gm1-1))
    logVapor = log( (temper**gm2)/(pdum+pinfty2)**(gm2-1)) 


    temper = temper*300
    pdum = pdum*1e5  
    pinfty1 = pinfty1*1e5 
    pinfty2 = pinfty2*1e5 


    gi_liquid = (gm1*cv1-qp1)*temper - cv1*temper*logLiquid + q1 
    gi_vapor = (gm2*cv2-qp2)*temper - cv2*temper*logVapor   + q2 
    


end subroutine calcgibbs 





